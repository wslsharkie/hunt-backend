"use strict";

const MONGO_DB_NAME = 'hunters';
const MONGO_USER = 'jxyip';
const MONGO_PASSWORD = 'ZYfqY16uk3AKfHGg';
const MONGO_REPLICA_1 = 'cluster0-shard-00-00-ivuul.mongodb.net:27017';
const MONGO_REPLICA_2 = 'cluster0-shard-00-01-ivuul.mongodb.net:27017';
const MONGO_REPLICA_3 = 'cluster0-shard-00-02-ivuul.mongodb.net:27017';

exports.APP_PORT = 3000;
exports.REDIS_PORT = 6379;

if(process.env.NODE_ENV === undefined) {
    process.env.NODE_ENV = 'development';
}

if(process.env.NODE_ENV === 'production') {
    exports.MONGO_DB_DEBUG = false;
    exports.MONGODB_CONNECT = 'mongodb://'+MONGO_USER+':'+MONGO_PASSWORD+'@' +
        MONGO_REPLICA_1+','+MONGO_REPLICA_2+','+MONGO_REPLICA_3+'/'+MONGO_DB_NAME
        +'?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin';

    exports.REDIS_HOST = 'redis';
} else {
    exports.MONGO_DB_DEBUG = true;
    exports.MONGODB_CONNECT = 'mongodb://localhost:27017/'+MONGO_DB_NAME;

    exports.REDIS_HOST = 'localhost';
}
