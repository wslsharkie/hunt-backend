// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var HeroSchema = new mongoose.Schema({
    heroId:     { type: Number, required: true },
    ownerId:    { type: String, required: true },
    experience: { type: Number, required: true, default: 0 }, // level
    essence:    { type: Number, required: true, default: 0 }, // star
    date:       { type: Date, required: true, default: Date.now() }
});

HeroSchema.statics.gameObject = null;

HeroSchema.statics.resourceKey = function(objectId) {
    return 'heroes-'+objectId.toString();
};

HeroSchema.statics.fieldKey = function () {
    return 'heroId';
};

// Export the Mongoose model
var Hero = mongoose.model('Hero', HeroSchema);
Hero.gameObject = new GameObject({model: Hero});
module.exports = Hero;
