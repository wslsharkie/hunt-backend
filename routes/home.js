var express = require('express');
var homeRouter = express.Router();
var homeController = require('../controllers/home');
var authController = require('../controllers/auth');

homeRouter.route('/?')
    .get(authController.isBearerAuthenticated, homeController.getHome);

module.exports = homeRouter;
