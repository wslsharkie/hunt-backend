"use strict";

var express = require('express');
var alliesRouter = express.Router();
var authController = require('../controllers/auth');
var alliesController = require('../controllers/allies');

alliesRouter.route('/')
    .get(authController.isBearerAuthenticated, alliesController.showAllies);

alliesRouter.route('/invite/:player_id')
    .post(authController.isBearerAuthenticated, alliesController.sendInvite);

alliesRouter.route('/accept/:player_id')
    .post(authController.isBearerAuthenticated, alliesController.acceptInvite);

alliesRouter.route('/remove/:player_id')
    .post(authController.isBearerAuthenticated, alliesController.removePending);

alliesRouter.route('/search/:hash')
    .post(authController.isBearerAuthenticated, alliesController.searchForAlly);

alliesRouter.route('/test/')
    .post(authController.isBearerAuthenticated, alliesController.test);

module.exports = alliesRouter;
