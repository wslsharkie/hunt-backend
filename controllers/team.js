"use strict";

// Load required packages
var Team = require('../models/team');
var Player = require('../models/player');
var Hero = require('../controllers/hero');
var Game = require('../controllers/game');
var Promise = require('bluebird');
var _ = require('lodash');

exports.MAX_MEMBERS = 4;
exports.MAX_TEAMS = 5;
exports.TOTAL_MEMBERS = exports.MAX_MEMBERS * exports.MAX_TEAMS;

exports.setTeams = function (player, teamsArray) {
    if(teamsArray === null || teamsArray.length === 0 || teamsArray.length > exports.MAX_TEAMS) {
        throw new Error('Invalid teams!');
    }

    _.each(teamsArray, function (team) {
        if(team.length !== exports.MAX_MEMBERS) {
            throw new Error('Invalid team size!');
        }
    });

    return Hero.get(player)
            .then(function (heroes) {
            // check player has heroes
            _.each(teamsArray, function (team) {
                _.each(team, function (heroId) {
                    if(heroId !== -1 && heroes.hasOwnProperty(heroId) === false) {
                        throw new Error('Invalid team member '+heroId+'!');
                    }
                });
            });

            return Team.gameObject.get(player._id)
                .then(function(playerTeam) {
                    if(playerTeam === null) {
                        var instance = {
                            ownerId:        player._id,
                            teams:          teamsArray,
                            activeTeamId:   0,
                            createdAt:      Date.now()
                        };

                        var newTeam = new Team(instance);

                        return Team.gameObject.set(newTeam)
                            .then(function(newTeam) {
                                return newTeam;
                            });
                    }

                    playerTeam.teams = teamsArray;
                    playerTeam.createdAt = Date.now();

                    return Team.gameObject.update(playerTeam)
                        .then(function() {
                            return playerTeam;
                        });
                })
                .catch(function(err) {
                    //console.log(err);
                    throw new Error(err.message);
                });
        })
};

exports.setActiveTeam = function(player, activeTeamId) {
    activeTeamId = parseInt(activeTeamId);

    if(activeTeamId < 0 || activeTeamId >= exports.MAX_TEAMS) {
        throw new Error('Can not set active team to this value!');
    }

    return Team.gameObject.get(player._id)
        .then(function (team) {
            if(team.activeTeamId === activeTeamId) {
                return true;
            }

            team.activeTeamId = activeTeamId;

            return Team.gameObject.update(team);
        })
        .catch(function () {
            throw new Error('No team object exists for this player!');
        });
};

exports.getTeams = function (player) {
    return Team.gameObject.get(player._id)
        .then(function(teamObject) {
            if(teamObject === null) {
                teamObject = exports.setTeams(player, exports.getStarterTeams());
            }

            return Promise.all([teamObject]);
        })
        .spread(function (teams) {
            return teams;
        });
};

// hero instance only
exports.getActiveTeam = function(player) {
    return Promise.all([exports.getActiveTeamArray(player), Hero.get(player)])
        .spread(function (team, heroes) {
            var activeTeam = {};
            _.each(team, function (heroId, index) {
                if(heroId !== -1) {
                    activeTeam[index] = heroes[heroId];
                    //activeTeam[index].images = Hero.getImages(heroId);
                }
            });

            return activeTeam;
        });
};

exports.getActiveTeamArray = function (player) {
    return exports.getTeams(player)
        .then(function (teamObject) {
            return teamObject.teams[teamObject.activeTeamId];
        });
};

// gets the full hero data in a team
exports.getFullActiveTeam = function (player) {
    return exports.getTeams(player)
        .then(function (teamObject) {
            return Promise.all([
                teamObject.teams[teamObject.activeTeamId],
                Hero.get(player)
            ]);
        })
        .spread(function (activeTeam, heroes) {
            var team = {};

            _.each(activeTeam, function (heroId, position) {
                if(heroId === -1) {
                    return;
                }

                team[position] = Hero.makeHeroWithInstance(heroes[heroId]);
            });

            return team;
        });
};

exports.getFullTeamByArray = function (player, teamArray) {
    return Hero.get(player)
        .then(function (heroes) {
            return _.mapValues(teamArray, function (heroId) {
                if(heroId === -1) {
                    return {};
                }
                return Hero.makeHeroWithInstance(heroes[heroId]);
            });
        });
};

exports.getStarterTeams = function () {
    var emptyTeam = [-1,-1,-1,-1];
    var teams = [];

    for(var i = 0; i < exports.MAX_TEAMS; i++) {
        if(i === 0) {
            var starter = _.clone(Hero.starterHeroes);
            while(starter.length < exports.MAX_MEMBERS) {
                starter.push(-1);
            }
            teams.push(starter);
        } else {
            teams.push(emptyTeam);
        }

        if(teams[i].length > exports.MAX_MEMBERS) {
            teams[i] = teams[i].slice(0,exports.MAX_MEMBERS);
        }
    }

    return teams;
};

exports.findHeroPosition = function(team, heroId) {
    var position = -1;

    _.each(team, function (member, key) {
        if(heroId.toString() === member.config.id.toString()) {
            position = key;
            return false;
        }
    });

    return position;
};

exports.getAverageTeamLevel = function(player) {
    return Promise.all([exports.getActiveTeamArray(player), Hero.get(player)])
        .spread(function (team, heroes) {
           var level = 0;
           var membersCount = 0;

           _.each(team, function (heroId) {
                if(heroId !== -1) {
                    level += Hero.getLevelForExperience(heroes[heroId].experience);
                    membersCount++;
                }
           });

            return Math.round(level / membersCount);
        });
};

function getTeamsWithHeroes(teamObject, heroes) {
    var data = {};

    _.each(teamObject.teams, function (currentTeam, teamId) {
        var teamData = {};

        _.each(currentTeam, function (heroId, teamPosition) {
            if(heroId === -1) {
                return;
            }

            if(heroes.hasOwnProperty(heroId) === false) {
                // should never happen!
                throw new Error('Player does not have hero '+heroId+' in team!');
            }

            teamData[teamPosition] = Hero.makeHeroWithInstance(heroes[config.id]);
        });

        data[teamId] = teamData;
    });

    return data;
};

// for supporting multiple teams
// currently not being used
exports.playerTeamKey = function(playerId, teamId) {
    return playerId + ':' + teamId;
};

function parseTeamsString(string) {
    var teams = [];
    var team = [];

    var data = string.split(':');

    if(data.length === 0 || data.length > exports.TOTAL_MEMBERS) {
        return teams;
    }

    _.each(data, function (heroId) {
        heroId = parseInt(heroId);

        var hero = Hero.getHeroConfig(heroId);

        if(hero === null) {
            heroId = -1; // empty slot
        }

        team.push(heroId);

        if(team.length === exports.MAX_MEMBERS) {
            teams.push(_.cloneDeep(team));
            team = [];
        }
    });

    return teams;
}

/*** ENDPOINTS ***/
exports.postTeams = function(req, res) {
    var player = req.player;

    var bodyParams = ['teams', 'active'];
    _.each(bodyParams, function (param) {
        if(req.body.hasOwnProperty(param) === false) {
            return res.send({ message: 'Missing param '+param+'!' });
        }
    });

    // for example:
    // '0:1:2' set team heroId 0 at position 0, heroId 1 at position 1, 2 at 2
    var teams = parseTeamsString(req.body.teams);
    var activeTeamId = parseInt(req.body.active);
    var teamObject = null;

    exports.setTeams(player, teams)
        .then(function (newTeam) {
            teamObject = newTeam;
            return exports.setActiveTeam(player, activeTeamId);
        })
        .then(function () {
            return res.send({team: teamObject});
        })
        .catch(function (err) {
            return res.send({message: err});
        });
};

exports.showTeams = function(req, res) {
    var player = req.player;

    exports.getTeams(player)
        .then(function (team) {
            return res.send({team: team});
        })
        .catch(function(err) {
            //console.log(err);
            return res.send({ error: 'Failed to find Team!' });
        });
};

exports.showTeamsAndHeroes = function (req, res) {
    var player = req.player;

    return exports.getTeams(player)
        .then(function (teamObject) {
            return Promise.all([teamObject, Hero.clientHeroes(player)]);
        })
        .spread(function (team, heroes) {
            return res.send({ team: team, heroes: heroes});
        })
        .catch(function(err) {
            //console.log(err);
            return res.send({ error: err.message });
        });
};

exports.resetTeams = function(req, res) {
    var player = req.player;
    Team.gameObject.delete({name: 'ownerId', value: player._id})
        .then(function () {
            return res.send({message: 'Team reset!'});
        });
};
