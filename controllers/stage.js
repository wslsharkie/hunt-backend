"use strict";

var Stage = require('../models/stage');
var Game = require('../controllers/game');
var Battle = require('../controllers/battle');
var BattleModel = require('../models/battle');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

exports.stages = {};

exports.create = function(playerId, stageId, battleId) {
    if(exports.getStageBattleConfig(stageId, battleId) === null) {
        throw new Error(stageId+' stage '+battleId+' battle does not exist!');
    }

    var instance = {
        stageBattle: exports.stageBattleKey(stageId, battleId),
        ownerId:     playerId
    };

    return new Stage(instance);
};

exports.getStage = function(playerId, stageId, battleId) {
    var field = exports.stageBattleKey(stageId, battleId);

    return Stage.gameObject.hmGet(playerId, field)
        .then(function (playerStage) {
            if(playerStage === null) {
                return exports.create(playerId, stageId, battleId);
            }

            return playerStage;
        })
};

exports.hasStage = function(playerId, stageId, battleId) {
    var field = exports.stageBattleKey(stageId, battleId);

    return Stage.gameObject.hmGet(playerId, field)
        .then(function (playerStage) {
            return playerStage !== null;
        });
};

exports.getAll = function (playerId) {
    return Stage.gameObject.hGetAll(playerId);
};

exports.stageBattleKey = function(stageId, battleId) {
    return stageId.toString()+':'+battleId.toString();
};

exports.getStageBattleConfig = function(stageId, battleId) {
    var stage = null;
    var battle = null;

    if(exports.stages.hasOwnProperty(stageId)) {
        stage = exports.stages[stageId];
    }

    if(stage !== null && typeof stage.battles[battleId] !== 'undefined') {
        battle = stage.battles[battleId];
    }

    return battle;
};

exports.completeStageBattle = function(player, stageId, battleIndex) {
    if(exports.getStageBattleConfig(stageId, battleIndex) === null) {
        throw new Error(stageId+' stage '+battleIndex+' battle does not exist!');
    }

    var field = exports.stageBattleKey(stageId, battleIndex);

    return exports.getStage(player._id, stageId, battleIndex)
        .then(function (playerStage) {
            playerStage.completed++;
            return Stage.gameObject.hSet(player._id, field, playerStage);
        });
};

exports.checkStageUnlocked = function(playerId, stageId, battleIndex) {
    stageId = parseInt(stageId);
    battleIndex = parseInt(battleIndex);

    if(stageId === 0 && battleIndex <= 0) {
        return true;
    }

    var reqStage = 0;
    var reqQuest = 0;
    var reqCompletions = 0;

    // check the last stage if battle index is the first of the stage
    if(exports.stages[stageId].battles[battleIndex].hasOwnProperty("reqStage")
        && exports.stages[stageId].battles[battleIndex].hasOwnProperty("reqQuest")) {
        reqStage = exports.stages[stageId].battles[battleIndex].reqStage;
        reqQuest = exports.stages[stageId].battles[battleIndex].reqQuest;
        reqCompletions = exports.stages[stageId].battles[battleIndex].reqCompletions;
    } else {
        console.log("checkStageUnlock for stage"+ stageId + " and quest=" + battleIndex + " has invalid requirements");
        return false;
    }

    return exports.hasStage(playerId, reqStage, reqQuest)
        .then(function (hasStage) {
            if(hasStage) {
                return exports.getStage(playerId, reqStage, reqQuest);
            }

            throw new Error('Player does not have this stage!');
        })
        .then(function (stage) {
            return stage.completed >= reqCompletions;
        })
        .catch(function (err) {
            return false;
        });
};

exports.findStageBattle = function(stageBattles, stageBattleKey) {
    var stageBattle = null;
    var results = _.filter(stageBattles, { 'stageBattle': stageBattleKey });

    if(results.length > 0) {
        stageBattle = results[0];
    }

    return stageBattle;
};

exports.init = function() {
    var path = Game.paths.configPath+'/stages/stages.json';
    exports.stages = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.stages);
};

/*** ENDPOINTS ***/

exports.showStage = function(req, res) {
    var player = req.player;

    if(req.params.stage_id === undefined) {
        return res.send({error: "Stage ID does not exist!"});
    }

    var stageId = req.params.stage_id.toString();

    if(exports.stages.hasOwnProperty(stageId)) {
        return res.send({stage: exports.stages[stageId]});
    }

    return res.send({error: "Stage ID does not exist!"});
};

exports.showStages = function (req, res) {
    var player = req.player;
    var stages = {};
    var nextStageLocked = false;

    exports.getAll(player._id)
        .then(function (stageBattles) {
            _.each(exports.stages, function (stageConfig) {
                stages[stageConfig.id] = {};

                for (var battleIndex = 0; battleIndex < stageConfig.battles.length; battleIndex++) {
                    var stageBattleKey = exports.stageBattleKey(stageConfig.id, battleIndex);
                    var stageBattle = exports.findStageBattle(stageBattles, stageBattleKey);
                    var requiredStageBattle = null;

                    if (stageConfig.battles[battleIndex].hasOwnProperty("reqStage") && stageConfig.battles[battleIndex].hasOwnProperty("reqQuest"))
                    {
                        requiredStageBattle = exports.findStageBattle(stageBattles, exports.stageBattleKey(stageConfig.battles[battleIndex].reqStage, stageConfig.battles[battleIndex].reqQuest));
                    }

                    if (stageBattle) {
                        stages[stageConfig.id][battleIndex] = stageBattle;
                        delete stages[stageConfig.id][battleIndex]['_id'];
                    } else if (requiredStageBattle !== null)
                    {
                        if (requiredStageBattle.completed >= stageConfig.battles[battleIndex].reqCompletions)
                        {
                            stages[stageConfig.id][battleIndex] = {};
                            stages[stageConfig.id][battleIndex] = {locked: false};
                        }
                    }
                    else {

                        if (stageConfig.id === 0 && battleIndex === 0) {
                            stages[stageConfig.id][battleIndex] = {};
                            stages[stageConfig.id][battleIndex] = {locked: false};
                        }
                    }
                }
            });

            return res.send({stages: stages});
        })
        .catch(function (err) {
            //console.log(err);
            return res.send({error: 'Failed to get stages!'});
        });
};

exports.startBattle = function (req, res) {
    var player = req.player;
    var battle, playerStage;

    var routeProps = ['stage_id', 'battle_id'];

    _.map(routeProps, function (prop) {
        if(!req.params.hasOwnProperty(prop)) {
            return res.send({error: "Can not start battle without "+prop});
        }
    });

    var stageId = req.params.stage_id;
    var battleId = req.params.battle_id;

    var stageBattle = exports.getStageBattleConfig(stageId, battleId);

    if(stageBattle === null) {
        return res.send({error: "Stage battle does not exist!"});
    }

    if(Game.DEBUG === false && player.energy < stageBattle.energy) {
        return res.send({error: "Energy"});
    }

    Player.gameObject.lock(player._id)
        .then(function (lockedPlayer) {
            player = lockedPlayer;

            if(Game.DEBUG === false && player.energy < stageBattle.energy) {
                throw new Error("Energy");
            }

            // check if previous battle is completed
            return exports.checkStageUnlocked(player._id, stageId, parseInt(battleId));
        })
        .then(function (stageUnlocked) {
            if(!stageUnlocked) {
                throw new Error("Player stage is locked!");
            }

            return exports.getStage(player._id, stageId, battleId);
        })
        .then(function (ps) {
            playerStage = ps;
            player.subProp('energy', stageBattle.energy);
            playerStage.energy += stageBattle.energy;

            // create battle and save
            var playerOptions = {stageBattle: exports.stageBattleKey(stageId, battleId)};
            return Battle.create(player, stageBattle.battleId, playerOptions);
        })
        .then(function (newBattle) {
            battle = newBattle;
            return Stage.gameObject.hSet(player._id, exports.stageBattleKey(stageId, battleId), playerStage);
        })
        .then(function() {
            return Player.gameObject.unlock(player);
        })
        .then(function () {
            return res.send({
                player: PlayerController.clientPlayer(player),
                battle: battle,
                images: Battle.getImages(battle.instance)
            });
        })
        .catch(function (err) {
            // delete battle if something goes wrong!
            if(battle) {
                BattleModel.gameObject.delete({name: '_id', value: battle.instance._id});
            }

            Player.gameObject.revert(player);
            return res.send({error: err.message, player: player});
        });
};


