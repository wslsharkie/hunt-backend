"use strict";

var Game = require('../controllers/game');
var jsonfile = require('jsonfile');

exports.minions = {};

exports.init = function() {
    var path = Game.paths.configPath+'/minions/minions.json';
    exports.minions = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.minions);
};

exports.getMinionConfig = function(minionId) {
  if(exports.minions.hasOwnProperty(minionId)) {
      return exports.minions[minionId];
  }

  return null;
};

exports.getImages = function (minionId) {
    if(exports.minions.hasOwnProperty(minionId)) {
        var images = {};
        var image = exports.getImage(minionId);
        images[image] = {image: image};
        return images;
    }

    return null;
};

exports.getImage = function(minionId) {
    if(exports.minions.hasOwnProperty(minionId)) {
        return exports.minions[minionId].image;
    }

    return null;
};

exports.getMinionHealthMaxByConfig = function (config) {
    return Math.ceil(config.healthScaling * config.level) + config.health;
};
