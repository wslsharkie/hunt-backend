"use strict";

var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var Team = require('../controllers/team');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var _ = require('lodash');

// generate a list of players
// get random players of the same level range
// client will store this list in cache to randomize

exports.RANGE_BASE = 1.25;
exports.LIST_KEY = 'arena-list-02217-';
exports.LIST_MAX = 50;
exports.POOL_MAX = 20;


exports.getScoreFor = function (player_id) {
    return redis.zscore(exports.LIST_KEY, player_id)
        .then( function (results) {
            console.log("arenaList getScore results=");
            console.log(results);
            return results;
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to get arenaRank score for player=" + player_id.toString());
        });
};

exports.setScoreFor = function (player_id, score) {
    return redis.zadd(exports.LIST_KEY, score, player_id)
        .then( function (results) {
            console.log("arenaList setScore results=");
            console.log(results);
            return results;
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to set arenaRank score for player=" + player_id.toString());
        });
};

exports.incrementScoreFor = function(player_id, points) {
    return redis.zincrby(exports.LIST_KEY, points, player_id)
        .then( function (results) {
            console.log("arenaList incrby results=");
            console.log(results);
            return results;
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to set arenaRank score for player=" + player_id.toString());
        });
};

exports.getRankFor = function(player_id)
{
    return redis.zrevrank(exports.LIST_KEY, player_id)
        .then( function (results) {
            console.log("arenaList getRankFor results=");
            console.log(results);
            return results;
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to get arenaRank rank for player=" + player_id.toString());
        });
};

exports.getCountFor = function()
{
    return redis.zcount(exports.LIST_KEY)
        .then( function (results) {
            console.log("arenaList getCount count=");
            console.log(results);
            return results;
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to get arenaRank count for player=" + player_id.toString());
        });
};

exports.getArenaRanges = function(playerRank, playerScore, listCount) {
    //the goal is to grab three ranges of possible opponents from the list, and select a random few from each range.
    // The three ranges are to find players who are higher ranked, roughly equally ranked, and lower ranked than the searching Player

    if (Number.isInteger(playerScore) === false) {
        playerScore = 0;
    }
    if (Number.isInteger(listCount) === false) {
        listCount = 0;
    }

    if (Number.isInteger(playerRank) === false) {
        playerRank = listCount;
    }
    var poolHi = 20;//Math.min(Math.round(listCount/100), 20); //grab at most 20 who are higher ranked
    var poolMid = 50;//Math.min(Math.round(listCount/100), 50); //grab at most 50 who are equally ranked
    var poolLow = 30;//Math.min(Math.round(listCount/100), 30); //grab at most 30 who are lower ranked

    var randOffset = Math.round((Math.random() * 600) - 300);
    var offsetHi = Math.min(Math.ceil(playerRank/10), 3000) + randOffset;  //at most 3000 ranks higher, but hopefully rouhgly 10% higher in rank than the player

    randOffset = Math.round((Math.random() * 500) - 250);
    var offsetMid = Math.min(Math.ceil(playerRank/50), 400) + randOffset;  // at most 200 ranks different, but hopefully roughly within 2% of the player's rank

    randOffset = Math.round((Math.random() * 800) - 400);
    var offsetLow = Math.min(Math.ceil(playerRank/10), 2000) + randOffset; // at most 2000 ranks lower, but hopefully roughly 10% lower in rank than the player


    var rangeHi = { min : (playerRank + offsetHi) - Math.ceil(poolHi / 2),  //now calculate the range start/end using player's rank and our previously created pool size and offsets
        max : (playerRank + offsetHi) + Math.ceil(poolHi / 2)};

    rangeHi.min = Math.max(rangeHi.min, 0);
    rangeHi.max = Math.max(rangeHi.max, poolHi);

    var rangeMid = {min : (playerRank - offsetMid) - Math.ceil(poolMid / 2),
        max : (playerRank - offsetMid) + Math.ceil(poolMid / 2)};

    rangeMid.min = Math.max(rangeMid.min, 0);
    rangeMid.max = Math.max(rangeMid.max, poolMid);

    var rangeLow = {min : (playerRank - offsetLow) - Math.ceil(poolLow / 2),
        max : (playerRank - offsetLow) + Math.ceil(poolLow / 2)};

    rangeLow.min = Math.max(rangeLow.min, 0);
    rangeLow.max = Math.max(rangeLow.max, poolLow);


    console.log([rangeLow, rangeMid, rangeHi]);
    return [rangeLow, rangeMid, rangeHi];
};

/*** ENDPOINTS ***/
exports.getArenaList = function(req, res) {
    var player = req.player;
    var playerIds = [];
    var scores = {};
    var players = [];

    // console.log(player)

    Promise.all([exports.getRankFor(player._id),
        exports.getScoreFor(player._id),
        redis.zcount(exports.LIST_KEY)])
        .spread( function (playerRank, playerScore, listCount) {
            var ranges = exports.getArenaRanges(playerRank, playerScore, listCount);

            return Promise.all([
                redis.zrevrangewithscores(exports.LIST_KEY, ranges[0].min, ranges[0].max),
                redis.zrevrangewithscores(exports.LIST_KEY, ranges[1].min, ranges[1].max),
                redis.zrevrangewithscores(exports.LIST_KEY, ranges[2].min, ranges[2].max)]);
        })
        .spread( function (listLow, listMid, listHi) {
            var max = [3, 5, 5];
            listLow = _.shuffle(listLow);
            listMid = _.shuffle(listMid);
            listHi = _.shuffle(listHi);
            var lists = [listLow, listMid, listHi];

            for(var i=0; i<lists.length;i++) {
                var memberCount = 0;

                _.each(lists[i], function (member) {
                    if(member.id !== player._id.toString() && memberCount < max[i]) {
                        scores[member.id] = member.score;
                        memberCount++;
                    } else {
                        return false;
                    }
                });
            }

            // console.log("getArenaList route results = ");
            //console.log(returnList);

            // testing
            // redis.zrevrangewithscores(exports.LIST_KEY, 0, 1000000)
            //     .then(function (result) {
            //         console.log(result)
            //         return res.send(returnList);
            //     });

            playerIds = Object.keys(scores);

            var playerPromises = [];
            _.each(playerIds, function (playerId) {
                playerPromises.push(PlayerController.get(playerId));
            });

            return Promise.all(playerPromises);
        })
        .then(function (pArray) {
            players = pArray;

            var teamPromises = [];
            _.each(playerIds, function (playerId, index) {
                var player = players[index];
                teamPromises.push(Team.getActiveTeam(player));
            });
            return Promise.all(teamPromises);
        })
        .then(function (tArray) {
            var result = {};

            _.each(playerIds, function (playerId, index) {
                var player = players[index];
                var team = tArray[index];

                result[playerId] = {
                    name: player.name,
                    level: PlayerController.getLevel(player),
                    score: scores[playerId],
                    team: team
                }
            });

            return res.send({list: result});
        })
        .catch( function (err) {
            // console.log(err);
            // console.log("Encountered error above while getting ArenaList for player");
            return res.send({error: 'Failed to get arena list!'});
        });
};



