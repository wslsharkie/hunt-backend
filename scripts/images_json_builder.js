var _         = require('lodash');
var fs        = require('fs');

var fileString = '{\n';
fs.readdirSync(__dirname+"/../public/images").forEach(function(file) {
    //console.log(file)

    if(file === '.DS_Store') {
        return;
    }

    fileString += '"'+file+'": {\n"image": "'+file+'"\n},\n';
});

// remove trailing comma
fileString = fileString.substring(0, fileString.length - 2);
//close off json string
fileString += '\n}';

fs.writeFile('images.json', fileString,  function(err) {
    if (err) {
        return console.error(err);
    }

    console.log("Data written successfully!");
});
