var express = require('express');
var heroRouter = express.Router();
var authController = require('../controllers/auth');
var heroController = require('../controllers/hero');

heroRouter.route('/')
    .get(authController.isBearerAuthenticated, heroController.showHeroes);

heroRouter.route('/:hero_id')
    .get(authController.isBearerAuthenticated, heroController.showHero);

heroRouter.route('/starter')
    .post(authController.isBearerAuthenticated, heroController.addStarterHeroes);

heroRouter.route('/summon/:hero_id')
    .post(authController.isBearerAuthenticated, heroController.summonHero);

heroRouter.route('/stars/:hero_id')
    .post(authController.isBearerAuthenticated, heroController.incrementStar);

// testing:
heroRouter.route('/reset')
    .post(authController.isBearerAuthenticated, heroController.resetHeroes);

module.exports = heroRouter;
