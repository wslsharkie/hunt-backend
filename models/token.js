// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Define our token schema
var TokenSchema   = new mongoose.Schema({
  value: { type: String, required: true, unique: true },
  playerId: { type: String, required: true, unique: true },
  clientId: { type: String, required: true, unique: true },
  expiration: { type: Date, required: true }
});

// Execute before each client.save() call
TokenSchema.pre('save', function(callback) {
  var token = this;

  if (!token.isModified('value')) return callback();

  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(token.value, salt, null, function(err, hash) {
      if (err) return callback(err);
      token.value = hash;
      callback();
    });
  });
});

TokenSchema.pre('findOne', function(callback){
  var query = this;

  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(query._conditions.value, salt, null, function(err, hash) {
      if (err) return callback(err);
      query._conditions.value = hash;
      callback();
    });
  });
});

TokenSchema.methods.verifyToken = function(value) {
  return bcrypt.compareSync(value, this.value);
};

// Export the Mongoose model
module.exports = mongoose.model('Token', TokenSchema);