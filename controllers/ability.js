"use strict";

// Load required packages
var Battle = require('../controllers/battle');
var Hero = require('../controllers/hero');
var Game = require('../controllers/game');
var Player = require('../controllers/player');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

exports.abilities = {};

exports.TYPE = {
    DAMAGE: 0,
    HEAL:   1,
    BUFF:   2,
    DEBUFF: 3,
    STATUS: 4,
    DRAIN:  5,
    PIERCE: 6,
    MISC:   7
};

exports.init = function() {
    var path = Game.paths.configPath+'/abilities/abilities.json';
    exports.abilities = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.abilities);
};

/*
 instance is a pointer to the battle snapshot!

 // actor - the one doing the ability
 // target - the target
 // damageData - damage object used to track battle info for this attack
 // battleTeam - player's team loadout for this battle
 */
exports.applyAbilityByType = function (type, battleObjects) {
    var abilities = getAbilitiesByType(type, battleObjects.actor);

    // apply each ability
    _.each(abilities, function (ability) {
        if(exports.hasOwnProperty(ability.applyFunction)) {
            var abilityFunction = exports[ability.applyFunction];

            if(typeof abilityFunction === "function") {
                var fnParams = [ability, battleObjects];
                abilityFunction.apply(null, fnParams);
            }
        }
    });
};

// activate all abilities on status of the actor and target
exports.activateAbilityByType = function (type, battleObjects) {
    var status = battleObjects.status;
    var actorStatus, targetStatus;
    if(battleObjects.side === "heroes") {
        actorStatus = status["heroes"][battleObjects.actor.instance.id];
        targetStatus = status["minions"][battleObjects.target.instance.id];
    } else {
        actorStatus = status["heroes"][battleObjects.target.instance.id];
        targetStatus = status["minions"][battleObjects.actor.instance.id];
    }

    var actorAbilities = getStatusAbilitiesByType(type, actorStatus);
    var targetAbilities = getStatusAbilitiesByType(type, targetStatus);
    var abilities = actorAbilities.concat(targetAbilities);

    //console.log(debuffs);
    //console.log(target)

    // apply each status ability
    _.each(abilities, function (ability) {
        if(exports.hasOwnProperty(ability.activateFunction)) {
            var abilityFunction = exports[ability.activateFunction];

            if(typeof abilityFunction === "function") {
                var fnParams = [ability, battleObjects];
                abilityFunction.apply(null, fnParams);
            }
        }
    });
};

function getAbilitiesByType(type, actor) {
    return _.reduce(actor.config.abilities, function (result, abilityId) {
        if(exports.abilities.hasOwnProperty(abilityId)) {
            var ability = exports.abilities[abilityId];

            if(ability.type === type) {
                result.push(ability);
            }

            return result;
        }
    }, []);
}

function getStatusAbilitiesByType(type, status) {
    return _.reduce(Object.keys(status), function (result, abilityId) {
        if(exports.abilities.hasOwnProperty(abilityId)) {
            var ability = exports.abilities[abilityId];

            if(ability.type === type) {
                result.push(ability);
            }
        }

        return result;
    }, []);
}

function getTargetSide(side) {
    var targetSide = "minions";
    if(side === "minions") {
        targetSide = "heroes";
    }

    return targetSide;
}

/*** ABILITY FUNCTIONS ***/

exports.woundEnemy = function (debuff, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var target = battleObjects.target;
    var attacker = battleObjects.actor;
    var side = getTargetSide(battleObjects.side);

    if(status[side][target.instance.id].hasOwnProperty(debuff.id)) {
        //console.log('already wounded!');
        return;
    }

    var roll = _.random(1, 1000);
    var resist = attacker.config.resistance * 1000;

    if(roll < resist) {
        damageData.logs.push("Wound resisted!");
    } else {
        //console.log("wound applied!")
        //console.log(target)
        status[side][target.instance.id][debuff.id] = 1; // true
        damageData.logs.push("Wound!");
    }
};

exports.doWoundEnemy = function (debuff, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var attacker = battleObjects.actor;
    var target = battleObjects.target;
    var targetSide = getTargetSide(battleObjects.side);

    if(status[targetSide][target.instance.id].hasOwnProperty(debuff.id)) {
        damageData.attackerDamage = Math.floor(damageData.attackerDamage * debuff.damageMultiplier);

        damageData.logs.push("Wound "+debuff.damageMultiplier+"x!");

        //console.log("wound damage!")

        // roll to remove status
        var roll = _.random(1, 1000);
        // reducing the resist chance with debuff effectiveness
        var resist = (attacker.config.resistance * 1000) + (debuff.effective * 1000);

        if(roll > resist) {
            delete status[targetSide][target.instance.id][debuff.id];
        }
    }
};

exports.healAlly = function(heal, battleObjects) {
    //console.log('heal')
    var damageData = battleObjects.damageData;
    var team = battleObjects.team;
    var actor = battleObjects.actor;

    // heal target by lowest % health
    var target = null;
    var minHealthPct = 1;

    _.each(team, function (member) {
        var healthPct = member.instance.health / member.instance.healthMax;
        if(healthPct < minHealthPct) {
            minHealthPct = healthPct;
            target = member;
        }
    });

    //console.log(actor)
    //console.log(target)

    if(target) {
        var healAmount = Math.floor(damageData.attackerDamage * heal.multiplier);
        target.instance.health += healAmount;
        target.instance.health = Math.min(target.instance.health, target.instance.healthMax);

        damageData.logs.push(actor.config.name+" healed "+target.config.name+" for +"+healAmount+"!");
    }
};

exports.rallyTeam = function (buff, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;

    _.each(battleObjects.team, function (member) {
        status.heroes[member.instance.id][buff.id] = 0;
    });

    damageData.logs.push("Rally!");
};

exports.applyRelentless = function (ability, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var actor = battleObjects.actor;
    
    status[battleObjects.side][actor.instance.id][ability.id] = 0;

    damageData.logs.push(actor.config.name+" activated relentless!");
};

exports.relentlessBuff = function (ability, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var actor = battleObjects.actor;

    if(status[battleObjects.side][actor.instance.id].hasOwnProperty(ability.id)) {
        damageData.attackerDamage = Math.floor(damageData.attackerDamage * ability.damageMultiplier);

        status[battleObjects.side][actor.instance.id][ability.id]++;

        if(status[battleObjects.side][actor.instance.id][ability.id] === ability.turns) {
            delete status[battleObjects.side][actor.instance.id][ability.id];
        }

        damageData.logs.push(actor.config.name+" attacked relentlessly!");
    }
};

exports.blockForTeam = function (ability, battleObjects) {
    //console.log('block');
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var actor = battleObjects.actor;

    _.each(battleObjects.team, function (member) {
        if(member.instance.id !== actor.instance.id) {
            status[battleObjects.side][member.instance.id][ability.id] = {memberId: actor.instance.id};
        } else {
            delete status.heroes[member.instance.id][ability.id];
        }
    });

    damageData.logs.push(actor.config.name+" is blocking for team!");
};

exports.block = function (ability, battleObjects) {
    // if side is minion check hero side for block status
    var statusData = battleObjects.status[Battle.SIDE.HEROES];
    var damageData = battleObjects.damageData;
    var block = null;

    // if side is hero check minion side for block status
    if(battleObjects.side === Battle.SIDE.HEROES) {
        statusData = battleObjects.status[Battle.SIDE.MINIONS];
    }

    // find block ability id
    _.each(statusData, function (status, memberId) {
        // make sure the target is not the blocker
        if(status.hasOwnProperty(ability.id)
            && battleObjects.target.instance.id !== status[ability.id].memberId) {

            block = status[ability.id];
            delete statusData[memberId][ability.id];
        }
    });

    if(block) {
        //console.log('before');
        //console.log(battleObjects.target);

        // find blocker by status member id and have blocker be the new target
        _.each(battleObjects.targetTeam, function (member) {
            //console.log(block.memberId);
            //console.log(member);

            if(block.memberId === member.instance.id) {

                //console.log('new target')
                //console.log(member);

                battleObjects.target = member;
                return false;
            }
        });

        //console.log('after');
        //console.log(battleObjects.target);
        damageData.logs.push(battleObjects.target.config.name+" blocked for team!");
    }
};

exports.rallyBuff = function (buff, battleObjects) {
    var status = battleObjects.status;
    var damageData = battleObjects.damageData;
    var actor = battleObjects.actor;

    damageData.targetDamage = Math.floor(damageData.targetDamage * buff.multiplier);

    status[battleObjects.side][actor.instance.id][buff.id]++;
    if(status[battleObjects.side][actor.instance.id][buff.id] === buff.turns) {
        delete status[battleObjects.side][actor.instance.id][buff.id];
    }
};

