"use strict";

var express = require('express');
var squadRouter = express.Router();
var authController = require('../controllers/auth');
var squadController = require('../controllers/squad');
var sqaudFeed = require('../controllers/squadFeed');
var squadList = require('../controllers/squadList');

squadRouter.route('/')
    .post(authController.isBearerAuthenticated, squadController.getSquadInfo);

squadRouter.route('/create')
    .post(authController.isBearerAuthenticated, squadController.createSquad);

squadRouter.route('/:squad_id/join/?')
    .post(authController.isBearerAuthenticated, squadController.join);

squadRouter.route('/leave')
    .post(authController.isBearerAuthenticated, squadController.leave);

squadRouter.route('/kick')
    .post(authController.isBearerAuthenticated, squadController.kick);

squadRouter.route('/promote')
    .post(authController.isBearerAuthenticated, squadController.promote);

squadRouter.route('/demote')
    .post(authController.isBearerAuthenticated, squadController.demote);

squadRouter.route('/list')
    .get(authController.isBearerAuthenticated, squadList.showList);

squadRouter.route('/feed/post')
    .post(authController.isBearerAuthenticated, sqaudFeed.post);

squadRouter.route('/feed/:squad_id')
    .get(authController.isBearerAuthenticated, sqaudFeed.showFeed);

// testing
squadRouter.route('/generate')
    .post(authController.isBearerAuthenticated, squadList.createDummySquads);

module.exports = squadRouter;
