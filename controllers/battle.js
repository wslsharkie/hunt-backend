"use strict";

// Load required packages
var Battle = require('../models/battle');
var BattleList = require('../controllers/battleList');
var BattleOps = require('../controllers/battleOperations');
var Team = require('../controllers/team');
var Hero = require('../controllers/hero');
var Equipment = require('../controllers/equipment');
var Game = require('../controllers/game');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var Minion = require('../controllers/minion');
var Stage = require('../controllers/stage');
var Reward = require('../controllers/reward');
var Status = require('../controllers/battleStatus');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

exports.TYPE = {};
exports.TYPE.STAGE = 0;
exports.TYPE.RAID = 1;
exports.TYPE.ARENA = 2;

exports.SIDE = {};
exports.SIDE.HEROES = "heroes";
exports.SIDE.MINIONS = "minions";

exports.STATE = {};
exports.STATE.RUNNING = 0;
exports.STATE.COLLECT = 1;

const RECHARGE_GEM_COST = 1;


exports.create = function(player, battleId, optional = {}) {
    var data = {};
    var content = exports.getConfig(battleId);

    if(!content) {
        throw new Error('Battle constant not found!');
    }

    data.config = content;

    return Team.getFullActiveTeam(player)
        .then(function (activeTeam) {
            var members = _.filter(activeTeam, function (hero) {
                return (hero.hasOwnProperty('instance') && hero.hasOwnProperty('config'));
            });

            if(members.length <= 0) {
                throw new Error("Invalid team!");
            }

            var players = {};

            // todo: calculate health max scale formula
            var teamSnapshot = exports.getTeamSnapshot(activeTeam);

            players[player._id] = {
                team: teamSnapshot,
                energy: 0,
                name: player.name
            };

            for(var option in optional) {
                players[player._id][option] = optional[option];
            }

            data.instance = {
                battleId:   data.config.id,
                ownerId:    player._id,
                players:    players,
                minions:    exports.getMinionsTeamSnapshot(data.config),
            };

            if(exports.TYPE.RAID === data.config.type) {
                data.instance.public = true;
            }

            var battle = new Battle(data.instance);
            return Battle.gameObject.set(battle);
        })
        .then(function (battle) {
            data.instance._id = battle._id;

            // add to list
            var listItem = {
                id:         data.instance._id,
                ownerId:    data.instance.ownerId,
                type:       data.config.type,
                expireTime: data.config.expireTime,
                createdAt:  Date.now()
            };

            return BattleList.add(player, listItem);
        })
        .then(function () {
            return data;
        });
};

exports.getBattle = function (id) {
    return Battle.gameObject.get(id)
        .then(function (battle) {
            return battle;
        });
};

exports.makeFullTeamWithConfig = function(battleTeam, config, bonuses = {}) {
    return _.reduce(battleTeam, function (result, member, position) {
        result[position] = {instance: {}, config: {}, bonus: {}};

        if(member.hasOwnProperty('id') && config.hasOwnProperty(member.id)) {
            result[position].instance = member;
            result[position].config = config[member.id];

            if (bonuses.hasOwnProperty(member.id))
            {
                _.each(bonuses[member.id], function(value, stat) {
                    if (result[position].bonus.hasOwnProperty(stat) === false) {
                        result[position].bonus[stat] = 0;
                    }
                    result[position].bonus[stat] += value;
                })
            }
        }

        return result;
    }, {});
};

exports.join = function (player, id) {
    var battle, config, activeTeam;

    return exports.getBattle(id)
        .then(function (checkBattle) {
            if(checkBattle === null) {
                throw new Error('Battle does not exist!');
            }

            config = exports.getConfig(checkBattle.battleId);

            if(!config) {
                throw new Error('Battle config not found!');
            }

            validateJoin(player, checkBattle, config);

            return Battle.gameObject.lock(id);
        })
        .then(function (lockedBattle) {
            battle = lockedBattle;
            return Player.gameObject.lock(player._id);
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return Team.getFullActiveTeam(player);
        })
        .then(function (team) {
            activeTeam = team;

            validateJoin(player, battle, config);

            var attribute = config.join.attribute;
            var joinCost = config.join.amount;


            var teamSnapshot = exports.getTeamSnapshot(activeTeam);

            battle.players[player._id.toString()] = {
                team: teamSnapshot,
                last_atk_pos : 0,
                name: player.name
            };

            // add to list
            var listItem = {
                id:         battle._id,
                ownerId:    battle.ownerId,
                type:       config.type,
                expireTime: config.expireTime,
                createdAt:  Date.now()
            };

            return BattleList.add(player, listItem);
        })
        .then(function () {
            // set public if at max
            if(config.maxPlayers <= Object.keys(battle.players).length) {
                battle.public = false;
            }

            return Promise.all([Battle.gameObject.unlock(battle), Player.gameObject.unlock(player)]);
        })
        .then(function () {
            return battle;
        })
        .catch(function (err) {
            //console.log(err);

            Battle.gameObject.revert(battle);
            Player.gameObject.revert(player);
            throw new Error(err.message);
        });
};

exports.quit = function (player, id) {
    var battle, config;

    return exports.getBattle(id)
        .then(function (checkBattle) {
            if(checkBattle === null) {
                throw new Error('Battle does not exist!');
            }

            config = exports.getConfig(checkBattle.battleId);

            if(!config) {
                throw new Error('Battle config not found!');
            }

            validateQuit(player, checkBattle);

            return Battle.gameObject.lock(id);
        })
        .then(function (lockedBattle) {
            battle = lockedBattle;
            validateQuit(player, battle);

            delete battle.players[player._id.toString()];

            return BattleList.delete(player, battle._id);
        })
        .then(function () {
            if(_.isEmpty(battle.players)) {
                return Battle.gameObject.revert(battle)
                    .then(function () {
                        return Battle.gameObject.delete({name: '_id', value: battle._id});
                    });
            }

            // set public if less than max
            if(config.maxPlayers > Object.keys(battle.players).length) {
                battle.public = true;
            }

            return Battle.gameObject.unlock(battle);
        })
        .catch(function (err) {
            //console.log(err);
            Battle.gameObject.revert(battle);
            throw new Error(err.message);
        });
};

exports.collect = function(player, id) {
    var battle, config;

    return exports.getBattle(id)
        .then(function (checkBattle) {
            if(checkBattle === null) {
                throw new Error('Battle does not exist!');
            }

            config = exports.getConfig(checkBattle.battleId);

            if(!config) {
                throw new Error('Battle config not found!');
            }

            validateCollect(player, checkBattle);

            return Battle.gameObject.lock(id);
        })
        .then(function (lockedBattle) {
            battle = lockedBattle;
            validateCollect(player, battle);
            battle.players[player._id].rewarded = true;
            return BattleList.markRewarded(player, battle._id);
        })
        .then(function () {
            var response = {
                battle:     battle,
                player:     player,
                actions:    {},
                level:      PlayerController.getLevelData(player),
                status:     {},
                victory:    true,
            };

            var team = exports.makeFullTeamWithConfig(battle.players[player._id].team, Hero.heroes);
            var heroesArray = getTeamHeroesArray(team);

            return Reward.rewardPlayer(player, heroesArray, config.rewardsId)
                .then(function (rewards) {
                    response.rewards = rewards;
                    var heroReward = _.filter(response.rewards, { 'type': Reward.TYPE.HERO_XP });
                    return Hero.getLevelPctData(player, heroesArray, heroReward[0].amount);
                })
                .then(function (heroLevels) {
                    response.heroLevels = heroLevels;
                    return Battle.gameObject.unlock(battle);
                })
                .then(function () {
                    checkBattleDelete(battle, config);
                    return response;
                });
        })
        .catch(function (err) {
            //console.log(err);
            Battle.gameObject.revert(battle);
            throw new Error(err.message);
        });
};

function checkTeamDefeated(battle, player_id) {
    // if (battle.players.hasOwnProperty(player_id) === false)
    // {
    //     throw new Error("Player is not in this battle!");
    // }
    var validHeroes = _.filter(battle.players[player_id].team, function (battleHero) {
        return (battleHero.health > 0);
    });

    if(validHeroes.length === 0) {
        return true;
    }

    return false;
}

function checkMinionsDefeated(battle) {
    var activeMinions = _.filter(battle.minions, function (minion) {
        return minion.health > 0;
    });

    if(activeMinions.length > 0) {
        return false;
    }

    return true;
}

/** deprecated for now, may be useful later
function findAttackerRandom(team) {
    var max = _.reduce(team, function (sum, member) {
        if(member.instance.health <= 0) {
            return sum;
        }
        return sum + member.config.speed;
    }, 0);
    var roll = _.random(1, max);
    var rangeMin = 1;
    var rangeMax = 1;
    var attacker = null;

    _.each(team, function (member) {
        if(member.instance.health <= 0) {
            return;
        }

        rangeMax = rangeMin + member.config.speed;

        if(roll >= rangeMin && roll <= rangeMax) {
            attacker = member;
            return false;
        }

        rangeMin = rangeMax + 1;
    });

    return attacker;
}
 **/

function findAttackerOrdered(team, last_attacker_pos) {
    var next_attacker_pos;
    var teamSize = _.size(team);
    var foundAttacker = false;
    var checks = 0;

    if (_.isNil(last_attacker_pos))
    {
        next_attacker_pos = 0;
    } else {
        next_attacker_pos = parseInt(last_attacker_pos) + 1;
    }

    while (foundAttacker === false)
    {
        if (checks > teamSize)
        {
            throw Error("No valid attacker found");
        }

        if (next_attacker_pos >= teamSize)
        {
            next_attacker_pos = 0;
        }

        if (team.hasOwnProperty(next_attacker_pos) && team[next_attacker_pos].instance.health > 0)
        {
            foundAttacker = true;
            break;
        } else {
            next_attacker_pos += 1;
            checks++;
        }
    }

    /** Deprecated for now to replaced by hero_death compatible findAttackerOrdered
    return team[next_attacker_pos]

    if (_.isNil(last_attacker_pos))
    {
        next_attacker_pos = 0;
    } else {
        next_attacker_pos = parseInt(last_attacker_pos) + 1;
    }

    if (next_attacker_pos >= teamSize)
    {
        next_attacker_pos = 0;
    }
     **/

    return team[next_attacker_pos];
}

function findHeroWithID(team, heroId) {
    var attacker = null;

    _.each(team, function (member) {
        if(member.instance.id === parseInt(heroId) && member.instance.health > 0) {
            attacker = member;
            return false;
        }
    });

    return attacker;
}

function findHeroAtPosition(team, position) {
    var attacker = null;

    _.each(team, function (member, pos_key) {
        if(pos_key == position || pos_key == position.toString()) {
            attacker = member;
            return false;
        }
    });

    return attacker;
}

function findMinionTargetPosition(team) {
    var targets = _.reduce(team, function (result, member, position) {
        if(member.instance.health > 0) {
            result.push(position);
        }

        return result;
    }, []);

    // todo: random for now
    if(targets.length > 0) {
        return targets[Math.floor(Math.random()*targets.length)];
    }

    return -1;
}

function findPositionInBattle(id, battleTeam) {
    var position = -1;

    _.each(battleTeam, function (member, memberPosition) {
        if(parseInt(member.instance.id) === parseInt(id)) {
            position = memberPosition;
            return false;
        }
    });

    return position;
}

exports.getMinionsTeamSnapshot = function(config) {
    var minionsIds = [];
    var minionsCount = config.minionsCount;
    var minionsTotal = config.minions.length;

    while(minionsCount > 0) {
        var minionIndex = _.random(0, minionsTotal-1);
        minionsIds.push(config.minions[minionIndex]);
        minionsCount--;
    }

    if(config.hasOwnProperty('minions')) {
         var minions = {};

         _.each(minionsIds, function(minionId, position) {
            var minionConfig = _.clone(Minion.getMinionConfig(minionId));
            minionConfig.level = _.random(config.level[0], config.level[1]);
            var health = Minion.getMinionHealthMaxByConfig(minionConfig);

             minions[position] = {
                id:         minionConfig.id,
                level:      minionConfig.level,
                health:     health,
                healthMax:  health,
                charge:     0,
                action:     0
             };
        });

        return minions;
    }
};

exports.getTeamSnapshot = function(fullTeam) {
    return _.mapValues(fullTeam, function (hero) {
        var health = Hero.getHealthMax(hero);

        return {
            id:         hero.instance.heroId,
            level:      Hero.getLevel(hero),
            stars:      hero.instance.stars,
            health:     health,
            healthMax:  health,
            charge:     0, // amount of charge before executing a special ability
            damage:     0 // damage dealt this battle
        }
    });
};

exports.rechargeTeamSnapshot = function(battle, player_id) {
    if (battle.players.hasOwnProperty(player_id) === false) {
        throw "Player not part of this battle";
    }

    if (battle.players[player_id].hasOwnProperty("team") === false) {
        throw "Player team not found in this battle";
    }

    var fullTeamSnapshot = battle.players[player_id].team;

    fullTeamSnapshot = _.mapValues(fullTeamSnapshot, function (hero) {
        return {
            id:         hero.id,
            level:      hero.level,
            stars:      hero.stars,
            health:     hero.healthMax,
            healthMax:  hero.healthMax,
            charge:     hero.charge, // amount of charge before executing a special ability
            damage:     hero.damage // damage dealt this battle
        }
    });

    battle.players[player_id].team = fullTeamSnapshot;
    return battle;
}

function validateAttack(player, battle, config, position) {
    if(battle.players.hasOwnProperty(player._id) === false) {
        throw new Error("Player is not in this battle!");
    }

    if(battle.minions[position] === undefined ||
        battle.minions[position].health <= 0) {
        throw new Error('No enemy in this position!');
    }

    if (checkTeamDefeated(battle, player._id) === true)
    {
        throw new Error('Your team has been defeated');
    }

    var attribute = config.cost.attribute;
    var attackCost = config.cost.amount;

    if(Game.DEBUG === false && player[attribute] < attackCost) {
        throw new Error('Not enough '+attribute+'!');
    }

    return true;
}

function validateJoin(player, battle, config) {
    if(battle.players.hasOwnProperty(player._id) === true) {
        throw new Error("Player is already in this battle!");
    }

    if(Object.keys(battle.players).length > config.maxPlayers) {
        throw new Error("Max players reached! This battle should not be public!");
    }

    if(battle.public === false) {
        throw new Error("This battle is no longer public!");
    }

    if(Date.now() > battle.date + config.expireTime) {
        throw new Error("This battle has ended!");
    }

    var attribute = config.join.attribute;
    var joinCost = config.join.amount;

    if(Game.DEBUG === false && player[attribute] < joinCost) {
        throw new Error('Not enough '+attribute+'!');
    }
}

function validateQuit(player, battle) {
    if(battle.players.hasOwnProperty(player._id) === false) {
        throw new Error("Player is not in this battle!");
    }

    return true;
}

function validateCollect(player, battle) {
    if(battle.players.hasOwnProperty(player._id) === false) {
        throw new Error("Player is not in this battle!");
    }

    if(battle.players[player._id].hasOwnProperty('rewarded')
        && battle.players[player._id].rewarded === true) {
        throw new Error("Player has already been rewarded for this battle!");
    }

    _.each(battle.minions, function (minion) {
        if(minion.health > 0) {
            throw new Error("Battle is not over!");
        }
    });

    return true;
}

function getMinionConfig(config, id) {
    var minion = null;

    if(exports.TYPE.ARENA === config.type) {
        minion = Hero.getHeroConfig(id);
    } else {
        minion = Minion.getMinionConfig(id);
    }

    return minion;
}

function getTeamHeroesArray(team) {
    return _.reduce(team, function (result, member) {
        if(member.instance.hasOwnProperty('id')) {
            result.push(member.instance.id);
        }

        return result;
    }, []);
}

function checkBattleDelete(battle, config) {
    var currentTime = Date.now();

    if(currentTime > config.expireTime +  battle.createdAt) {
        return Battle.gameObject.delete({name: '_id', value: battle._id});
    }

    var rewarded = true;
    _.each(battle.players, function (p) {
        if(p.hasOwnProperty('rewarded') === false) {
            rewarded = false;
            return false;
        }
    });

    if(rewarded) {
        return Battle.gameObject.delete({name: '_id', value: battle._id});
    }
}

exports.getConfigString = function (battleId) {
    return 'battles/'+battleId;
};

exports.getConfig = function(battleId) {
    var path = Game.paths.configPath+'/battles/'+battleId.toString() + '.json';
    return jsonfile.readFileSync(path);
};

exports.getImages = function(battle) {
    var config = exports.getConfig(battle.battleId);

    if(!config) {
        throw new Error('Battle constant not found!');
    }

    var images = {};

    _.each(battle.players, function (p) {
        if(p.hasOwnProperty('team')) {
            _.each(p.team, function (member) {
                _.merge(images, Hero.getImages(member.id));
            });
        }
    });

    if(config.type === exports.TYPE.ARENA) {
        _.each(battle.minions, function (member) {
            _.merge(images, Hero.getImages(member.id));
        });
    } else {
        _.each(battle.minions, function (member) {
            _.merge(images, Minion.getImages(member.id));
        });
    }

    return images;
};

exports.battleAttack = function (player, battleId, params) {
    var data = {player: {}, enemy: {}}; // player attack and enemy attack
    var team = null;
    var enemyTeam = null;
    var battle = null;
    var status = null;
    var config = null;
    var victory = true; // battle over flag
    var defeat  = false;
    var abilityAction = false;

    if(params.hasOwnProperty('heroId')) {
        abilityAction = true;
    }

    // get the player team
    // check for hero
    // get the battle
    // attack with hero at target
    return exports.getBattle(battleId)
        .then(function (checkBattle) {
            if(checkBattle === null) {
                throw new Error('Battle does not exist!');
            }

            config = exports.getConfig(checkBattle.battleId);

            if(!config) {
                throw new Error('Battle config not found!');
            }

            validateAttack(player, checkBattle, config, params.position);

            return Battle.gameObject.lock(battleId); //return Promise.all([Equipment Battle.gameObject.lock(battleId);
        })
        .then(function (lockedBattle) {
            battle = lockedBattle;
            return Player.gameObject.lock(player._id);
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;

            var equipmentList = [];
            _.each(battle.players[player._id].team, function (heroInfo) {
                equipmentList.push(Equipment.get(player, heroInfo.id));
            });
            return Promise.all(equipmentList);
        })
        .then(function(eqData) {
            validateAttack(player, battle, config, params.position);

            var equipmentBonuses = {};
            _.each(eqData, function(heroEQ) {
                equipmentBonuses[heroEQ.heroId] = Equipment.getBonusesFromData(heroEQ);
            });

            // get the player team snapshot on the battle object
            var playerTeam = exports.makeFullTeamWithConfig(battle.players[player._id].team, Hero.heroes, equipmentBonuses);

            // get the enemy team snapshot on the battle object
            var minionTeam = exports.makeFullTeamWithConfig(battle.minions, Minion.minions);

            return [playerTeam, minionTeam];
        })
        .spread(function (playerTeam, minionTeam) {
            team = playerTeam;
            enemyTeam = minionTeam;
        })
        .then(function () {
            return Status.get(battle, player);
        })
        .then(function (playerBattleStatus) {
            status = playerBattleStatus;
            var attribute = config.cost.attribute;
            var attackCost = config.cost.amount;
            var attackingHero = null;

            player.subProp(attribute, attackCost);

            // record energy spent
            battle.players[player._id].energy += attackCost;

            if(abilityAction) {
                attackingHero = findHeroWithID(team, params.heroId);
            } else {
                attackingHero = findAttackerOrdered(team, battle.players[player._id].last_atk_pos);
            }

            if(attackingHero === null) {
                throw new Error('No heroes can attack! All heroes are KO?');
            }

            if (abilityAction && attackingHero.instance.health <= 0)
            {
                throw new Error("Cannot use abilities of a KO'd heroes");
            }

            var heroId = attackingHero.config.id;
            var heroPosition = findPositionInBattle(heroId, team);

            if(heroPosition === -1) {
                throw new Error('Hero not in team!');
            }

            var enemyPosition = params.position;

            var minion = findHeroAtPosition(enemyTeam, enemyPosition);

            /** Deprecated for now, trying findHeroAtPosition for now
            var minion = {
                config: getMinionConfig(config, battle.minions[enemyPosition].id),
                instance: battle.minions[enemyPosition],
                bonus : {}
            };
             **/

            // get damage data calculations
            var battleParams = {
                player:         player,
                actor:          attackingHero,
                target:         minion,
                team:           team,
                targetTeam:     enemyTeam,
                battle:         battle,
                status:         status,
                side:           exports.SIDE.HEROES,
                abilityAction:  abilityAction
            };

            var resultsData = BattleOps.getResults(battleParams);

            // increase minion action charge
            minion.instance.action++;

            // battle actions response data
            data.player.id = heroId;
            data.player.targetId = minion.instance.id;
            data.player.position = heroPosition;
            data.player.targetPosition = enemyPosition;
            data.player.damage = resultsData.attackerDamage;
            data.player.logs = resultsData.logs;
            data.player.ability = resultsData.ability;

            // record damage done by hero
            battle.players[player._id].team[heroPosition].damage += resultsData.attackerDamage;
            battle.players[player._id].last_atk_pos = heroPosition;
        })
        .then(function () {
            // select current target minion to attack
            var attackingMinion = enemyTeam[data.player.targetPosition];

            if(attackingMinion.health < 0) {
                return;
            }

            // only attack if action charge reaches max
            if(exports.TYPE.ARENA !== config.type
                && attackingMinion.instance.action <= attackingMinion.config.actionMax) {
                delete data.enemy;
                return;
            }

            // reset action charge counter
            attackingMinion.instance.action = 0;

            var minionId = attackingMinion.instance.id;
            var minionPosition = findPositionInBattle(minionId, enemyTeam);

            if(minionPosition === -1) {
                throw new Error('Minion not in team!');
            }

            var enemyPosition = findMinionTargetPosition(team);

            if(enemyPosition === -1) {
                throw new Error('No valid player team hero target for minion!');
            }

            // get damage data calculations
            var battleParams = {
                player:         player,
                actor:          attackingMinion,
                target:         team[enemyPosition],
                team:           enemyTeam,
                targetTeam:     team,
                battle:         battle,
                status:         status,
                side:           exports.SIDE.MINIONS,
                abilityAction:  false
            };

            var resultsData = BattleOps.getResults(battleParams);

            // battle actions response data
            data.enemy.id = attackingMinion.instance.id;
            data.enemy.targetId =  team[enemyPosition].instance.id;
            data.enemy.position = minionPosition;
            data.enemy.targetPosition = enemyPosition;
            data.enemy.damage = resultsData.attackerDamage;
            data.enemy.logs = resultsData.logs;
            data.enemy.ability = resultsData.ability;
        })
        .then(function () {

            victory = checkMinionsDefeated(battle);

            if (!victory)
            {
                defeat = checkTeamDefeated(battle, player._id)
            }

        })
        .then(function () {
            // mark stageBattle completed
            if(victory && battle.players[player._id].hasOwnProperty('stageBattle')) {
                var sbString = battle.players[player._id]['stageBattle'].split(":");
                return Stage.completeStageBattle(player, sbString[0], sbString[1]);
            }
        })
        .then(function () {
            if(victory) {
                battle.state = exports.STATE.COLLECT;
                battle.players[player._id].rewarded = true;
                return BattleList.markRewarded(player, battle._id);
            }
        })
        .then(function () {
            // rewards
            var response = {
                battle:     battle,
                //team:       team,
                player:     PlayerController.clientPlayer(player, ['_id']),
                actions:    data,
                status:     status,
                victory:    victory,
                defeat :    defeat,
            };

            if(victory && config.type !== exports.TYPE.ARENA) {
                battle.players[player._id].rewarded = true;

                var heroesArray = getTeamHeroesArray(team);
                return Reward.rewardPlayer(player, heroesArray, config.rewardsId)
                    .then(function (rewards) {
                        response.rewards = rewards;
                        var heroReward = _.filter(response.rewards, { 'type': Reward.TYPE.HERO_XP });
                        return Hero.getLevelPctData(player, heroesArray, heroReward[0].amount);
                    })
                    .then(function (heroLevels) {
                        response.heroLevels = heroLevels;
                        return response;
                    });
            }

            if(config.type === exports.TYPE.ARENA) {
                if(victory || checkTeamDefeated(team) === true) {
                    return Reward.rewardArena(battle, player, getTeamHeroesArray(team), victory === true)
                        .then(function (rewards) {
                            response.rewards = rewards;
                            return response;
                        });
                }
            }

            return response;
        })
        .then(function (response) {
            if(response.rewards) {
                console.log(response.rewards);
            }
            return Promise.all([
                Battle.gameObject.unlock(battle),
                Player.gameObject.unlock(player),
                Status.set(battle, player, status)
            ]).then(function () {
                checkBattleDelete(battle, config);
                return response;
            });
        })
        .catch(function (err) {
            console.log(err)
            Player.gameObject.revert(player);
            Battle.gameObject.revert(battle);
            throw new Error(err.message);
        });
};

exports.getShowBattlePackage = function(battle, player) {
    var results = { player: player,
        battle: {instance: battle, config: exports.getConfig(battle.battleId)},
        images: exports.getImages(battle)
    };
    if (checkMinionsDefeated(battle, player._id))
    {
        results['victory'] = true;
    } else if (checkTeamDefeated(battle, player._id))
    {
        results['defeat'] = true;
    }
    return results;
}

/*** ENDPOINTS ***/
exports.createBattle = function(req, res) {
    var player = req.player;

    if(req.body.battle_id === undefined) {
        return res.send({error: "Battle ID does not exist!"});
    }

    return exports.create(player, req.body.battle_id)
        .then(function (battle) {
            return res.send({ battle: battle, images: exports.getImages(battle.instance) });
        })
        .catch(function(err) {
            //console.log(err);
            return res.send({error: "Battle does not exist!"});
        });
};

exports.special = function (req, res) {
    var player = req.player;

    if(req.params.object_id === undefined) {
        return res.send({error: "Battle does not exist!"});
    }

    var battleObjectId = req.params.object_id;

    var bodyKeys = ['position', 'hero_id'];

    _.each(bodyKeys, function(key) {
        if(req.body.hasOwnProperty(key) === false) {
            return res.send({error: "Battle attack missing key!"});
        }
    });

    var params = {
        position: req.body.position,
        heroId: req.body.hero_id
    };

    exports.battleAttack(player, battleObjectId, params)
        .then(function (response) {
            return res.send(response);
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: 'Failed to special attack!'});
        });
};

exports.attack = function(req, res) {
    var player = req.player;

    if(req.params.object_id === undefined) {
        return res.send({error: "Battle does not exist!"});
    }

    var battleObjectId = req.params.object_id;

    var bodyKeys = ['position'];

    _.each(bodyKeys, function(key) {
        if(req.body.hasOwnProperty(key) === false) {
           return res.send({error: "Battle attack missing key!"});
       }
    });

    exports.battleAttack(player, battleObjectId, {position: req.body.position})
        .then(function (response) {
            return res.send(response);
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: 'Failed to attack!'});
        });
};

exports.showBattleList = function (req, res) {
    var player = req.player;

    return BattleList.get(player._id)
        .then(function (list) {
            return res.send({ list: list });
        })
        .catch(function(err) {
            //console.log(err);
            return res.send({error: "Battle List does not exist!"});
        });
};

exports.showBattle = function (req, res) {
    var player = req.player;
    var results = {};

    if(req.params.object_id === undefined) {
        return res.send({error: "Battle does not exist!"});
    }

    var battleObjectId = req.params.object_id;

    return exports.getBattle(battleObjectId)
        .then(function (battle) {
            return res.send(exports.getShowBattlePackage(battle, player))
            })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: "Battle does not exist!"});
        });
};

// player must pay energy to enter a battle
exports.joinBattle = function (req, res) {
    var player = req.player;

    if(req.params.object_id === undefined) {
        return res.send({error: "Missing object id!"});
    }

    return exports.join(player, req.params.object_id)
        .then(function (data) {
            return res.send(exports.getShowBattlePackage(data, player));
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: err.message});
        });
};

exports.quitBattle = function(req, res) {
    var player = req.player;

    if(req.params.object_id === undefined) {
        return res.send({error: "Missing object id!"});
    }

    return exports.quit(player, req.params.object_id)
        .then(function () {
            return res.send({ result: "1" });
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: "Failed to quit battle!"});
        });
};

exports.collectBattle = function(req, res) {
    var player = req.player;

    if(req.params.object_id === undefined) {
        return res.send({error: "Missing object id!"});
    }

    return exports.collect(player, req.params.object_id)
        .then(function (result) {
            return res.send(result);
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: "Failed to quit battle!"});
        });
};

exports.deleteBattles = function(req, res) {
    var player = req.player;

    return BattleList.get(player._id)
        .then(function (battleList) {
            _.each(battleList.list, function (listItem) {
                if(listItem.ownerId === player._id) {
                    Battle.gameObject.delete({name: '_id', value: listItem.id});
                }
            });

            return res.send({battles: battleList});
        });
};

exports.payTeamRecharge = function(req, res)
{
    var player = req.player;
    var battle = null;
    if(req.body.battle_id === undefined) {
        return res.send({error: "Missing battle id!"});
    }

    var battle_id = req.body.battle_id;
    return exports.getBattle(battle_id)
        .then(function (checkBattle) {
            if (checkBattle === null) {
                throw new Error('Battle does not exist!');
            }
            battle = checkBattle;

            if (battle.players.hasOwnProperty(player._id) === false) {
                throw "Player not part of this battle";
            }

            if (battle.players[player._id].hasOwnProperty("team") === false)
            {
                throw "Player team not found in this battle";
            }

            if (player.gems < RECHARGE_GEM_COST)
            {
                throw "Sorry, you will need more gems first";
            }

            return Player.gameObject.lock(player._id)
        })
        .then(function(lockedPlayer) {
            player = lockedPlayer;

            if (player.gems < RECHARGE_GEM_COST)
            {
                throw "Sorry, you will need more gems first";
            }
            player.gems -= RECHARGE_GEM_COST;

            return Battle.gameObject.lock(battle_id)
        })
        .then(function(lockedBattle) {
            battle = lockedBattle;

            battle = exports.rechargeTeamSnapshot(battle, player._id)

            return Promise.all([Battle.gameObject.unlock(battle), Player.gameObject.unlock(player)]);
        })
        .then(function() {
            return res.send(exports.getShowBattlePackage(battle, player));
        })
        .catch(function (err) {
            //console.log(err);

            Battle.gameObject.revert(battle);
            Player.gameObject.revert(player);
            return res.send({error: err.message});
        });
}
