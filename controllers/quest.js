"use strict";

// Load required packages
var Hero = require('../models/hero');
var Quest = require('../models/quest');
var Game = require('../controllers/game');
var Player = require('../models/player');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

exports.XP_MULTIPLIER = 3;

exports.statics = {};
exports.statics.quests = {};

exports.doQuest = function(req, res) {
    var player = req.player;

    if(req.params.quest_id === undefined) {
        return res.send({error: "Quest ID does not exist!"});
    }

    exports.getQuest(player, req.params.quest_id)
        .then(function (quest) {
            // validate
           if(player.energy < quest.constants.energyCost) {
               throw new Error("Energy");
           }

           if(quest.instance.energy >= quest.constants.totalEnergy) {
               throw new Error("Quest already completed!");
           }

           if(quest.constants.hasOwnProperty('requiredQuest')) {
               return exports.getQuest(player, quest.constants.requiredQuest)
                   .then(function(requiredQuest) {
                       if(exports.isQuestComplete(requiredQuest)) {
                           return [quest, player];
                       }

                       throw new Error("Required quest not completed!");
                   });
           } else {
               return [quest, player];
           }
        })
        .spread(function (quest, player) {
            return Player.gameObject.lock(player._id.toString())
                .then(function(player) {
                    if(player.energy < quest.constants.energyCost) {
                        throw new Error("Energy");
                    }

                    return [quest, player];
                });
        })
        .spread(function (quest, player) {
            // do quest
            player.subProp('energy', quest.constants.energyCost);
            player.addExperience(player.energy * exports.XP_MULTIPLIER);
            quest.instance.energy += quest.constants.energyCost;

            // save
            var data = {};
            data[quest.constants.id] = quest.instance;

            return Quest.gameObject.hmSet(player._id, data)
                .then(function () {
                    return Player.gameObject.unlock(player);
                })
                .then(function () {
                    return res.send({quest: quest, player: player});
                })
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: err.message, player: player});
        });
};

exports.getQuestConfig = function(questId) {
    if(exports.statics.quests.hasOwnProperty(questId)) {
        return exports.statics.quests[questId];
    }

    return null;
};

exports.getAvailableQuests = function(req, res) {
    var player = req.player;

    // Quest.gameObject.hGetAll(player._id)
    //     .then(function (quests) {
    //         _.reduce()
    //     })

    // _.reduce(exports.statics.quests, function (result, questConfig, questId) {
    //
    //
    // }, quests);
    //
    // return Quest.gameObject.hGetAll(player._id)
    //     .then(function(quests) {
    //         return quests;
    //     });
};

exports.isQuestComplete = function(quest) {
    if(quest.constants.totalEnergy <= quest.instance.energy) {
        return true;
    }

    return false;
};

exports.getQuest = function(player, questId) {
    var data = {};
    var constants = exports.getQuestConfig(questId);

    if(constants === null) {
        return null;
    }

    data.constants = constants;

    return Quest.gameObject.hmGet(player._id, {name: "questId", value: questId})
        .then(function (quest) {
            data.instance = new Quest(quest);
            return data;
        })
        .catch(function (err) {
            var instance = {
                questId: data.constants.id,
                ownerId: player._id,
                energy:  0,
                createdAt: Date.now()
            };

            data.instance = new Quest(instance);

           return data;
        });
};

exports.init = function() {
    var path = Game.paths.configPath+'/quests/quests.json';
    exports.statics.quests = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.statics.quests);
};
