"use strict";

// Load required packages
var Boss = require('../models/boss');
var Game = require('../controllers/game');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');

exports.boss = null;
//exports.model = Boss;

// Create endpoint /api/users for POST
exports.createBoss = function(req, res) {
    console.log('MAKE BOSS!');

    var player = req.player;

    var data = {};
    var content;

    if(req.params.boss_id === undefined) {
        data.error = "This boss does not exist!";
    } else {
        content = getConstants(req.params.boss_id);
    }

    if(content) {
        data.constants = content;

        data.instance = {
            bossId: data.constants.bossId,
            ownerId: player._id.toString(),
            health: data.constants.health,
            attackers: [],
            createdAt: Date.now()
        };

        var mBoss = new Boss(data.instance);

        // mongodb
        Boss.gameObject.set(mBoss).then(function(boss) {
            data.instance._id = boss._id;
            return res.send(data);

        }).catch(function(err) {
            //console.log(err);
            data = { error: "This boss does not exist!" };
            return res.send(data);
        });

    } else {
        data.error = "This boss does not exist!";
        return res.send(data);
    }
};

exports.getBoss = function(req, res) {
    //var player = req.player;

    var data = {};
    var objectId;

    if(req.params.object_id === undefined) {
        data.error = "Invalid params!";
        return res.send(data);
    }

    objectId = req.params.object_id;

    Boss.gameObject.get(objectId).then(function(boss) {
        if(boss === null) {
            data.error = "This object id does not exist!";
            return res.send(data);
        }

        return res.send(makeBossData(boss));

    }).catch(function(err) {
        data.error = "Invalid object id";
        return res.send(data);
    });

};

exports.attackBoss = function(req, res) {
    var player = req.player;
    var objectId = null;
    var boss = null;

    if(req.params.object_id === undefined) {
        return res.send({ error: 'Invalid params!' });
    }

    objectId = req.params.object_id;

    Boss.gameObject.lock(objectId)
        .then(function(mBoss) {
            var boss = instantiate(mBoss);
            var attacker = registerAttacker(player);
            if(attacker === null) {
                return res.send({ error: 'This boss can not be attacked!' });
            }

            var attackerPos = findAttackerPosition(player);
            var playerDamage = getDamage(player);

            // hit boss
            boss.instance.health -= playerDamage;

            // record damage
            attacker.damage += playerDamage;
            boss.instance.attackers[attackerPos] = attacker;

            Boss.gameObject.unlock(mBoss)
                .then(function() {
                    return res.send(boss);
                });
        })
        .catch(function(err) {
            //console.log(err)
            return res.send({ error: 'Invalid object id!' });
        });
};

function getConstants(bossId) {
    var path = Game.paths.configPath+'/bosses/'+bossId.toString() + '.json';
    return jsonfile.readFileSync(path);
}

function instantiate(bossData) {
    exports.boss = makeBossData(bossData);
    return exports.boss;
}

function makeBossData(boss) {
    var data = {};
    data.instance = boss;
    data.constants = getConstants(boss.bossId);
    return data;
}

function getDamage(player) {
    var attack = player.getAttack();
    return Math.floor(Math.pow(1.05,attack));
}

function findAttackerPosition(player) {
    var boss = exports.boss;

    for(var i=0; i < boss.instance.attackers.length; i++) {
        var attacker = boss.instance.attackers[i];
        if(attacker.playerId === player._id.toString()) {
            return i;
        }
    }

    return -1;
}

function registerAttacker(player) {
    var boss = exports.boss;

    if(boss.constants.max_attackers === boss.instance.attackers.length) {
        return null;
    }
    var attacker;
    var index = findAttackerPosition(player);

    if(index !== -1) {
        attacker = boss.instance.attackers[index];
        return attacker;
    }

    /*
     attacker = {playerId: player._id, damage: 0};
     */
    attacker = {playerId: player._id, damage: 0};
    boss.instance.attackers.push(attacker);
    return attacker;
}
