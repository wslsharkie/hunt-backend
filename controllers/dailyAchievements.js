"use strict";

// Load required packages
var dailyAchievements = require('../models/dailyAchievements');
var Player = require('../models/player');
var Game = require('../controllers/game');
var Promise = require('bluebird');
var Achievements = require('../controllers/achievements');



exports.getDailyAchievements = function (owner_id)
{
    return dailyAchievements.gameObject.get(owner_id)
        .then(function(dailyData) {
            if (dailyData === null)
            {
                dailyData = { ownerId : owner_id, dailies : ["0"], currentDay : 0, createdAt : Date.now() };

                var newDailyData = new dailyAchievements(dailyData);
                dailyAchievements.gameObject.set(newDailyData)
                    .then(function () {
                        return dailyData;
                    });
            }

            return dailyData;
        });
};

exports.getNewRandomDailies = function()
{
    var totalDailies = _.clone(Achievements.ALL_DAILY_IDS);
    var newDailies = [];
    var key = 0;
    var tries = 0;

    while ((newDailies.length < 1) && (tries < 1000))
    {
        key = _.random(0, totalDailies.length - 1);
        newDailies.push(totalDailies[key]);
        totalDailies.splice(key, 1);
    }

    return newDailies;
};

exports.checkResetDailies = function(owner_id)
{
    return exports.getDailyAchievements(owner_id)
        .then(function (dailyData) {

            var now = new Date();
            var today = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate());

            if (today.getUTCSeconds() > dailyData.currentDay )
            {
                return dailyAchievements.gameObject.lock(owner_id)
                    .then( function (lockedDailyData) {
                        now = new Date();
                        today = Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate());
                        if (today.getUTCSeconds() > lockedDailyData.currentDay )
                        {
                            lockedDailyData.dailies = exports.getNewRandomDailies();
                            lockedDailyData.currentDay = now.getUTCSeconds();

                            var dailyPromises = [];

                            _.each(lockedDailyData.dailies, function(dailyId) {
                                dailyPromises.push(Achievements.clearOneAchievement(owner_id, dailyId));
                            })

                            return Promise.all(dailyPromises)
                                .then (function () {
                                    return dailyAchievements.gameObject.unlock(lockedDailyData);
                                })
                                .then ( function () {
                                    return lockedDailyData;
                                });
                        }
                    });

            } else {
                return dailyData;
            }
        }).catch(function(err) {
            //console.log(err)
            throw new Error("Error in Daily Achievement Resetting");
        });
};

