"use strict";

// Load required packages
var Battle = require('../controllers/battle');
var Team = require('../controllers/team');
var Hero = require('../controllers/hero');
var Game = require('../controllers/game');
var Ability = require('../controllers/ability');
var Minion = require('../controllers/minion');
var Promise = require('bluebird');
var _ = require('lodash');

exports.getResults = function(battleParams) {
    // instance is a pointer to the battle snapshot!
    var actor = battleParams.actor;
    var target = battleParams.target;
    var team = battleParams.team;
    var targetTeam = battleParams.targetTeam;
    var battle = battleParams.battle;
    var player = battleParams.player;
    var abilityAction = battleParams.abilityAction;

    var damageData;

    // console.log(battle)
    // console.log(player)

    damageData = getDamageData(actor, target);
    damageData.ability = 0;
    battleParams.damageData = damageData;

    Ability.activateAbilityByType(Ability.TYPE.BUFF, battleParams);
    Ability.activateAbilityByType(Ability.TYPE.DEBUFF, battleParams);
    Ability.activateAbilityByType(Ability.TYPE.STATUS, battleParams);

    if(abilityAction && actor.instance.charge < actor.config.chargeMax) {
        throw new Error('Ability action is not fully charged for this actor!');
    }

    if((abilityAction || battleParams.side === Battle.SIDE.MINIONS)
        && actor.instance.charge >= actor.config.chargeMax) {
        // use ability if fully charged
        Ability.applyAbilityByType(Ability.TYPE.HEAL, battleParams);
        Ability.applyAbilityByType(Ability.TYPE.BUFF, battleParams);
        Ability.applyAbilityByType(Ability.TYPE.DEBUFF, battleParams);
        Ability.applyAbilityByType(Ability.TYPE.STATUS, battleParams);

        actor.instance.charge = 0;
        damageData.ability = 1;
    } else if (actor.instance.charge < actor.config.chargeMax) {
        // otherwise charge hero ability
        actor.instance.charge++;
    }

    // todo: minion/boss abilities
    // Ability.activateDebuffs(minion, battleEnemy, battleHero, damageData);
    // Ability.applyDebuffs(minion, battleEnemy, battleHero, damageData);

    // apply damage
    target.instance.health -= damageData.attackerDamage;
    actor.instance.health -= damageData.targetDamage;

    //console.log('****')
    //console.log(battleParams.status)
    return damageData;
};

function getDamageData(attacker, target) {
    // damage
    var baseAttackerDamage = getDamage(attacker);
    var attackerCritMultiplier = getCritMultiplier(attacker);
    var attackerDamage = Math.ceil(baseAttackerDamage * attackerCritMultiplier);
    var attackerCrit = false;
    var targetCrit = false;
    // no counterattack damage for now
    var targetDamage = 0; // getDamage(target);

    // defense damage reduction
    if(targetDamage > 0) {
        var attackerDefense = Math.floor(targetDamage * getDefenseMultiplier(attacker));
        targetDamage -= attackerDefense;
    }

    if(attackerDamage > 0) {
        var targetDefense = Math.floor(attackerDamage * getDefenseMultiplier(target));
        attackerDamage -= targetDefense;
    }

    var logString = attacker.config.name + ' dealt '+attackerDamage;

    // crit flag
    if(attackerCritMultiplier > 1) {
        attackerCrit = true;
        logString += ' critical damage to '+target.config.name+'!';
    } else {
        logString += ' damage to '+target.config.name+'!';
    }

    return {
        attackerDamage:     attackerDamage,
        attackerDefense:    attackerDefense,
        targetDefense:      targetDefense,
        attackerCrit:       attackerCrit,
        targetDamage:       targetDamage,
        targetCrit:         targetCrit,
        logs:               [logString]
    }
}

// all calculations derived from level
function getLevel (actor) {
    var level = 1;

    if(actor.hasOwnProperty('instance')
        && actor.instance.hasOwnProperty('level')) {
        level = actor.instance.level;
    } else {
        level = Hero.getLevel(actor);
    }

    return level;
}

function getAttack (actor) {
    var level = getLevel(actor);
    var bonusAttack = 0;
    if (actor.bonus.hasOwnProperty('attack'))
    {
        bonusAttack = actor.bonus.attack;
    }
    return Math.ceil(actor.config.attackScaling * level) + (actor.config.attack + bonusAttack);
}

function getDamage (actor) {
    var randomModifier = _.random(1.01, 1.05);
    var bonusDmgPercent = 0;
    if (actor.bonus.hasOwnProperty('dmg_percent'))
    {
        bonusDmgPercent = actor.bonus.dmg_percent;
    }
    var baseDamage = (actor.config.damageMultiplier + (bonusDmgPercent / 100)) * getAttack(actor);
    return Math.ceil(baseDamage * randomModifier);
}

function getDefenseMultiplier (actor) {
    var level = getLevel(actor);
    var defense = actor.config.defenseScaling * level;
    if (actor.bonus.hasOwnProperty('defense'))
    {
        defense += actor.bonus.defense;
    }
    return (defense / (defense + (level * 10)));
}

function getCritMultiplier (actor) {
    var critRoll = _.random(1.01, 100.01);
    var critMultiplier = 1;
    var critBonus = 0;
    if (actor.bonus.hasOwnProperty('crit'))
    {
        critBonus += actor.bonus.crit;
    }

    if(critRoll <= (actor.config.criticalChance + critBonus)) {
        critMultiplier = actor.config.criticalMultiplier;
        if (actor.bonus.hasOwnProperty('crit_dmg'))
        {
            critMultiplier += actor.bonus.crit_dmg;
        }
    }

    return critMultiplier;
}
