// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var QuestSchema = new mongoose.Schema({
    questId:    { type: Number, required: true }, // Quest id
    ownerId:    { type: String, required: true },
    energy:     { type: Number, required: true, default: 0},
    createdAt:  { type: Number, required: true }
});

QuestSchema.pre('findOne', function(callback){
    var query = this;

    if(mongoose.Types.ObjectId.isValid(query._conditions._id)) {
        return callback();
    }

    throw new Error('_id query is not an object id!');
});

QuestSchema.statics.gameObject = null;

QuestSchema.statics.resourceKey = function(objectId) {
    return 'quests:'+objectId.toString();
};

// Export the Mongoose model
var Quest = mongoose.model('Quest', QuestSchema);
Quest.gameObject = new GameObject({model: Quest});
module.exports = Quest;
