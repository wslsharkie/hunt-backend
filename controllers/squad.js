"use strict";

// Load required packages
var Squad = require('../models/squad');
var SquadFeed = require('../controllers/squadFeed');
var SquadDonationController = require('../controllers/squadDonation');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var Team = require('../controllers/team');
var Game = require('../controllers/game');
var Promise = require('bluebird');
var _ = require('lodash');

exports.RANK = {
    LEADER: 0,
    OFFICER: 1,
    MEMBER: 2
};

exports.MAXMEMBERS = 20;

exports.get = function(squadId) {
    if(squadId.length === 0) {
        return Promise.resolve(null);
    }

    return Squad.gameObject.get(squadId)
        .then(function(squad) {
            return squad;
        });
};

exports.create = function(player, squadName) {
    if(player.coin < costToCreate(player.level)) {
        return res.send({error:"Not enough money!"});
    }

    var squad;

    return Player.gameObject.lock(player._id)
        .then(function(lockedPlayer) {
            player = lockedPlayer;

            if(player.coin < costToCreate(player.level)) {
                return res.send({error:"Not enough money!"});
            }

            player.subProp('coin', costToCreate(player.level));

            return createSquadMember(player, exports.RANK.LEADER);
        })
        .then(function(member) {
            var members = {};
            members[player._id] = member;

            var instance = {
                leaderId: player._id,
                name: squadName.toString().substr(0,14),
                members: members,
                blurb : "Enter guild notices here",
            };

            squad = new Squad(instance);
            return Squad.gameObject.set(squad);
        })
        .then(function (newSquad) {
            squad = newSquad;
            player.squadId = squad._id.toString();
            return Player.gameObject.unlock(player);
        })
        .then(function() {
            return squad;
        })
        .catch(function(err) {
            //console.log(err)
            Player.gameObject.revert(player);
            throw new Error(err.message);
        });
};

exports.isMember = function(squad, playerId) {
    return squad.members.hasOwnProperty(playerId);
};

// leader sets the boss/battle to be hunted
//exports.setBossToHunt

// all members contribute energy for boss to be created
//exports.addEnergyToHunt

function costToCreate(level) {
    return Math.ceil(level*Math.pow(1.2,level));
}

exports.createSquadMember = function(player, rank) {
    return Team.getFullActiveTeam(player)
        .then(function (team) {
            var member = {
                id: player._id,
                name: player.name,
                level: PlayerController.getLevel(player),
                rank: rank
            };

            if(team.hasOwnProperty(0)) {
                member.hero = team[0].instance;
            }

            return member;
        });
}

exports.isOfficer = function(squad, playerId) {
    if(squad.members[playerId].rank === exports.RANK.OFFICER) {
        return true;
    }

    return false;
};

exports.isLeader = function(squad, playerId) {
    if(squad.members[playerId].rank === exports.RANK.LEADER) {
        return true;
    }

    return false;
};

/*** ENDPOINTS ***/
exports.createSquad = function(req, res) {
    if(req.body.name === undefined) {
        return res.send({error:"Needs a name!"});
    }

    var player = req.player;

    exports.create(player, req.body.name)
        .then(function(squad) {
            player.squadId = squad._id;
            return res.send({player: player, squad: squad});
        })
        .catch(function(err) {
            //console.log(err)
            return res.send({ error: 'Failed to create squad!' });
        });
};

exports.getSquadInfo = function(req, res) {
    var player = req.player;

    var squadId;

    if(typeof req.body.squad_id === 'string') {
        squadId = req.body.squad_id;
    }

    if(squadId === undefined && player.squadId.length > 0) {
        squadId = player.squadId;
    }

    if(squadId === undefined) {
        return res.send({error: "No squad id!"});
    }

    var data = {};

    return exports.get(squadId)
        .then( function (squad) {
            data['squad'] = squad;
            data['isMember'] = exports.isMember(squad, player._id);

            if(data['isMember']) {
                return Promise.all([SquadFeed.get(squadId), SquadDonationController.getDonationOptions(squadId, player._id)])
            } else {
                return [false, false];
            }
        })
        .spread(function (squadFeed, donationOptions) {
            if(squadFeed) {
                data['feed'] = squadFeed;
            }
            if (donationOptions)
            {
                data['options'] = donationOptions;
            }
            return res.send(data);
        })
        .catch(function(err) {
            return res.send({ error: 'Failed to find squad!' });
        });
};

exports.join = function(req, res) {
    var player = req.player;

    if(req.params.squad_id === undefined) {
        return res.send({error: "No id!"});
    }

    if(player.squadId.length > 0) {
        return res.send({error: "Player is already in a squad!"});
    }

    var squadId = req.params.squad_id;
    var squad;

    Squad.gameObject.lock(squadId)
        .then(function (lockedSquad) {
            squad = lockedSquad;
            return Player.gameObject.lock(player._id);
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return exports.createSquadMember(player, exports.RANK.MEMBER);
        })
        .then(function (member) {
            player.squadId = squad._id.toString();
            squad.members[player._id] = member;
            squad.size = Object.keys(squad.members).length;

            if(squad.size === exports.MAXMEMBERS) {
                squad.public = false;
            }

            return Promise.all([Player.gameObject.unlock(player), Squad.gameObject.unlock(squad), SquadFeed.get(squadId)]);
        })
        .spread(function(plockResults, slockResults, squadFeed) {
            return res.send({squad: squad, feed: squadFeed, isMember: true});
        })
        .catch(function(err) {
            //console.log(err)
            Squad.gameObject.revert(squad);
            Player.gameObject.revert(player);
            return res.send({ error: 'Failed to join!' });
        });
};

exports.leave = function (req, res) {
    var player = req.player;

    if(player.squadId.length === 0) {
        return res.send({error: "Invalid squad id!"});
    }

    var squad;

    Squad.gameObject.lock(player.squadId)
        .then(function (lockedSquad) {
            squad = lockedSquad;

            if(squad.members.hasOwnProperty(player._id) === false) {
                throw new Error("Player is not in this squad!");
            }

            return Player.gameObject.lock(player._id);
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;

            player.squadId = "";
            delete squad.members[player._id];
            squad.size--;
            squad.public = true;

            return Promise.all([Player.gameObject.unlock(player), Squad.gameObject.unlock(squad)]);
        })
        .then(function() {
            return res.send({squad: squad, isMember: false});
        })
        .then(function () {
            // remove squad with no members!
            if(Object.keys(squad.members).length === 0) {
                Squad.gameObject.delete(squad._id);
            }
        })
        .catch(function(err) {
            //console.log(err)
            Squad.gameObject.revert(squad);
            Player.gameObject.revert(player);
            return res.send({ error: 'Failed to leave squad!' });
        });
};

exports.kick = function (req, res) {
    var player = req.player;

    if(req.body.target_id === undefined) {
        return res.send({error: "No id!"});
    }

    var targetId = req.body.target_id;

    if(player.squadId.length === 0) {
        return res.send({error: "This player is not in a squad!"});
    }

    if(player._id === targetId) {
        return res.send({error: "Can not kick yourself from squad!"});
    }

    var squad;
    var target;

    Squad.gameObject.lock(player.squadId)
        .then(function (lockedSquad) {
            squad = lockedSquad;

            if(squad.members.hasOwnProperty(player._id) === false) {
                throw new Error("Player is not in this squad!");
            }

            if(squad.members.hasOwnProperty(targetId) === false) {
                throw new Error("Target is not in this squad!");
            }

            if(exports.isOfficer(squad, player._id) === false
                && exports.isLeader(squad, player._id) === false) {
                throw new Error("Player is not allowed to kick another member!");
            }

            if(exports.isLeader(squad, targetId)) {
                throw new Error("Player is not allowed to kick the leader!");
            }

            return Player.gameObject.lock(targetId);
        })
        .then(function (lockedPlayer) {
            target = lockedPlayer;

            target.squadId = "";
            delete squad.members[target._id];
            squad.size--;
            squad.public = true;

            return Promise.all([Player.gameObject.unlock(target), Squad.gameObject.unlock(squad)]);
        })
        .then(function() {
            return res.send({squad: squad, isMember: true});
        })
        .catch(function(err) {
            //console.log(err)
            Squad.gameObject.revert(squad);
            Player.gameObject.revert(target);
            return res.send({ error: 'Failed to kick player from squad!' });
        });
};

exports.promote = function (req, res) {
    var player = req.player;

    if(req.body.target_id === undefined) {
        return res.send({error: "No id!"});
    }

    var targetId = req.body.target_id;

    if(player.squadId.length === 0) {
        return res.send({error: "This player is not in a squad!"});
    }

    if(player._id.toString() === targetId) {
        return res.send({error: "Can not promote yourself in squad!"});
    }

    var squad;

    Squad.gameObject.lock(player.squadId)
        .then(function (lockedSquad) {
            squad = lockedSquad;

            if(squad.members.hasOwnProperty(player._id) === false) {
                throw new Error("Player is not in this squad!");
            }

            if(squad.members.hasOwnProperty(targetId) === false) {
                throw new Error("Target is not in this squad!");
            }

            if(exports.isOfficer(squad, player._id) === false
                || exports.isLeader(squad, player._id) === false) {
                throw new Error("Player is not allowed to promote another member!");
            }

            if(exports.isLeader(squad, targetId)) {
                throw new Error("Player is not allowed to promote the leader!");
            }

            squad.members[targetId].rank = exports.RANK.OFFICER;

            return Squad.gameObject.unlock(squad);
        })
        .then(function() {
            return res.send({squad: squad, isMember: true});
        })
        .catch(function(err) {
            //console.log(err)
            Squad.gameObject.revert(squad);
            return res.send({ error: 'Failed to promote player in squad!' });
        });
};

exports.demote = function (req, res) {
    var player = req.player;

    if(req.body.target_id === undefined) {
        return res.send({error: "No id!"});
    }

    var targetId = req.body.target_id;

    if(player.squadId.length === 0) {
        return res.send({error: "This player is not in a squad!"});
    }

    if(player._id === targetId) {
        return res.send({error: "Can not demote yourself in squad!"});
    }

    var squad;

    Squad.gameObject.lock(player.squadId)
        .then(function (lockedSquad) {
            squad = lockedSquad;

            if(squad.members.hasOwnProperty(player._id) === false) {
                throw new Error("Player is not in this squad!");
            }

            if(squad.members.hasOwnProperty(targetId) === false) {
                throw new Error("Target is not in this squad!");
            }

            if(exports.isLeader(squad, targetId)) {
                throw new Error("Player is not allowed to demote the leader!");
            }

            if(exports.isOfficer(squad, targetId) === true
                && exports.isLeader(squad, player._id) === false) {
                throw new Error("Only leader is allowed to demote officer!");
            }

            squad.members[targetId].rank = exports.RANK.MEMBER;

            return Squad.gameObject.unlock(squad);
        })
        .then(function() {
            return res.send({squad: squad, isMember: true});
        })
        .catch(function(err) {
            //console.log(err)
            Squad.gameObject.revert(squad);
            return res.send({ error: 'Failed to demote player in squad!' });
        });
};

var Client = require('../controllers/client');
exports.createDummyMembers = function(req, res) {
    var player = req.player;
    if(Game.DEBUG === false) {
        return res.send({error: "unauthorized"});
    }

    var playerPromises = [];
    var players = [];
    var lockedPlayers = [];
    var squadId = player.squadId;
    var squad;

    for(var i=0; i<10; i++) {
        var p = Client.create()
            .then(function (client) {
                return PlayerController.newPlayer(client, "Battle Dummy");
            });

        playerPromises.push(p);
    }
    return Promise.all(playerPromises)
        .then(function (returnedPlayers) {
            players = returnedPlayers;
            var heroPromises = [];

            console.log(players);

            _.each(players, function (p) {
                heroPromises.push(Hero.addHeroes(p, Hero.starterHeroes));
            });

            return Promise.all(heroPromises);
        })
        .then(function () {
            return Promise.each(players, function (p) {
                var lp;
                var squad;
                Squad.gameObject.lock(squadId)
                    .then(function (lockedSquad) {
                        squad = lockedSquad;
                        return Player.gameObject.lock(p._id);
                    })
                    .then(function (lockedPlayer) {
                        lp = lockedPlayer;
                        return createSquadMember(lp, exports.RANK.MEMBER);
                    })
                    .then(function (member) {
                        lp.squadId = squad._id;
                        squad.members[lp._id] = member;
                        squad.size = Object.keys(squad.members).length;

                        if(squad.size === exports.MAXMEMBERS) {
                            squad.public = false;
                        }

                        return Promise.all([Player.gameObject.unlock(lp), Squad.gameObject.unlock(squad)]);
                    })
                    .then(function() {
                        lockedPlayers.push(lp);
                        return lp.squadId;
                    })
                    .catch(function(err) {
                        console.log(err);
                        Squad.gameObject.revert(squad);
                        Player.gameObject.revert(player);
                        return res.send({ error: 'Failed to join!' });
                    });
            })
            return res.send(i + " dummy players created!");
        })
        .then( function (dummyResults) {
            console.log(dummyResults);
            // _.each(testerResults, function(results) {
            //     resultsTracker[results.hero_id] += 1;
            // })
            console.log("resultsTracker: ");
            return res.send({results : dummyResults, squad: squad, players: players});
        })

}
