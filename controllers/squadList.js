"use strict";

var Squad = require('../models/squad');
var Hero = require('../controllers/hero');
//var PlayerController = require('../controllers/player');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var _ = require('lodash');

// generate a list of squads
// get random squads of the same player level range
// client will store this list in cache to randomize

exports.RANGE_BASE = 1.25;
exports.LIST_KEY = 'squad:list:';
exports.LIST_MAX = 20;
exports.EXPIRE_TIME = 600; // seconds

function getLevelRange(player) {
    if(player.experience === 0) {
        return {min: 0, max: 1};
    }

    var value = Math.ceil(player.experience * Math.pow(exports.RANGE_BASE, 2) - player.experience);
    var rangeMin = Math.max(0, player.experience-value);
    var rangeMax = player.experience+value;
    return {min: rangeMin, max: rangeMax};
}

exports.resourceKey = function(player) {
    return exports.LIST_KEY+player._id;
};

exports.get = function (player) {
    return redis.get(exports.resourceKey(player))
        .then(function (list) {
            if(Game.DEBUG === false && list) {
                return list;
            }

            //var range = getLevelRange(player);

            var query = [
                // Grouping pipeline
                { "$match": {
                    "size": {"$gte":1, "$lte": SquadController.maxMembers-1},
                    "public": {"$eq": true}
                } },
                // specify fields
                { "$project" : {
                    "name" : 1 ,
                    "members" : 1,
                    "size" : 1
                } },
                // random sample pipeline
                { "$sample": { "size" : exports.LIST_MAX } }
            ];

            return Squad.aggregate(query).exec()
                .then(function (squads) {
                    if(squads.length > 0) {
                        return redis.set(exports.resourceKey(player), squads, exports.EXPIRE_TIME)
                            .then(function () {
                                // member average level
                                _.each(squads, function (squad) {
                                    var totalLevel = 0;
                                    var squadSize = 0;

                                    for(var memberId in squad.members) {
                                        var member = squad.members[memberId];
                                        totalLevel += member.level;
                                        squadSize++;
                                    }

                                    squad.avgLevel = Math.max(Math.floor(totalLevel / squadSize), 1);
                                    delete squad.members;
                                });

                                return squads;
                            });
                    }

                    return squads;
                });
        })
        .catch(function (err) {
            console.log(err)
            throw new Error("Failed to get squads list!");
        });
};

/*** ENDPOINTS ***/
exports.showList = function(req, res) {
    var player = req.player;

    exports.get(player)
        .then(function (list) {
            return res.send({list: list});
        })
};

// testing
var PlayerList = require('../controllers/playerList');
var SquadController = require('../controllers/squad');
exports.createDummySquads = function (req, res) {
    if(Game.DEBUG === false) {
        return res.send({error: "unauthorized"});
    }

    var player = req.player;

    // get list of players
    PlayerList.get(player)
        .then(function (playerList) {
            //console.log(playerList);
            var nameCount = 0;

            var generatePromises = _.map(playerList, function (p) {
                return SquadController.create(p, "Dummy "+nameCount++);
            });

            return Promise.all(generatePromises);
        })
        .then(function () {
            return res.send("Squads generated!");
        });
};
