"use strict";

// Load required packages
var Achievements = require('../models/achievements');
var DailyAchievements = require('../controllers/dailyAchievements');
var Game = require('../controllers/game');
var Player = require('../models/player');
var Item = require('../controllers/item');
var Promise = require('bluebird');
var jsonfile = require('jsonfile');
var _ = require('lodash');

/*
Example emit to this class:
 Game.EventEmitter.emit(Game.EVENTS.HOME, player._id);
 */

exports.achievements = {};

exports.ALL_DAILY_IDS = ["0", "1"];

exports.TYPE = { ADD : 0, MAX: 1};

exports.DAILIES = { dHOME : 0, dSTAGE : 1 };

exports.TUTORIALS = { iQUEST1: 2001, iQUEST2: 2002};

exports.TROPHIES = { tLEVEL_UP : 10001, tQUEST : 10002};

exports.init = function() {
    var path = Game.paths.configPath+'/achievements/achievements.json';
    exports.achievements = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.achievements);
    Game.deepFreeze(exports.ALL_DAILY_IDS);
    Game.deepFreeze(exports.TYPE);
    Game.deepFreeze(exports.DAILIES);
    Game.deepFreeze(exports.TUTORIALS);
    Game.deepFreeze(exports.TROPHIES);

    initEventListeners();
};

exports.getAllAchievements = function(ownerId) {
    return Achievements.gameObject.hGetAll(ownerId)
        .then(function (achieveData) {
            return achieveData;
        });
};

exports.getOneAchievement = function(ownerId, achId) {
    return Achievements.gameObject.hmGet(ownerId, achId)
        .then(function (achieveData) {
            if (achieveData === null) {
                achieveData = {
                    ownerId : ownerId ,
                    achId : achId ,
                    achVal: 0,
                    collected : 0,
                    last_updated: Date.now(),
                    date: Date.now(),
                };

                var newAchDasta = new Achievements(achieveData);

                return Achievements.gameObject.hSet(ownerId, achId, newAchDasta)
                    .then(function() {
                        return achieveData;
                    });
            }
            return achieveData;
        });
};

exports.clearOneAchievement = function (owner_id, achId)
{
    var achData;

    return Achievements.gameObject.getOneAchievement(owner_id, achId)
        .then( function (data) {
            achData = data;

            if(achData.achVal === 0 && achData.collected === 0) {
                return false;
            }

            return Achievements.gameObject.hmLock(owner_id, achId)
                .then( function (lockedAchData) {
                    achData = lockedAchData;

                    if(achData.achVal === 0 && achData.collected === 0) {
                        return Achievements.gameObject.revert(achData);
                    }

                    achData.achVal = 0;
                    achData.collected = 0;
                    return Achievements.gameObject.hmUnlock(achData);
                });
        }).catch(function(err) {
            Achievements.gameObject.revert(achData);
            throw new Error("Can't clear achievement data for player for achId"+achId);
        });
};

exports.getOneAchievementDetails = function(achId, achievementProgress, collected, canCollect)
{
    var achInfo = {};
    achInfo.id = achId;
    //achInfo.title = exports.achievements[achId].name;
    //achInfo.desc = exports.achievements[achId].desc;
    achInfo.canCollect = canCollect;

    achInfo.val = 0;
    achInfo.val = achievementProgress;

    var currentGoal = {};
    var currentGoalLevel = 0;
    _.each (exports.achievements[achId].goals, function (goal) {
        if (goal.id >= currentGoalLevel
            && goal.id <= collected)
        {
            currentGoal = goal;
        }
    });
    //achInfo.text = currentGoal.flavor;
    //achInfo.target = currentGoal.value;
    if(exports.ALL_DAILY_IDS.includes(achId)) {
        achInfo.is_daily = true;
    }

    var rewardList = exports.getRewardListText(currentGoal.reward);

    achInfo.rewards = rewardList;

    // if (currentGoal.hasOwnProperty("item_reward"))
    // {
    //     achInfo.reward = "reward = "+ currentGoal.item_amt.toString() + "x item" + currentGoal.item_reward.toString();
    // } else {
    //     achInfo.reward = currentGoal.reward.toString() + "gold!";
    // }

    return achInfo;
}

exports.checkToCollect = function (playerID)
{

}

exports.canCollectAchievement = function (achData, achId)
{
    if (exports.achievements.hasOwnProperty(achId) === false)
    {
        throw new Error("Achievement id" + achId +" cannot collect");
    }

    //if highest progress so far is higher than what you've collected, you can collect your achievement
    var highestGoalAchieved = exports.findHighestProgressLevel(achId, achData.achVal);
    if (highestGoalAchieved >= achData.collected && exports.achievements[achId].goals.hasOwnProperty(achData.collected) === true)
    {
        return true;
    }
    return false;

}


function canProcess(achievement, achData, value) {
    switch (achievement.type) {
        case "add" :
            if ((value + achData.achVal) > achData.achVal) {
                return true;
            }
            break;
        case "max" :
            if (value > achData.achVal) {
                return true;
            }
            break;
        default:
            return false;
            break;
    }

    return false;
}

exports.awardReward = function (player_id, rewardList)
{
    var rewarded = [];
    var player = null;

    return Player.gameObject.lock(player_id)
        .then( function (lockedPlayer) {
            player = lockedPlayer;

            _.each(rewardList, function (reward) {
                switch (reward.type) {
                    case "coins" :

                        player.coins += reward.amount;
                        rewarded.push(reward);
                        break;

                    case "items" :

                        Item.addItem(player, reward.id, reward.amount);
                        rewarded.push(reward);
                        break;
                }
            });

            return Player.gameObject.unlock(player)
                .then( function () {
                    return rewarded;
                })
        })
        .catch(function (err) {
            //console.log(err)
            Player.gameObject.revert(player);
            throw new Error(err.message);
        });
};

//process
    //increment( value, a_ID)
    //updateMax( value, a_ID)
exports.processAchievement = function (playerId, achId, value)
{
    if (parseInt(value) <= 0)
    {
        throw new Error("Invalid value"+value+" for processAchievement");
    }

    if (exports.achievements.hasOwnProperty(achId) === false) {
        //console.log(exports.achievements);
        throw new Error("Achievement"+achId+" not found");
    }

    var achConfig = exports.achievements[achId];
    var achData;

    return exports.getOneAchievement(playerId, achId)
        .then( function (achDataResult) {
            achData = achDataResult;
            if(canProcess(achConfig, achData, value) === false) {
                return false;
            }

            return Achievements.gameObject.hmLock(playerId, achId)
                .then(function (lockedAchData) {
                    achData = lockedAchData;

                    if(canProcess(achConfig, achData, value) === false) {
                        return Achievements.gameObject.revert(achData);
                    }

                    switch (achConfig.type)
                    {
                        case "add" :
                            achData.achVal += value;
                            break;
                        case "max" :
                            achData.achVal = value;
                            break;
                    }

                    return Achievements.gameObject.hmUnlock(achData);
                });
        })
        .catch(function (err) {
            //console.log(err)
            Achievements.gameObject.revert(achData);
            throw new Error(err.message);
        });
};

exports.findHighestProgressLevel = function (achId, achValue) {

    if (exports.achievements.hasOwnProperty(achId) === false)
    {
        throw new Error("No achievement id" +achId+" exists for progress check");
    }

    var achGoals = exports.achievements[achId].goals;
    var highestProgress = 0;

    _.each(achGoals, function (threshold) {
        if (achValue >= threshold.value && threshold.id >= highestProgress)
        {
            highestProgress = threshold.id;
        }
    });

    return highestProgress;
};

exports.getRewardListText = function (list)
{
    var rewardList = "";
    _.each(list, function (reward, key) {
        var amount = reward.amount.toString();

        if (rewardList.length > 1)
        {
            rewardList += "; ";
        }
        if (reward.type == "coins")
        {
            rewardList += "" + amount + "x Gold";
        } else if (reward.type == "items")
        {
            var item_name = Item.getItemConfig(reward.id).name;
            rewardList += "" + amount + "x " + item_name;
        }
    })

    return rewardList;
}



//processAndCheck => sync  /* SHELVED FOR NOW */
    //process
    //checkToCollect

//grantRewards(rewards)

//resetDailies( achievements)


/*** ENDPOINTS ***/

exports.showAllAchievementInfo = function(req, res)
{
    var player = req.player;
    var achList = {};

    return DailyAchievements.checkResetDailies(player._id)
        .then(function () {
            return exports.getAllAchievements(player._id)
        })
        .then( function (allAchData) {
            _.each(allAchData, function (achData, achId) {
                achList[achId] = exports.getOneAchievementDetails(achId, achData.achVal, achData.collected, exports.canCollectAchievement(achData, achId));
            });

            _.each (exports.achievements, function (achConfigData, achId) {
                if (achList.hasOwnProperty(achId) === false)
                {
                    achList[achId] = exports.getOneAchievementDetails(achId, 0, 0, false);
                }
            });
        })
        .then( function () {
            return res.send(achList);
        })
        .catch( function (err) {
            //console.log(err);
            return res.send({error : "Achievement Lookup Failed"});
        })
};

exports.collectAchievement = function (req, res)
{
    var player = req.player;
    var achId = req.body.ach_id;
    var achData = null;
    var rewardsToAdd

    if(req.body.ach_id === undefined) {
        return res.send({error: "Missing achievement ID paramater"});
    }

    if (exports.achievements.hasOwnProperty(achId) === false)
    {
        return res.send({error: "No achievement id" +achId+" exists for player"+player._id+"! Hack?"});
    }

    return Achievements.gameObject.hmLock(player._id, achId)
        .then( function (lockedAchData) {
            achData = lockedAchData;

            if (exports.canCollectAchievement(lockedAchData, achId) === false) {
                throw new Error("1 - Achievement id" +achId+" not complete for player"+player._id+"! Hack?");
            }

            //correct b/c goal we want to award is zero based and the #of times collected is 1 base
            var goalToCollect = lockedAchData.collected;
            rewardsToAdd = exports.achievements[achId].goals[goalToCollect].reward;

            return exports.awardReward(player._id, rewardsToAdd);
        })
        .then( function (awardedItems) {
            achData.collected += 1;
            return Promise.all([awardedItems, Achievements.gameObject.hmUnlock(achData)]);
        })
        .spread( function (awardedItems, saved) {
            if (saved)
            {
                awardedItems = exports.getRewardListText(rewardsToAdd);

            }
            return res.send({rewards: "Rewarded:" + awardedItems});
        })
        .catch(function (err) {
            //console.log(err)
            Achievements.gameObject.revert(achData);
            return res.send({error: "Failed to collect!"});
        });
};


function initEventListeners() {

    /*** ACHIEVEMENT EMITTER LISTENERS ***/

    Game.EventEmitter.on(Game.EVENTS.HOME, function (player_id) {
        console.log("WSL ** home page event registered, processing achievemenet"+exports.DAILIES.dHOME);
        exports.processAchievement(player_id, exports.DAILIES.dHOME, 1);
    });


    Game.EventEmitter.on(Game.EVENTS.QUEST_DONE, function (player_id, stage_id) {
        console.log("WSL ** level up page event registered, processing achievemenet"+exports.TROPHIES.tQUEST);
        exports.processAchievement(player_id, exports.TROPHIES.tQUEST, stage_id);
    });

}
