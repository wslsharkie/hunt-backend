"use strict";

var express = require('express');
var bossRouter = express.Router();
var authController = require('../controllers/auth');
var bossController = require('../controllers/boss');

bossRouter.route('/create/:boss_id/?')
    .post(authController.isBearerAuthenticated, bossController.createBoss);

bossRouter.route('/:object_id/?')
    .get(authController.isBearerAuthenticated, bossController.getBoss);

bossRouter.route('/:object_id/attack/?')
    .post(authController.isBearerAuthenticated, bossController.attackBoss);

module.exports = bossRouter;
