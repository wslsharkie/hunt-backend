var express = require('express');
var oauthRouter = express.Router();
var oauthController = require('../controllers/oauth');
var authController = require('../controllers/auth');
var playerController = require('../controllers/player');

oauthRouter.route('/token/?')
    .post(authController.isClientAuthenticated, oauthController.token);

module.exports = oauthRouter;
