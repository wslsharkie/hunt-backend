var express = require('express');
var gatewayRouter = express.Router();
var authController = require('../controllers/auth');
var gatewayController = require('../controllers/gateway');

gatewayRouter.route('/summon/:buy_id')
    .post(authController.isBearerAuthenticated, gatewayController.summon);

module.exports = gatewayRouter;
