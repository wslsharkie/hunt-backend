"use strict";

var Equipment = require('../models/equipment');
var Item = require('../controllers/item');
var Hero = require('../controllers/hero');
var Player = require('../models/player');
var jsonfile = require('jsonfile');
var Game = require('../controllers/game');
var _ = require('lodash');

exports.TYPE = {};
exports.TYPE.SWORD = 0;

exports.equipment = {};
exports.heroSets = {}; // heroId : [equipmentId, ...]

exports.createEquipment = function (playerId, heroId) {
    var hero = Hero.getHeroConfig(heroId);

    if(hero === null) {
        throw new Error(heroId+' does not exist!');
    }

    var heroSet = exports.getDefaultHeroSet(heroId);

    if(heroSet === null) {
        throw new Error(heroId+' hero set does not exist!');
    }

    var equipment = {};

    _.each(heroSet, function (setId) {
        equipment[setId] = {id: setId, level: 0};
    });

    var instance = {
        ownerId:    playerId,
        heroId:     heroId,
        equipment:  equipment,
        createdAt:  Date.now()
    };

    return new Equipment(instance);
};

exports.init = function() {
    var eqPath = Game.paths.configPath+'/equipment/equipment.json';
    exports.equipment = jsonfile.readFileSync(eqPath);

    var setPath = Game.paths.configPath+'/equipment/heroSets.json';
    exports.heroSets = jsonfile.readFileSync(setPath);

    Game.deepFreeze(exports.equipment);
    Game.deepFreeze(exports.heroSets);
};

exports.getEquipmentConfig = function(id) {
    if(exports.equipment.hasOwnProperty(id)) {
        return exports.equipment[id];
    }

    return null;
};

exports.getDefaultHeroSet = function (heroId) {
    if(exports.heroSets.hasOwnProperty(heroId)) {
        return exports.heroSets[heroId];
    }

    return null;
};

exports.get = function (player, heroId) {
    return Equipment.gameObject.hmGet(player._id, heroId)
        .then(function (heroEquipment) {
            if(heroEquipment === null) {
                // make sure player owns this hero!
                return Hero.getHero(player, heroId)
                    .then(function (hero) {
                        if(hero === null) {
                            throw new Error('Can not get equipment for hero player does now own!');
                        }

                        heroEquipment = exports.createEquipment(player._id, heroId);

                        return Equipment.gameObject.hSet(player._id, heroId, heroEquipment)
                            .then(function () {
                                return heroEquipment;
                            })
                    });
            }

            return heroEquipment;
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err.message);
        });
};

exports.incrementLevel = function (player, heroId, equipmentId) {
    return Equipment.gameObject.hmGet(player._id, heroId)
        .then(function (heroEquipment) {
            if(heroEquipment === null) {
                throw new Error('Failed to get hero '+heroId+' equipment!');
            }

            if(heroEquipment.equipment.hasOwnProperty(equipmentId) === false) {
                throw new Error('Hero '+heroId+' does not have equipment '+equipmentId+'!');
            }

            var config = exports.getEquipmentConfig(equipmentId);

            if(config === null) {
                throw new Error(equipmentId+' equipment does not exist!');
            }

            var nextLevel = heroEquipment.equipment[equipmentId].level + 1;
            var maxLevel = config.levels.length + 1;

            if(maxLevel < nextLevel) {
                throw new Error('Equipment is at max level?');
            }

            heroEquipment.equipment[equipmentId].level = nextLevel;
            return Equipment.gameObject.hSet(player._id, heroId, heroEquipment);
        });
};

exports.craft = function (player, heroId, equipmentId) {
    return validateCrafting(player, heroId, equipmentId)
        .then(function () {
            return Player.gameObject.lock(player._id);
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return validateCrafting(player, heroId, equipmentId);
        })
        .then(function () {
            return exports.get(player, heroId);
        })
        .then(function (heroEquipment) {
            var instance = heroEquipment.equipment[equipmentId];
            var config = exports.getEquipmentConfig(instance.id);
            var requirements = config.levels[instance.level];
            return Item.subtractItems(player, requirements);
        })
        .then(function () {
            return exports.incrementLevel(player, heroId, equipmentId);
        })
        .then(function () {
            return Player.gameObject.unlock(player);
        })
        .catch(function (err) {
            Player.gameObject.revert(player);
            throw new Error(err.message);
        });
};

function getItemsNeeded(equipmentId) {
    var config = exports.getEquipmentConfig(equipmentId);

    if(config === null) {
        throw new Error(equipmentId+' equipment does not exist!');
    }

    return _.reduce(config.levels, function (result, level) {
        _.each(level, function (requirement) {
            if(result.includes(requirement.id) === false) {
                result.push(requirement.id);
            }
        });

        return result;
    }, []);
}

function getItemsRequired(equipment) {
    var itemIds = [];

    _.each(equipment.equipment, function (equipment, equipmentId) {
        itemIds = _.concat(getItemsNeeded(equipmentId));
    });

    return _.uniq(itemIds);
}

function validateCrafting(player, heroId, equipmentId) {
    return exports.get(player, heroId)
        .then(function (equipment) {
            if(equipment.equipment.hasOwnProperty(equipmentId) === false) {
                throw new Error(equipmentId+' equipment does not exist for hero!');
            }

            var instance = equipment.equipment[equipmentId];
            var config = exports.getEquipmentConfig(instance.id);

            if(config === null) {
                throw new Error(instance.id+' equipment set does not exist!');
            }

            if(config.levels[instance.level] === undefined) {
                throw new Error('Can not craft equipment '+equipmentId+' at level '+instance.level+'!');
            }

            var requirements = config.levels[instance.level];

            return Promise.all([requirements, Item.getAll(player)]);
        })
        .spread(function (requirements, items) {
            _.each(requirements, function (requirement) {
                if(items.hasOwnProperty(requirement.id) === false) {
                    throw new Error('Missing items to craft!');
                }

                if(items[requirement.id].amount < requirement.amount) {
                    throw new Error('Missing items to craft!');
                }
            });

             return true;
        });
}

exports.getBonusesFromData = function(heroEQ)
{
    var allEQ = heroEQ.equipment;
    var finalBonuses = {};
    var level;
    var eqID;
    var bonus;

    _.each(allEQ, function(eqData, key) {
        eqID = eqData.id;
        level = eqData.level;
        if (level > 0)
        {
            level--;
            bonus = _.nth(exports.getEquipmentConfig(eqID).bonus, level);
            _.each(bonus, function(value, stat) {
                if (finalBonuses.hasOwnProperty(stat) === false)
                {
                    finalBonuses[stat] = 0;
                }
                finalBonuses[stat] += value;
            })
        }
    })

    return finalBonuses;
}

/*** ENDPOINTS ***/
exports.showEquipmentAndItems = function (req, res) {
    var player = req.player;

    if(req.params.hero_id === undefined) {
        return res.send({error: "Hero param does not exist!"});
    }

    exports.get(player, req.params.hero_id)
        .then(function (heroEquipment) {
            var itemIds = getItemsRequired(heroEquipment);
            return Promise.all([heroEquipment, Item.getItems(player, itemIds)]);
        })
        .spread(function (heroEquipment, items) {
            return res.send({ equipment: heroEquipment, items: items });
        })
        .catch(function(err) {
            //console.log(err)
            return res.send({ error: 'Failed to get hero equipment!' });
        });
};

exports.postCraft = function (req, res) {
    var player = req.player;

    var bodyKeys = ['hero_id', 'equipment_id'];

    _.each(bodyKeys, function(key) {
        if(req.body[key] === undefined) {
            return res.send({error: "Missing key for crafting!"});
        }
    });

    exports.craft(player, req.body.hero_id, req.body.equipment_id)
        .then(function () {
            return exports.get(player, req.body.hero_id);
        })
        .then(function (heroEquipment) {
            var itemIds = getItemsRequired(heroEquipment);
            return Promise.all([heroEquipment, Item.getItems(player, itemIds)]);
        })
        .spread(function (heroEquipment, items) {
            return res.send({ equipment: heroEquipment, items: items });
        })
        .catch(function(err) {
            //console.log(err)
            return res.send({ error: 'Failed to craft!' });
        });
};

exports.reset = function (req, res) {
    var player = req.player;

    if(Game.DEBUG === false) {
        return res.send({ error: '' });
    }

    return Equipment.gameObject.hmGet(player._id, req.body.hero_id)
        .then(function (heroEquipment) {
            _.each(heroEquipment.equipment, function (equipment) {
                equipment.level = 0;
            });

            return Equipment.gameObject.hSet(player._id, req.body.hero_id, heroEquipment);
        })
        .then(function () {
            return res.send({reset:"reset!"});
        });
};
