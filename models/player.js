"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var PlayerSchema = new mongoose.Schema({
    clientId:       { type: String, required: true, unique: true },
    name:           { type: String, default: "New Player" },
    attack:         { type: Number, default: 1},
    defense:        { type: Number, default: 1},
    health:         { type: Number, default: 100},
    healthMax:      { type: Number, default: 100},
    energy:         { type: Number, default: 10 },
    energyMax:      { type: Number, default: 10 },
    stamina:        { type: Number, default: 10 },
    staminaMax:     { type: Number, default: 10 },
    experience:     { type: Number, default: 0 },
    coins:           { type: Number, default: 10 },
    gems:           { type: Number, default: 10 },
    squadId:        { type: String, default: ""},
    lastRecharge:   { type: Date, default: Date.now },
    date:           { type: Date, default: Date.now }
});

// Execute before each user.save() call
PlayerSchema.pre('save', function(callback) {
    //var player = this;
    callback();
});

PlayerSchema.statics.gameObject = null;

PlayerSchema.statics.resourceKey = function(objectId) {
    return 'players:'+objectId.toString();
};

PlayerSchema.methods.getAttack = function() {
    return this.attack;
};

PlayerSchema.methods.addProp = function(prop, value) {
    if(Player.gameObject.validateProp(prop, value)) {
        this[prop] += value;
        if(this.hasOwnProperty(this[prop+'Max']) && this[prop+'Max'] < this[prop]) {
            this[prop] = this[prop+'Max'];
        }
        return true;
    }

    throw new Error('Invalid player property!');
};

PlayerSchema.methods.subProp = function(prop, value) {
    if(Player.gameObject.validateProp(prop, value)) {
        this[prop] -= value;
        if(this[prop] < 0) { this[prop] = 0; }
        return true;
    }

    throw new Error('Invalid player property!');
};

PlayerSchema.methods.addExperience = function (value) {
    var currentLevel = this.getLevelForXP();
    this.experience += value;


    var newLevel = this.getLevelForXP();
    if(newLevel > currentLevel) {
        // level up!
        this.level = newLevel;
    }
};

// Export the Mongoose model
var Player = mongoose.model('Player', PlayerSchema);
Player.gameObject = new GameObject({model: Player});
module.exports = Player;
