// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var BattleSchema = new mongoose.Schema({
    battleId:   { type: Number, required: true },
    ownerId:    { type: String, required: true },
    players:    { type: Object, required: true },
    minions:    { type: Object, required: true },
    public:     { type: Boolean, default: false },
    state:      { type: Number, default: 0},
    level:      { type: Number, default: 1},
    createdAt:  { type: Date, default: Date.now }
});

BattleSchema.pre('findOne', function(callback){
    var query = this;

    if(mongoose.Types.ObjectId.isValid(query._conditions._id)) {
        return callback();
    }

    throw new Error('_id query is not an object id!');
});

BattleSchema.statics.gameObject = null;

BattleSchema.statics.resourceKey = function(objectId) {
    return 'battles:'+objectId.toString();
};

// Export the Mongoose model
var Battle = mongoose.model('Battle', BattleSchema);
Battle.gameObject = new GameObject({model: Battle});
module.exports = Battle;
