var express = require('express');
var adminRouter = express.Router();
var authController = require('../controllers/auth');
var adminController = require('../controllers/admin');

// Admin
// Only used for adding admins then comment out!
adminRouter.route('/?')
    .post(authController.isAdminAuthenticated, adminController.postAdmins);

module.exports = adminRouter;
