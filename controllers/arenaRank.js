"use strict";

// Load required packages
var ArenaRank = require('../models/arenaRank');
var ArenaList = require('../controllers/arenaList');
var Player = require('../models/player');
var Game = require('../controllers/game');
var Promise = require('bluebird');
var _ = require('lodash');

exports.getArenaRank = function (owner_id)
{
    return ArenaRank.gameObject.get(owner_id)
        .then(function(rankData) {
            if (rankData === null)
            {
                rankData = new ArenaRank({
                    ownerId : owner_id,
                    score : 0,
                    tier : 0
                });

                return ArenaRank.gameObject.set(rankData)
                    .then(function () {
                        return rankData;
                    })
            }
            return rankData;
        });
};

exports.pointsForWin = function(winner_score, loser_score)
{
    return Math.ceil((winner_score - loser_score) / 1000);
};

exports.pointsForLoss = function(winner_Score, loser_score)
{
    return Math.ceil(winner_score - loser_score) / 2000;
};

exports.addArenaScore = function (owner_id, points)
{
    var newRankData;

    if (points <= -1000 || points >= 1000)
    {
        throw Error("Attempt to add arena score too large");
    }

    return ArenaRank.gameObject.lock(owner_id)
        .then( function (lockedRankData) {
            newRankData = lockedRankData;

            if (points <= -1000 || points >= 1000)
            {
                throw Error("Attempt to add arena score too large");
            }

            newRankData.arenaScore += points;

            return ArenaRank.gameObject.unlock(newRankData);
        })
        .then(function () {
            return ArenaList.incrementScoreFor(owner_id, points);
        });
};

exports.subtractArenaScore = function (owner_id, points)
{
    var newRankData;

    if (points >= 0)
    {
        throw Error("Attempt to add arena score too large");
    }

    return ArenaRank.gameObject.lock(owner_id)
        .then( function (lockedRankData) {
            newRankData = lockedRankData;

            if (points >= 0)
            {
                throw Error("Attempt to add arena score too large");
            }

            newRankData.arenaScore -= points;

            return ArenaRank.gameObject.unlock(newRankData);
        })
        .then(function () {
            return ArenaList.incrementScoreFor(owner_id, points);
        }).then(function () {
            return newRankData;
        });
};


/*** ENDPOINTS ***/
exports.testScores = function (req, res) {
    if(Game.DEBUG === false) {
        return res.send("");
    }

    var query = [
        // specify fields
        { "$project" : {
            "name" : 1 ,
            "experience" : 1,
            "coin" : 1,
            "squad" : 1
        } },
        // random sample pipeline
        { "$sample": { "size" : 10 } }
    ];

    var players;

    return Player.aggregate(query).exec()
        .then(function (queryResults) {
            players = queryResults;

            var rankPromises = [];

            _.each(players, function (p) {
                rankPromises.push(exports.getArenaRank(p._id));
            });

            return Promise.all(rankPromises);
        })
        .then(function () {
            console.log('got all rank data')

            var scorePromises = [];

            _.each(players, function (p) {
                var score = _.random(1,100);
                scorePromises.push(exports.addArenaScore(p._id, score));
            });

            return Promise.all(scorePromises);
        })
        .then(function () {
            return res.send("Scores saved for "+players.length+" players!");
        });
};
