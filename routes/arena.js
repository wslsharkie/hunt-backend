"use strict";

var express = require('express');
var arenaRouter = express.Router();
var authController = require('../controllers/auth');
var arena = require('../controllers/arena');
var arenaList = require('../controllers/arenaList');
var arenaRank = require('../controllers/arenaRank');

arenaRouter.route('/create/?')
    .post(authController.isBearerAuthenticated, arena.createBattle);

arenaRouter.route('/list/')
    .get(authController.isBearerAuthenticated, arenaList.getArenaList);

arenaRouter.route('/test/rank')
    .post(authController.isBearerAuthenticated, arenaRank.testScores);

module.exports = arenaRouter;
