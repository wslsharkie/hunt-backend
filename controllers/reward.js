"use strict";

var Game = require('../controllers/game');
var Item = require('../controllers/item');
var Hero = require('../controllers/hero');
var Battle = require('../controllers/battle');
var ArenaRank = require('../controllers/arenaRank');
var jsonfile = require('jsonfile');
var _ = require('lodash');

exports.TYPE = {
    ITEM: 0,
    HERO_XP: 1,
    HERO: 2,
    ARENA_PTS: 3,
    GEMS: 4
};

function getConfig(rewardId) {
    var path = Game.paths.configPath+'/rewards/'+rewardId.toString() + '.json';
    return jsonfile.readFileSync(path);
}

function addReward(rewards, reward) {
    var rewarded = false;

    _.each(rewards, function (setReward, key) {
        if(setReward.type === reward.type && setReward.id === reward.id) {
            rewards[key].amount += reward.amount;
            rewarded = true;
            return false;
        }
    });

    if(rewarded === false) {
        rewards.push(reward);
    }

    return rewards;
}

exports.rewardPlayer = function(player, team, rewardId) {
    var config = getConfig(rewardId);

    if(!config) {
        throw new Error('Reward config not found!');
    }

    var rewards = [];

    var total = _.reduce(config.rewards, function (result, reward) {
        if(reward.hasOwnProperty('weight')) {
            result += reward.weight;
        }

        return result;
    }, 0);

    for(var i=0; i < config.rolls; i++) {
        var roll = _.random(1, total);
        var weight = 0;

        _.each(config.rewards, function (reward) {
            weight += reward.weight;

            if(roll <= weight) {
                rewards = addReward(rewards, reward);
                return false;
            }
        });
    }

    var itemRewards = _.filter(rewards, {type: exports.TYPE.ITEM});
    return Item.addItems(player, itemRewards)
        .then(function () {
            const gems = _.filter(rewards, {type: exports.TYPE.GEMS});
            if(gems.length > 0) {
                const gemReward = gems[0];
                player.gems += gemReward.amount;
                console.log('ADD '+gemReward.amount+' GEMS!!!!');
            }
        })
        .then(function () {
            if(team) {
                return Hero.addExperienceToHeroes(player, team, config.heroExperience);
            }
        })
        .then(function () {
            if(config.hasOwnProperty('heroRewards')) {
                var heroRewardsPromises = _.map(config.heroRewards, function (heroId) {
                    return Hero.hasHero(player, heroId)
                        .then(function (ownsHero) {
                            if(ownsHero === false) {
                                rewards.push({
                                    type: exports.TYPE.HERO,
                                    id: heroId,
                                    amount: 1,
                                    name: Hero.getHeroConfig(heroId).name
                                });
                                return Hero.addHero(player, heroId);
                            }

                            return ownsHero;
                        });
                });

                return Promise.all(heroRewardsPromises);
            }

            return true;
        })
        .then(function () {
            if(config.heroExperience > 0) {
                rewards.push({
                    type: exports.TYPE.HERO_XP,
                    amount: config.heroExperience,
                    desc: "Hero XP"
                });
            }

            return _.reduce(rewards, function (result, reward) {
                switch (reward.type) {
                    case exports.TYPE.ITEM: case exports.TYPE.GEMS:
                        var rewardItem = _.clone(Item.getItemConfig(reward.id));
                        if(rewardItem === null) {
                            throw new Error('Item reward '+reward.id+' not found!');
                        }

                        rewardItem.amount = reward.amount;
                        result.push(rewardItem);
                        break;
                    default:
                        result.push(reward);
                        break;
                }

                return result;
            }, []);
        });
};

exports.rewardArena = function (battle, player, team, win) {
    var rewards = [];

    var opponentId = '';

    for(var playerId in battle.players) {
        var p = battle.players[playerId];

        if(p.hasOwnProperty('opponent') && p.opponent === true) {
            opponentId = playerId;
            break;
        }
    }

    if(opponentId === '') {
        throw new Error('Unable to reward arena player with opponent ID!');
    }

    return Promise.all([ArenaRank.getArenaRank(player._id), ArenaRank.getArenaRank(opponentId)])
        .spread(function (playerRank, opponentRank) {
            if(win) {
                return saveScores(player._id, playerRank.score, opponentId, opponentRank.score);
            } else {
                return saveScores(opponentId, opponentRank.score, player._id, playerRank.score);
            }
        })
        .spread(function (score1, score2) {
            if(win) {
                rewards.push({
                    type: exports.TYPE.ARENA_PTS,
                    amount: score1,
                    desc: "You won "+score1+" Arena Points!"
                });

                rewards.push({
                    type: exports.TYPE.ARENA_PTS,
                    amount: score2,
                    desc: "Your opponent lost "+score2+" Arena Points!"
                });
            } else {
                rewards.push({
                    type: exports.TYPE.ARENA_PTS,
                    amount: score2,
                    desc: "You lost "+score2+" Arena Points!"
                });

                rewards.push({
                    type: exports.TYPE.ARENA_PTS,
                    amount: score1,
                    desc: "Your opponent won "+score1+" Arena Points!"
                });
            }

            var heroXP = Math.ceil(battle.players[player._id].energy * 1.5);
            rewards.push({
                type: exports.TYPE.HERO_XP,
                amount: heroXP,
                desc: "Hero XP"
            });
            return Hero.addExperienceToHeroes(player, team, heroXP);
        })
        .then(function () {
            return rewards;
        });
};

function saveScores(winnerId, winnerScore, loserId, loserScore) {
    var winnerPoints = ArenaRank.pointsForWin(winnerScore, loserScore);
    var loserPoints = ArenaRank.pointsForLoss(winnerScore, loserScore);

    return Promise.all([
        ArenaRank.addArenaScore(winnerId, winnerPoints),
        ArenaRank.subtractArenaScore(loserId, loserPoints)
    ]);
}

/*** ENDPOINTS ***/

exports.showRewardsForBattle = function (req, res) {
    //var player = req.player;

    if(req.body.battle_id === undefined) {
        return res.send({error: "Battle ID does not exist for loot!"});
    }

    var battleId = parseInt(req.body.battle_id);

    var config = Battle.getConfig(battleId);
    if(!config) {
        throw new Error('Battle config not found!');
    }

    if(config.hasOwnProperty('rewardsId') === false) {
        throw new Error('Reward ID not found!');
    }
    
    var rewards = getConfig(config.rewardsId);

    if(!rewards) {
        throw new Error('Reward config not found!');
    }

    var response = {};
    var fields = ["id", "type", "amount"];

    rewards.rewards.forEach(function (reward) {
        response[reward.id] = {};

        fields.forEach(function (field) {
            response[reward.id][field] = reward[field];
        });
    });

    return res.send({rewards: response});
};


