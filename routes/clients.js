var express = require('express');
var clientRouter = express.Router();
var authController = require('../controllers/auth');
var clientController = require('../controllers/client');
var UserController = require('../controllers/user');

clientRouter.route('/?')
    .post(authController.isNewClientAuthenticated, clientController.postClients);

clientRouter.route('/code')
    .post(authController.isBearerAuthenticated, UserController.requestCode);

clientRouter.route('/recover')
    .post(authController.isBearerAuthenticated, UserController.recoverAccount);

module.exports = clientRouter;
