var express = require('express');
var equipmentRouter = express.Router();
var authController = require('../controllers/auth');
var equipmentController = require('../controllers/equipment');

equipmentRouter.route('/:hero_id')
    .get(authController.isBearerAuthenticated, equipmentController.showEquipmentAndItems);

equipmentRouter.route('/craft')
    .post(authController.isBearerAuthenticated, equipmentController.postCraft);

// // testing:
equipmentRouter.route('/reset')
    .post(authController.isBearerAuthenticated, equipmentController.reset);

module.exports = equipmentRouter;
