"use strict";

var SquadDonation = require('../models/squadDonation');
var SquadFeedController = require('../controllers/squadFeed');
var Item = require('../controllers/item');
var Hero = require('../controllers/hero');
var Squad = require('../controllers/squad');
var Player = require('../models/player');
var Promise = require('bluebird');
var jsonfile = require('jsonfile');
var Game = require('../controllers/game');
var _ = require('lodash');

exports.TYPE = {};
exports.TYPE.SWORD = 0;

exports.equipment = {};
exports.heroSets = {}; // heroId : [equipmentId, ...]

exports.allowedDonations = {};

const COOLDOWN = 28800000;

exports.init = function() {
    var path = Game.paths.configPath+'/donations/donations.json';
    exports.allowedDonations = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.allowedDonations);
};

exports.get = function (squadId) {
    return SquadDonation.gameObject.hGetAll(squadId)
        .then(function (donations) {
            return donations;
        });
};

exports.getRequestFor = function (squadId, requestorId)
{
    return SquadDonation.gameObject.hmGet(squadId, requestorId)
        .then(function (request) {
            return request;
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err.message);
        });
}

exports.create = function (squadId, requestorId, item_id) {
    var config = Item.getItemConfig(item_id);

    if(config === null || exports.allowedDonations.hasOwnProperty(item_id) === false) {
        throw new Error(item_id +' cannot be requested!');
    }

    var instance = {
        requestorId: requestorId,
        ownerId:     squadId,
        itemId:    item_id,
        donators:   {},
        donated:    0,
        createdAt:  Date.now()
    };

    return new SquadDonation(instance);
};

exports.getMaxDonations = function(item_id)
{
    if (exports.allowedDonations.hasOwnProperty(item_id))
    {
        return exports.allowedDonations[item_id];
    } else {
        return 0;
    }

}

exports.getDonationOptions = function(squadId, requestorId)
{
    var availableOptions = {};
    return exports.getRequestFor(squadId, requestorId)
        .then(function(playerRequest) {
            if (checkCooldownReset(playerRequest))
            {
                _.each(exports.allowedDonations, function(amount, item_id) {
                    availableOptions[item_id] = amount;
                })
            }
            return availableOptions;
        })
}

function checkCooldownReset(request) {
    if (request === null || _.isEmpty(request))
    {
        return true;
    }

    if ((Date.now() - request.createdAt) < COOLDOWN)
    {
        return true;
    }

    return false;
}



/*** ENDPOINTS ***/
exports.createRequest = function (req, res)
{
    var player = req.player;
    var newRequest = {};
    var requestor_ID = player._id.toString();

    if(req.body.item_id === undefined) {
        return res.send({error: "Item ID does not exist!"});
    }
    var item_id = req.body.item_id;

    if(req.body.override_id !== undefined)
    {
        requestor_ID = req.body.override_id;
    }

    if (player.squadId === null || player.squadId.length == 0)
    {
        throw new Error("You must be in a Squad before you can create a donation request.");
    }
    return exports.getRequestFor(player.squadId, requestor_ID.toString())//Promise.all([Squad.get(player.squadId), exports.getRequestFor(player.squadId, player._id.toString())])
        .then(function (oldData) {
            if (!_.isEmpty(oldData)
                && oldData.hasOwnProperty('createdAt')
                && checkCooldownReset(oldData) === false)
            {
                var remaining = (COOLDOWN - _.ceil(Date.now() - oldData.createdAt)) / 1000;
                var minutes = _.ceil(remaining / 60) % 60;
                var hours = _.ceil(remaining / 60 / 60) % 24;
                if (hours > 0)
                {
                    throw new Error("Oops! You still have " + hours + "h" + minutes + "m remaining on your donation request cooldown.");
                } else if (minutes > 0) {
                    throw new Error("Oops! You still have " + minutes + "min remaining on your donation request cooldown.");
                }
                throw new Error("Sorry, please try again in a minute ");
            }

            //otherwise make a new request and save it - no block needed since only one can be made accessible anyhow
            //TODO: consider putting this in a block

            newRequest = exports.create(player.squadId, requestor_ID, item_id);
            return SquadDonation.gameObject.hSet(player.squadId, requestor_ID, newRequest)
        })
        .then(function(setResults) {
            // console.log("O.O O.O O.O setResults=");
            // console.log(setResults);
            return SquadFeedController.getFeed(player.squadId)
        })
        .then(function(feed) {
            return res.send({ feed: feed})
        })
        .catch(function(err) {
            //console.log(err);
            res.send({error: err.message});
        });
}

exports.donateToRequest = function(req, res)
{
    var player = req.player;
    var squad_id = '';
    var item_id = '';
    var req_id = '';
    var requestor = {};
    var request = {};

    if(req.body.item_id === undefined) {
        throw new Error("Item ID does not exist!");
    }
    item_id = req.body.item_id;

    if(req.body.req_id === undefined) {
        throw new Error("Donation request does not exist!");
    }
    req_id = req.body.req_id;

    if (player.squadId === null || player.squadId.length == 0)
    {
        throw new Error("You must be in the same squad send and receive donations");
    }
    squad_id = player.squadId;

    return Promise.all([exports.getRequestFor(squad_id, req_id), Player.gameObject.get(req_id), Item.getItem(player, item_id)])
        .spread(function(requestObj, requestorObj, ownedData) {
            request = requestObj;
            requestor = requestorObj;

            if (_.isEmpty(request)) {
                throw new Error("Donation request could not be found");
            }

            if (request.donated >= exports.getMaxDonations(item_id))
            {
                throw new Error("This donation request has already been fulfilled. Thank you!");
            }

            if (ownedData === null || ownedData.amount <= 0)
            {
                throw new Error("Sorry, you do not own this item to donate.");
            }

            return Promise.all([Player.gameObject.lock(player._id.toString()), Player.gameObject.lock(requestor._id.toString()), SquadDonation.gameObject.hmLock(squad_id, req_id), Item.getItem(player, item_id)])
        })
        .spread(function(lockedPlayer, lockedRequestor, lockedRequest, refreshedOwnedData) {
            player = lockedPlayer;
            requestor = lockedRequestor;
            request = lockedRequest;

            if (refreshedOwnedData.amount <= 0)
            {
                throw new Error("Sorry, you do not own this item to donate.");
            }

            return Item.subtractItem(player, item_id, 1)
        })
        .then(function (subResults) {
            return Item.addItem(requestor, item_id, 1)
        })
        .then(function (addResults) {
            request.donated += 1;
            if (_.isEmpty(request.donators))
            {
                request.donators = {}
            }
            request.donators[player._id.toString()] = player.name;
            return true
        })
        .then(function (donateResults) {
            return Promise.all([Player.gameObject.unlock(player), Player.gameObject.unlock(requestor), SquadDonation.gameObject.hmUnlock(request)])
        })
        .spread(function(updatedPlayer, updatedRequestor, updatedRequest) {
            player = updatedPlayer;
            requestor = updatedRequestor;
            res.send({updated: request});
        })
        .catch(function(err) {
            //console.log(err)
            Player.gameObject.revert(player);
            Player.gameObject.revert(requestor);
            SquadDonation.gameObject.revert(request);
            return res.send({ error: err.message});
         });
}
