// redis
//var bluebird = require('bluebird');
var redisModule = require('redis');
var Redlock = require('redlock');
var _redis;
var _ = require('lodash');

// debug: 6379, localhost
exports.init = function(port, host) {
    var options = {
        retry_strategy: function (options) {
            if (options.error.code === 'ECONNREFUSED') {
                // End reconnecting on a specific error and flush all commands with a individual error
                return new Error('The server refused the connection');
            }
            if (options.total_retry_time > 1000 * 60 * 60) {
                // End reconnecting after a specific timeout and flush all commands with a individual error
                return new Error('Retry time exhausted');
            }
            if (options.times_connected > 10) {
                // End reconnecting with built in error
                return undefined;
            }
            // reconnect after
            return Math.max(options.attempt * 100, 3000);
        }
    };

    _redis = redisModule.createClient(port, host, options);

    // debug redis
    _redis.on("monitor", function (time, args, raw_reply) {
        console.log(time + ": " + args); // 1458910076.446514:['set', 'foo', 'bar']
    });
};

exports.getClient = function() {
    if(!_redis) {
        throw new Error('*** Redis not initialized! ***');
    }
    return _redis;
};

exports.set = function(key, value, expire = 0) {
    return _redis.setAsync(key, JSON.stringify(value))
        .then(function() {
            if(expire > 0) {
                _redis.expireAsync(key, expire)
                    .then(function(res) {
                        if(res === 0) {
                            throw new Error('Unable to set expire time on key '+key);
                        }
                        return true;
                    });
            }
            return true;
        })
        .catch(function(err){
            // console.log('Redis failed to set!');
            // console.log(err);
            return null;
        });
};

exports.get = function(key) {
    return _redis.getAsync(key)
        .then(function(value){
            if(value) return JSON.parse(value);
            return null;
        })
        .catch(function(err){
            //console.log(err);
            return null;
        });
};

exports.hmset = function(key, object) {
    // not allowed to set empty object
    // return true to avoid errors
    if(_.isEmpty(object)) {
        return true;
    }

    var map = {};
    for(var prop in object) {
        map[prop] = JSON.stringify(object[prop]);
    }

    return _redis.hmsetAsync(key.toString(), map)
        .catch(function(err){
            throw new Error(err);
        });
};

exports.hmget = function(key, field) {
  return _redis.hmgetAsync(key, field)
      .then(function(value){
          if(value) return JSON.parse(value);
          return null;
      })
      .catch(function(err){
          // console.log(err);
          return null;
      });
};

exports.hgetall = function(key) {
    return _redis.hgetallAsync(key)
        .then(function(object) {
            if(object === null) {
                return null;
            }

            var map = {};
            for(var prop in object) {
                map[prop] = JSON.parse(object[prop]);
            }

            return map;
        })
        .catch(function(err){
            // console.log(err);
            return null;
        });
};

exports.mget = function(keys) {
    return _redis.mgetAsync(keys)
        .then(function(values){
            var parsedValues = [];
            values.forEach(function(value) {
                parsedValues.push(JSON.parse(value));
            });

            return parsedValues;
        })
        .catch(function(err){
            //console.log(err);
            return null;
        });
};

exports.sadd = function (key, member) {
    return _redis.saddAsync(key, member);
};

exports.srem = function (key, member) {
    return _redis.sremAsync(key, member);
};

exports.smembers = function (key) {
    return _redis.smembersAsync(key)
        .then(function(value){
            if(value) {
                return value;
            }
            return null;
        })
        .catch(function(err){
            // console.log(err);
            return null;
        });
};

exports.zadd = function (key, score, member) {
    return _redis.zaddAsync(key, score, member)
        .then( function(returnedScore) {
            //console.log("zAddAsync returned score: " + returnedScore);
            return returnedScore;
        })
        .catch (function (err) {
            //console.log("Redis failed to zAdd");
            //console.log(err);
            return null;
        })
};

exports.zrevrange = function (key, start, end) {
    return _redis.zrevrangeAsync(key, start, end)
        .catch (function (err) {
            //console.log("Redis failed to zRevRange");
            //console.log(err);
            return null;
        })
};

exports.zrevrangewithscores = function (key, start, end) {
    return _redis.zrevrangeAsync(key, start, end, 'WITHSCORES')
        .then( function (object) {
            var finalList = [];
            var i = 0;
            var memberToSave = '';

            _.each(object, function(row) {
                if (i % 2 === 0)
                {
                    memberToSave = row;
                } else {
                    finalList.push( {id : memberToSave, score : row} );
                }
                i++;
            });

            return finalList;
        })
        .catch (function (err) {
            //console.log("Redis failed to zRevRange with scores");
            //console.log(err);
            return null;
        })
};

exports.zrange = function (key, start, end) {
    return _redis.zrangeAsync(key, start, end)
        .catch (function (err) {
            //console.log("Redis failed to zRange");
            //console.log(err);
            return null;
        })
};

exports.zcount = function (key) {
    return _redis.zcountAsync(key, '-inf', '+inf')
        .then(function (results) {
            //console.log("zcount for key:" + key + " results=");
            //console.log(results);
            return results;
        })
        .catch (function (err) {
            //console.log("Redis failed to zcount");
            //console.log(err);
            return null;
        })
};

exports.zcountbetween = function (key, min, max) {
    return _redis.zcountAsync(key, min, max)
        .then(function (results) {
            //console.log("zcountbetween " + min + " to " + max + " for key:" + key + " results=");
            //console.log(results);
            return results;
        })
        .catch (function (err) {
            //console.log("Redis failed to zcountbetween");
            //console.log(err);
            return null;
        })
};

exports.zincrby = function (key, points, member_id) {
    return _redis.zincrbyAsync(key, points, member_id)
        .then(function (results) {
            console.log("zincrby for key:" + key + " with points:" + points + " results=");
            console.log(results);
            return results;
        })
        .catch (function (err) {
            console.log("Redis failed to zincrby");
            console.log(err);
            return null;
        })
};

exports.zrank = function (key, member_id) {
    return _redis.zrankAsync(key, member_id)
        .then(function (results) {
            console.log("zrank for key:" + key + " and member_id:" + member_id + " results=");
            console.log(results);
            return results;
        })
        .catch (function (err) {
            console.log("Redis failed to zrank");
            console.log(err);
            return null;
        })
};

exports.zrevrank = function (key, member_id) {
    return _redis.zrevrankAsync(key, member_id)
        .then(function (results) {
            console.log("zrank for key:" + key + " and member_id:" + member_id + " results=");
            console.log(results);
            return results;
        })
        .catch (function (err) {
            console.log("Redis failed to zrank");
            console.log(err);
            return null;
        })
};

exports.zscore = function (key, member_id) {
    return _redis.zscoreAsync(key, member_id)
        .then(function (results) {
            console.log("zscore for key:" + key + " and member_id:" + member_id + " results=");
            console.log(results);
            return results;
        })
        .catch (function (err) {
            console.log("Redis failed to zscore");
            console.log(err);
            return null;
        })
};

exports.lpush = function (key, value) {
    if(typeof value === 'object') {
        value = JSON.stringify(value);
    } else {
        value = value.toString();
    }

    return _redis.lpushAsync(key, value)
        .then(function (length) {
            //  length of the list after the push operation
            return length;
        });
};

exports.lrange = function (key, start, stop) {
    return _redis.lrangeAsync(key, start, stop)
        .then(function (list) {
            if(list && list.length > 0) {
                return _.map(list, function (value) {
                    // is object?
                    if(value.includes('{') && value.includes('}')) {
                        value = JSON.parse(value);
                    }

                    return value;
                });
            }
        })
        .catch(function () {
            return null;
        });
};

exports.ltrim = function (key, start, stop) {
    return _redis.ltrimAsync(key, start, stop);
};

exports.lock = function(key, ttl=6000) {
    var resource = 'locks:'+key;

    var redlock = new Redlock(
        // you should have one client for each redis node
        // in your cluster
        [_redis],
        {
            // the expected clock drift; for more details
            // see http://redis.io/topics/distlock
            driftFactor: 0.01,

            // the max number of times Redlock will attempt
            // to lock a resource before erroring
            retryCount:  7,

            // the time in ms between attempts
            retryDelay:  200
        }
    );

    redlock.on('clientError', function(err) {
        console.error('A redis error has occurred:', err);
    });

    return redlock.lock(resource, ttl).then(function(lock) {
        return lock;
    });
};

exports.unlock = function(lock) {
    // unlock your resource when you are done
    return lock.unlock()
        .catch(function(err) {
            // we weren't able to reach redis; your lock will eventually
            // expire, but you probably want to log this error
            console.error(err);
        });
};

exports.delete = function(key) {
    return _redis.delAsync(key)
        .catch(function(err){
            throw new Error(err);
        });
};
