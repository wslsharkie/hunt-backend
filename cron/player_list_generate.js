"use strict";

var Promise = require('bluebird');
var _ = require('lodash');

var redis = require('../controllers/redis');

var Player = require('../models/player');

exports.LIST_MAX = 20;
exports.EXPIRE_TIME = 90; // seconds

exports.levels = [
    {'level':1, 'min':2, 'max':2},
    {'level':2, 'min':2, 'max':2},
    {'level':3, 'min':3, 'max':3},
    {'level':4, 'min':4, 'max':4},
    {'level':5, 'min':5, 'max':7},
    {'level':10, 'min':7, 'max':11},
    {'level':15, 'min':10, 'max':16},
    {'level':20, 'min':15, 'max':21},
    {'level':25, 'min':20, 'max':26},
    {'level':30, 'min':25, 'max':31},
    {'level':35, 'min':30, 'max':36},
    {'level':40, 'min':35, 'max':41},
    {'level':45, 'min':40, 'max':50},
    {'level':50, 'min':45, 'max':55},
    {'level':55, 'min':50, 'max':60},
    {'level':60, 'min':55, 'max':65},
    {'level':65, 'min':60, 'max':70},
    {'level':70, 'min':65, 'max':75},
    {'level':75, 'min':70, 'max':80},
    {'level':80, 'min':75, 'max':85},
    {'level':85, 'min':80, 'max':95},
    {'level':90, 'min':85, 'max':100},
    {'level':95, 'min':85, 'max':110},
    {'level':100, 'min':90, 'max':115},
    {'level':125, 'min':115, 'max':140},
    {'level':150, 'min':130, 'max':160},
    {'level':175, 'min':160, 'max':180},
    {'level':200, 'min':180, 'max':210},
    {'level':250, 'min':200, 'max':260},
    {'level':300, 'min':250, 'max':310},
    {'level':350, 'min':300, 'max':370},
    {'level':400, 'min':350, 'max':450},
    {'level':500, 'min':400, 'max':600},
    {'level':700, 'min':550, 'max':5000}
];

exports.generate = function() {
    _.map(exports.levels, function (range) {
        var query = [
            // Grouping pipeline
            { "$match": {
                "level": {"$gte":range.min, "$lte": range.max}
            } },
            // specify fields
            { "$project" : {
                "name" : 1 ,
                "health" : 1,
                "level" : 1,
                "coin" : 1,
                "squad" : 1
            } },
            // random sample pipeline
            { "$sample": { "size" : exports.LIST_MAX } }
        ];

        Player.aggregate(query).exec()
            .then(function (players) {
                if(players.length > 0) {
                    redis.set(exports.resourceKey(range.level), players, exports.EXPIRE_TIME);
                }
            });
    });
};

exports.resourceKey = function(level) {
    return 'player_list:'+level;
};
