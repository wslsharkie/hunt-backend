"use strict";

// Load required packages
var Essence = require('../models/essence');
var Hero = require('../controllers/hero');
// var Game = require('../controllers/game');
var Player = require('../models/player');
var jsonfile = require('jsonfile');
var _ = require('lodash');

exports.createEssence = function (playerId, heroId) {
    var config = Hero.getHeroConfig(heroId);

    if(config === null) {
        return null;
    }

    var instance = {
        heroId:     heroId,
        ownerId:    playerId,
        essence:    0
    };

    return new Essence(instance);
};

exports.getEssence = function (player, heroId) {
    return Essence.gameObject.hmGet(player._id, heroId)
        .then(function (essence) {
            if(essence === null) {
                return exports.createEssence(player._id, heroId);
            }

            return essence;
        });
};

exports.addEssence = function (player, heroId, value) {
    if(parseInt(value) <= 0) {
        throw new Error("Invalid Essence value");
    }

    return exports.getEssence(player, heroId)
        .then(function (essence) {
            essence.essence += parseInt(value);
            return Essence.gameObject.hSet(player._id, heroId, essence);
        });
};

exports.subEssence = function (player, heroId, value) {
    if(parseInt(value) <= 0) {
        throw new Error("Invalid Essence value");
    }

    return exports.getEssence(player, heroId)
        .then(function (essence) {
            if (essence.essence < value)
            {
                throw new Error("Not enough Essence");
            }
            essence.essence -= parseInt(value);
            if(essence.essence < 0) {
                essence.essence = 0;
                throw new Error("Essence can not be less than 0!");
            }
            return Essence.gameObject.hSet(player._id, heroId, essence);
        });
};

exports.get = function (player) {
    return Essence.gameObject.hGetAll(player._id)
        .then(function (essenceHash) {
            return essenceHash;
        });
};
