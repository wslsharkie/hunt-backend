// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var StageSchema = new mongoose.Schema({
    stageBattle:    { type: String, required: true }, // stageId + ':' + battleId
    ownerId:        { type: String, required: true },
    energy:         { type: Number, default: 0},
    completed:      { type: Number, default: 0},
    createdAt:      { type: Number, default: Date.now() }
});

StageSchema.statics.gameObject = null;

StageSchema.statics.resourceKey = function(objectId) {
    return 'stages-'+objectId.toString();
};

StageSchema.statics.fieldKey = function () {
    return 'stageBattle';
};

// Export the Mongoose model
var Stage = mongoose.model('Stage', StageSchema);
Stage.gameObject = new GameObject({model: Stage});
module.exports = Stage;
