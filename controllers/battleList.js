"use strict";

const BattleList = require('../models/battleList');
const Battle = require('../models/battle');
const Game = require('../controllers/game');
const Promise = require('bluebird');
const _ = require('lodash');

exports.LIST_SCHEMA = ['id', 'ownerId', 'type', 'expireTime', 'createdAt'];

exports.get = function(id) {
    return BattleList.gameObject.get(id)
        .then(function (battleList) {
            if(battleList === null) {
                var instance = {
                    ownerId:    id,
                    list:       [],
                    createdAt:  Date.now()
                };

                battleList = new BattleList(instance);
                return BattleList.gameObject.set(battleList)
                    .then(function (newBattleList) {
                        return Promise.all([newBattleList, exports.clean(newBattleList.list)]);
                    });
            }

            return Promise.all([battleList, exports.clean(battleList.list)]);
        })
        .spread(function (battleList, cleanedList) {
            battleList.list = cleanedList;
            return new BattleList(battleList);
        });
};

exports.clean = function(list) {
    var newList = [];

    _.reduce(list, function (result, listItem) {
        var removeItem = false;

        if(listItem.hasOwnProperty('createdAt') === false
            || listItem.hasOwnProperty('expireTime') === false) {
            removeItem = true;
        }

        if(listItem.hasOwnProperty('createdAt')
            && listItem.hasOwnProperty('expireTime')) {
            var currentTime = Date.now();

            if(currentTime > listItem.expireTime +  listItem.createdAt) {
                removeItem = true;

                // delete the battle if expired
                Battle.gameObject.delete({name: '_id', value: listItem.id});
            }
        }

        if(listItem.hasOwnProperty('rewarded')) {
            removeItem = true;
        }

        if(removeItem === false) {
            result.push(listItem);
        }

        return result;
    }, newList);

    return newList;
};

exports.add = function (player, listItem) {
    _.each(exports.LIST_SCHEMA, function (key) {
        if(listItem.hasOwnProperty(key) === false) {
            throw new Error("Missing key "+key+"for battle list item!");
        }
    });

    return exports.get(player._id)
        .then(function (battleList) {
            if(Game.DEBUG === false && listItem.config.type === exports.TYPE.STAGE) {
                var result = _.filter(battleList.list, {'type': exports.TYPE.STAGE});
                if(result.length > 0) {
                    throw new Error('Only one stage battle may be created!');
                }
            }

            var duplicateItems = _.filter(battleList.list, {id: listItem.id});
            if(duplicateItems.length > 0) {
                throw new Error("This battle already exists in the list!");
            }

            battleList.list.push(listItem);

            return BattleList.gameObject.update(battleList);
        })
        .catch(function (err) {
            //console.log(err)
            throw new Error(err.message);
        });
};

exports.delete = function (player, id) {
    return exports.get(player._id)
        .then(function (battleList) {
            var listCount = battleList.list.length;
            battleList.list = _.reject(battleList.list, {id: id.toString()});

            if(listCount > battleList.list.length) {
                return BattleList.gameObject.update(battleList);
            }

            return false;
        });
};

exports.markRewarded = function (player, id) {
    return exports.get(player._id)
        .then(function (battleList) {
            var rewardedListItem = false;

            _.each(battleList.list, function (listItem) {
                if(listItem.id.toString() === id.toString()) {
                    listItem.rewarded = true;
                    rewardedListItem = true;
                    return false;
                }
            });

            if(rewardedListItem) {
                return BattleList.gameObject.update(battleList);
            }
        });
};

/*** ADMIN ***/
exports.destroy = function (req, res) {
    var player = req.player;

    if(Game.DEBUG !== true) {
        res.send({error: "No Admin!"})
    }

    return exports.get(player._id)
        .then((battleList) => {
            BattleList.gameObject.delete({name:'_id', value: battleList._id});
        })
        .then(() => {
            res.send({error: "OK!"});
        });
};
