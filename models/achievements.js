"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');
var _ = require('lodash');

// Define our user schema
var AchievementsSchema = new mongoose.Schema({
    ownerId:        { type: String, required: true},
    achId:          { type: Number, required: true},
    achVal:         { type: Number, required: true},
    collected:      { type: Number, required: true},
    last_updated:   { type: Date, required: true, default: Date.now },
    date:           { type: Date, required: true, default: Date.now }
});

// Previous schema
// var AchievementsSchema = new mongoose.Schema({
//     ownerId:        { type: String, required: true, unique: true },
//     achId:          { type: Number, required: true},
//     active_dailies: { type: Array, required: true},
//     daily_timestap: { type: Number, required: true},
//     createdAt:      { type: Number, required: true },
// });

AchievementsSchema.statics.gameObject = null;

AchievementsSchema.statics.resourceKey = function(objectId) {
    return 'achievements:'+objectId.toString();
};

AchievementsSchema.statics.fieldKey = function () {
    return 'achId';
};

// Export the Mongoose model
var Achievements = mongoose.model('Achievements', AchievementsSchema);
Achievements.gameObject = new GameObject({model: Achievements});
module.exports = Achievements;
