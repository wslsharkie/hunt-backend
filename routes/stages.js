"use strict";

var express = require('express');
var stageRouter = express.Router();
var authController = require('../controllers/auth');
var stageController = require('../controllers/stage');

stageRouter.route('/?')
    .get(authController.isBearerAuthenticated, stageController.showStages);

stageRouter.route('/:stage_id')
    .post(authController.isBearerAuthenticated, stageController.showStage);

stageRouter.route('/:stage_id/battles/:battle_id')
    .post(authController.isBearerAuthenticated, stageController.startBattle);

module.exports = stageRouter;
