"use strict";

// Load required packages
var Battle = require('../controllers/battle');
var BattleModel = require('../models/battle');
var BattleList = require('../controllers/battleList');
var Team = require('../controllers/team');
var Hero = require('../controllers/hero');
var Game = require('../controllers/game');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
//var Reward = require('../controllers/reward');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

var ArenaConfigId = 2000;

exports.create = function(player, opponent) {
    var content = Battle.getConfig(ArenaConfigId);

    if(!content) {
        throw new Error('Battle constant not found!');
    }

    var config = content;

    var playerTeam, opponentTeam, battle;

    return Player.gameObject.lock(player._id)
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return Promise.all([Team.getFullActiveTeam(player), Team.getFullActiveTeam(opponent)]);
        })
        .spread(function (pTeam, oTeam) {
            playerTeam = pTeam;
            opponentTeam = oTeam;

            var players = {};
            players[player._id] = {team: Battle.getTeamSnapshot(playerTeam), energy: 0};
            players[opponent._id] = {
                opponent: true,
                name: opponent.name,
                level: PlayerController.getLevel(opponent)
            };

            var instance = {
                battleId:   config.id, // not used for arena battle so it can be any number
                ownerId:    player._id,
                players:    players,
                minions:    getOpponentTeamSnapshot(opponentTeam),
                public:     false
            };

            var newBattle = new BattleModel(instance);
            return BattleModel.gameObject.set(newBattle);
        })
        .then(function (newBattle) {
            battle = newBattle;

            // add to list
            var listItem = {
                id:         battle._id,
                ownerId:    battle.ownerId,
                type:       config.type,
                expireTime: config.expireTime,
                createdAt:  Date.now()
            };

            return BattleList.add(player, listItem);
        })
        .then(function () {
            return Player.gameObject.unlock(player);
        })
        .then(function () {
            return {config: config, instance: battle};
        })
        .catch(function (err) {
            //console.log(err)
            Player.gameObject.revert(player);
            throw new Error('Failed to create arena battle!');
        })
};

function getOpponentTeamSnapshot(team) {
    var snapshot = Battle.getTeamSnapshot(team);

    _.each(snapshot, function (member) {
        member.action = 0;
    });

    return snapshot;
}

/*** ENDPOINTS ***/
exports.createBattle = function(req, res) {
    var player = req.player;

    if(req.body.player_id === undefined) {
        return res.send({error: "Player ID does not exist!"});
    }

    PlayerController.get(req.body.player_id)
        .then(function (opponent) {
            return exports.create(player, opponent);
        })
        .then(function (battle) {
            return res.send({ battle: battle , images: Battle.getImages(battle.instance) });
        })
        .catch(function(err) {
            //console.log(err);
            return res.send({error: "Failed to start battle with this player!"});
        });
};
