// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Define our client schema
var ClientSchema = new mongoose.Schema({
  secret:   { type: String, required: true },
  playerId: { type: String }
});

// Execute before each client.save() call
ClientSchema.pre('save', function(callback) {
  var client = this;

  // Break out if the password hasn't changed
  if (!client.isModified('secret')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(client.secret, salt, null, function(err, hash) {
      if (err) return callback(err);
      client.secret = hash;
      callback();
    });
  });
});

ClientSchema.methods.verifySecret = function(secret) {
  return bcrypt.compareSync(secret, this.secret);
};

// Export the Mongoose model
module.exports = mongoose.model('Client', ClientSchema);