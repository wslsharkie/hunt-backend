"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var ArenaRankSchema = new mongoose.Schema({
    ownerId:    { type: String, required: true, unique: true },
    score:      { type: Number, required: true },
    tier:       { type: Number, required: true },
    createdAt:  { type: Date, default: Date.now }
});

var ArenaPrefix = 'ArenaRank022017';

ArenaRankSchema.statics.gameObject = null;

ArenaRankSchema.statics.getQuery = function(id) {
    return {ownerId: id};
};

ArenaRankSchema.methods.resourceKey = function(instance) {
    return ArenaPrefix+'-'+instance.ownerId;
};

ArenaRankSchema.statics.resourceKey = function(objectId) {
    return ArenaPrefix+'-'+objectId.toString();
};

// Export the Mongoose model
var ArenaRank = mongoose.model(ArenaPrefix, ArenaRankSchema);
ArenaRank.gameObject = new GameObject({model: ArenaRank});
module.exports = ArenaRank;
