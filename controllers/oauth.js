// Load required packages
var oauth2orize = require('oauth2orize');
var Token = require('../controllers/token');

// create OAuth 2.0 server
var server = oauth2orize.createServer();

//Client Credentials Flow
server.exchange(oauth2orize.exchange.clientCredentials(function(client, scope, callback) {
    var token = Token.create(client);

    Token.set(token)
        .then(function() {
            return callback(null, {value: token.value});
        })
        .catch(function(err){
            return callback(err);
        });
}));

// token endpoint
exports.token = [
    server.token(),
    server.errorHandler()
];
