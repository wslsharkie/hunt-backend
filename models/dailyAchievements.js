"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var dailyAchievementsSchema = new mongoose.Schema({
    ownerId:    { type: String, required: true, unique: true },
    dailies:    { type: Array, required: true},
    currentDay: { type: Number, required: true},
    createdAt:  { type: Number, required: true }
});

// Execute before each user.save() call
dailyAchievementsSchema.pre('save', function(callback) {
    //var player = this;
    callback();
});

dailyAchievementsSchema.statics.gameObject = null;

dailyAchievementsSchema.statics.getQuery = function(id) {
    return {ownerId: id};
};

dailyAchievementsSchema.statics.resourceKey = function(objectId) {
    return 'dailyAchievements:'+objectId.toString();
};

// overrides the static resourceKey
dailyAchievementsSchema.methods.resourceKey = function(instance) {
    return 'dailyAchievements:'+instance.ownerId;
};

// Export the Mongoose model
var dailyAchievements = mongoose.model('dailyAchievements', dailyAchievementsSchema);
dailyAchievements.gameObject = new GameObject({model: dailyAchievements});
module.exports = dailyAchievements;
