// Load required packages
var Client = require('../models/client');
var uid = require('uid-safe');

// client side code used for making new clients
const _GameClientName = "L3%s#V[z_";
const _GameClientPassword = "87eYrHw%C+";

exports.constants = {
  _GameClientName: _GameClientName,
  _GameClientPassword: _GameClientPassword
};

exports.create = function () {
    // Create a new instance of the Client model
    var client = new Client();
    client.secret = uid.sync(18);
    client.playerId = ""; // set on oauth2 exchange

    return client.save()
        .then(function () {
            return client;
        })
        .catch(function (err) {
            throw new Error("Failed to create client!");
        });
};

/*** ENDPOINTS ***/
exports.postClients = function(req, res) {
    // Create a new instance of the Client model
    var client = new Client();
    var uidString = uid.sync(18);
    client.secret = uidString;
    client.playerId = ""; // set on oauth2 exchange

    return client.save()
        .then(function (client) {
            return res.send({ data: {_id: client._id, secret: uidString } });
        })
        .catch(function (err) {
            return res.send({ error: "Failed to create client!" });
        });
};

exports.getClients = function(req, res) {
  // Use the Client model to find all clients
  Client.find({ userId: req.user._id }, function(err, clients) {
    if (err)
      return res.send(err);

    res.json(clients);
  });
};