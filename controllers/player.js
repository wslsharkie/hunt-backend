"use strict";

// Load required packages
var Player = require('../models/player');
var ArenaRank = require('../controllers/arenaRank');
var Allies = require('../controllers/allies');
var Squad = require('../controllers/squad');
var Team = require('../controllers/team');
var Promise = require('bluebird');

const RECHARGE_TIME = 120 * 1000; // 2 minutes

exports.get = function (id) {
    var player;
    var rechargeCount = 0;

    return Player.gameObject.get(id)
        .then(function (p) {
            if(p === null) {
                throw new Error("Failed to retrieve player!");
            }

            rechargeCount = getRechargeCount(p);
            if((p.energy < p.energyMax || p.stamina < p.staminaMax)
                && rechargeCount > 0) {
                return Player.gameObject.lock(id);
            }

            return p;
        })
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            rechargeCount = getRechargeCount(player);
            if((player.energy >= player.energyMax && player.stamina >= player.staminaMax)
                || rechargeCount === 0) {
                return true;
            }

            player.energy += Math.min(rechargeCount, player.energyMax);
            player.stamina += Math.min(rechargeCount, player.staminaMax);
            player.lastRecharge = Date.now();

            return Player.gameObject.unlock(player);
        })
        .then(function () {
            return player;
        })
        .catch(function (err) {
            //console.log(err)
            Player.gameObject.revert(player);
            throw new Error(err.message);
        });
};

exports.newPlayer = function(client, name) {
    var player = new Player({
        clientId: client._id.toString(),
        name: name
    });

    return Player.gameObject.set(player).then(function(player) {
        client.playerId = player._id;
        return client.save();
    })
    .then(function (client) {
        return player;
    })
    .catch(function(err) {
        throw new Error(err);
    });
};

exports.getLevelData = function (player) {
    var currentLevel = exports.getLevel(player);
    var totalXPforNextLevel = exports.getExperienceForLevel(currentLevel + 1);
    var totalXPforCurrentLevel = exports.getExperienceForLevel(currentLevel);

    return {
        level: currentLevel,
        experience: player.experience - totalXPforCurrentLevel,
        experienceToNextLevel: totalXPforNextLevel - totalXPforCurrentLevel
    }
};

exports.getLevel = function (player) {
    return exports.getLevelForExperience(player.experience);
};

exports.getExperienceForLevel = function (level) {
    if(level === 1) return 0;
    return Math.ceil(6.25*Math.pow(level,2));
};

exports.getLevelForExperience = function (experience) {
    if(experience === 0) {
        return 1;
    }

    var level = Math.floor(Math.sqrt(experience/6.25));
    return Math.max(1, level);
};

// response data that is used for client display
exports.clientPlayer = function (player, optional = []) {
    var client = {};

    var defaults = [
        'health',
        'healthMax',
        'energy',
        'energyMax',
        'stamina',
        'staminaMax',
        'coins',
        'gems',
        'lastRecharge',
        '_id'
    ];

    defaults.forEach(function(prop) {
        client[prop] = player[prop];
    });

    Object.assign(client, exports.getLevelData(player));

    if(optional.length > 0) {
        optional.forEach(function (option) {
            client[option] = player[option];
        })
    }

    return client;
};

function getRechargeCount(player) {
    var rechargeDate = new Date(player.lastRecharge);
    var timePassed = Date.now() - rechargeDate.getTime();
    // console.log("date", Date.now())
    // console.log("rechargeDate", rechargeDate.getTime());
    // console.log("timePassed", timePassed);
    // console.log("count", Math.floor(timePassed / RECHARGE_TIME))
    return Math.floor(timePassed / RECHARGE_TIME);
}

/*** ENDPOINTS ***/
exports.showProfile = function(req, res) {
    var player = req.player;

    if(req.body.player_id !== undefined) {
        player = exports.get(req.body.player_id);
    }

    Promise.all([player])
        .spread(function (player) {
            return Promise.all([
                player,
                ArenaRank.getArenaRank(player._id),
                Allies.get(player),
                Squad.get(player.squadId),
                Team.getActiveTeam(player)
            ]);
        })
        .spread(function (player, arenaRank, allies, squad, team) {
            return res.send({
                player: exports.clientPlayer(player),
                arena: arenaRank,
                allies: allies,
                squad: squad,
                team: team
            });
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: "Failed to get player profile!"});
        });
};

