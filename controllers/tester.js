"use strict";

// Load required packages
var Essence = require('../controllers/essence');
var EquipmentController = require('../controllers/equipment');
var Hero = require('../models/hero');
var HeroController = require('../controllers/hero');
var Quest = require('../models/quest');
var ArenaList = require('../controllers/arenaList');
var Gateway = require('../controllers/gateway');
var Game = require('../controllers/game');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var SquadController = require('../controllers/squad');
var Squad = require('../models/squad');
var SquadDonation = require('../models/squadDonation');
var SquadDonationController = require('../controllers/squadDonation');
var Item = require('../controllers/item');
var redis = require('../controllers/redis');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var _ = require('lodash');

exports.test1 = function(req, res) {
    var player = req.player;
    var data = {};

    var object = {
        "0": {"1": "Jeff", "2": "Apple"},
        "1": {"1": "push", "2": "pull"},
        "2": {"1": "fire", "2": "water"}
    };

    return redis.hmset(resourceKey(player._id), object)
        .then(function (success) {
            if(!success) {
                return res.send({error: "Failed to set!"});
            }
            data.success = success;

            return data;
        })
        .then(function () {
            //return false;
            return redis.hmset(resourceKey(player._id), {"2": {"earth": "wind"}});
        })
        .then(function () {
            //return redis.hmget(resourceKey(player._id), {name: "powers", value: "1"});
            return redis.hmget(resourceKey(player._id), '2');
        })
        .then(function (value) {
            data.result = value;
            return res.send(data);
        });
};

exports.test2 = function(req, res) {
    var player = req.player;

    // Player.gameObject.lock(player._id)
    //     .then(function (player) {
    //         player.energy += 100;
    //         return Player.gameObject.unlock(player)
    //             .then(function () {
    //                 return res.send(player);
    //             })
    //     })

    Item.addItems(player, [{"id": 0, "amount": 2}, {"id": 1, "amount": 1}])
        .then(function () {
            return Item.getAll(player);
        })
        .then(function (items) {
            return res.send(items);
        })
};

exports.test3 = function (req, res) {
    var player = req.player;

    var experienceArray = [500, 20000, 100000];

    _.each(experienceArray, function (xp) {
        var level = PlayerController.getLevelForExperience(xp);

        console.log('Level:');
        console.log(level);

        console.log('Experience:');
        console.log(PlayerController.getExperienceForLevel(level));

        console.log('');
    });

    return res.send(player);
};

exports.test4 = function(req, res) {
    var player = req.player;

    var essenceToAdd = 1000;
    var heroId = 6;

    Essence.addEssence(player, heroId, essenceToAdd)
        .then(function () {
            return Essence.getEssence(player, heroId);
        })
        .then(function (essence) {
            return res.send(essence);
        });
};

exports.test5 = function(req, res) {
    var player = req.player;

    return redis.zrevrange(ArenaList.LIST_KEY, '-INF', '+INF')
        .then(function (wholeList) {
            console.log("wholeList=");
            console.log(wholeList);
            return res.send(wholeList);
        });
};

exports.test6 = function(req, res) {
    var player = req.player;
    var rank = 0;
    var score = 0;
    var count = 0;

    return redis.zrevrange(ArenaList.LIST_KEY, '-INF', '+INF')
        .then(function (wholeList) {
            console.log("wholeList=");
            console.log(wholeList);
            return ArenaList.getRankFor(player._id);
        })
        .then(function (playerRank) {
            console.log("playerRank = " + playerRank);
            console.log(playerRank);
            rank = playerRank;
            return ArenaList.getScoreFor(player._id);
        })
        .then(function (playerScore) {
            console.log("playerScore = " + playerScore);
            console.log(playerScore);
            score = playerScore;
            return ArenaList.getCountFor();
        })
        .then(function (listCount) {
            console.log("listCount = " + listCount);
            console.log(listCount);
            count = listCount;
            return ArenaList.getArenaRanges(rank, score, count);
        })
        .then(function (ranges) {
            console.log("ranges for rank=" + rank + " score=" + score + " and count=" + count + " results=");
            console.log(ranges);
            return res.send({ranges : ranges});
        });

};

exports.test7 = function(req, res) {
    var player = req.player;

    var member_id = 'nate';
    return redis.zadd('test_sorted_set', 133, member_id.toString())
        .then( function (results) {
            console.log("results of set of test_sorted_set returned with 133 and id=nate");
            console.log(results);
            return redis.zrevrange('test_sorted_set', 0, -1);
        })
        .then( function (results) {
            console.log("results of revrange of test_sorted_set returned with 0 to -1");
            console.log(results);
            return redis.zrevrangewithscores('test_sorted_set', 0, -1, true);
        })
        .then( function (results) {
            console.log("results of revrangewithscores of test_sorted_set returned with 0 to -1");
            console.log(results);
            return redis.zcount('test_sorted_set');
        })
        .then( function (results) {
            console.log("results of zcount of test_sorted_set");
            console.log(results);
            return redis.zcountbetween('test_sorted_set', 0, 50);
        })
        .then( function (results) {
            console.log("results of zcountbetween of test_sorted_set returned with 0 to 55");
            console.log(results);
            return redis.zrank('test_sorted_set', member_id.toString());
        })
        .then(function (results) {
            console.log("results of zrank of test_sorted_set for member_id nate");
            console.log(results);
            return redis.zscore('test_sorted_set', member_id.toString());

        })
        .then(function (results) {
            console.log("results of zscore of test_sorted-set for member_id nate");
            console.log(results);
            return res.send({data: results});
        });

    // return Hero.gameObject.hmLock(player._id, 3)
    //     .then(function (hero) {
    //         hero.experience += 100;
    //
    //         return Hero.gameObject.hmUnlock(hero)
    //             .then(function () {
    //                 return res.send({data: hero});
    //             });
    //     })
    //     .catch(function (err) {
    //         console.log(err);
    //     });
};

exports.test8 = function(req, res) {
    var player = req.player;

    return redis.zrevrange(ArenaList.LIST_KEY, '-100000000000', '100000000000')
        .then( function (wholeList) {
            console.log("wholeList=");
            console.log(wholeList);
            return wholeList;
        }).then( function (wholeList) {
            var input = [];
            var i = 0;
            for (i=1; i <=100; i++)
            {
                var memberId = 'm'+ i;
                var memadd = redis.zadd(ArenaList.LIST_KEY, i, memberId);
                input.push(memadd);
            }
            return Promise.all(input)
            // return Promise.all([redis.zadd(ArenaList.LIST_KEY, 1, 'm1'),
            //     redis.zadd(ArenaList.LIST_KEY, 2, 'm2'),
            //     redis.zadd(ArenaList.LIST_KEY, 3, 'm3'),
            //     redis.zadd(ArenaList.LIST_KEY, 4, 'm4'),
            //     redis.zadd(ArenaList.LIST_KEY, 5, 'm5')])
        }).then(function(results) {
            console.log("promise results:");
            console.log(results);

            return redis.zrevrange(ArenaList.LIST_KEY, '-100000000000', '100000000000')
                .then(function (results) {
                    console.log("get of wholeList returned:");
                    console.log(results);
                    return res.send({data: results});
                })
        }).catch(function (err) {
                    console.log(err);
                });

    // return Hero.gameObject.hmLock(player._id, 3)
    //     .then(function (hero) {
    //         hero.experience += 100;
    //
    //         return Hero.gameObject.hmUnlock(hero)
    //             .then(function () {
    //                 return res.send({data: hero});
    //             });
    //     })
    //     .catch(function (err) {
    //         console.log(err);
    //     });
};

exports.test9 = function (req, res) {
    var player = req.player;

    return ArenaList.getArenaList(req, res);
};

exports.test10 = function (req, res) {
    var player = req.player;

    var iterations = 0;
    var resultsTracker = {"0" : 0, "1" : 0, "2" : 0, "3" : 0, "4" : 0, "5" : 0, "6" : 0, "recharge" : 0};

    return HeroController.get(player)
        .then( function (allHeroData) {
            var testerPromises = [];
            while (iterations < 1000)
            {
                testerPromises.push("0");
                iterations++;
            }

            return Promise.each(testerPromises, function(value) {
                return Gateway.summon(value, player)
                    .then( function(results) {
                        resultsTracker[results.hero_id] += 1;
                    })
            })
                .then( function (testerResults) {
                    console.log(testerResults);
                    // _.each(testerResults, function(results) {
                    //     resultsTracker[results.hero_id] += 1;
                    // })
                    console.log("resultsTracker: ");
                    console.log(resultsTracker);
                    return res.send({tracker : resultsTracker});
                })


        })

};

exports.test11 = function (req, res) {
    var player = req.player;

    if (req.body.hero_id === undefined)
    {
        throw new Error("hero_id parameter not sent with request");
    }
    var heroId = req.body.hero_id;



    return EquipmentController.get(player, heroId)
        .then(function(getResults) {
            console.log("O.O O.O O.O O.O getResults");
            console.log(getResults);
            return EquipmentController.getBonusesFromData(getResults);
        })
        .then(function(bonuses) {
            console.log("O.O O.O O.O O.O bonuses");
            console.log(bonuses);
            return res.send({results : bonuses});
        })
    // return HeroController.getHero(player, "0")
    //     .then(function (hero) {
    //         var currentStars = HeroController.getStars(hero);
    //         var essenceRequired = HeroController.getEssenceForStars(5);
    //         hero.essence += essenceRequired; // star level up!
    //
    //
    //         return Hero.gameObject.hSet(player._id, "0", hero);
    //     })
    //     .then( function(results) {
    //         res.send({data: results});
    //     })
        .catch(function (err) {
            console.log(err);
        });
};

exports.test12 = function (req, res) {
    var player = req.player;
    var squad_id = req.body.squad_id;
    var donations = {};
    return SquadDonationController.get(squad_id)
        .then(function(allDonations) {
            donations = allDonations
            console.log("O.O O.O O.O allDonations ");
            console.log(allDonations);
            return SquadDonationController.getRequestFor(squad_id, player._id.toString())
        })
        .then(function(myrequest) {
            console.log("O.O O.O O.O O.O my request=");
            console.log(myrequest);
            return res.send({donations : donations, request : myrequest })
        })
        .catch(function(err) {
            console.log(err)
            Squad.gameObject.revert(squad);
            Player.gameObject.revert(joiner);
            return res.send({ error: 'Failed to join!' });
        });
};

exports.test13 = function (req, res) {
    return SquadDonationController.createRequest(req, res)
        .catch(function(err) {
            console.log(err)
            return res.send({error: err.message});
        });
};

exports.test14 = function (req, res) {
    var results = {};
    return SquadDonationController.donateToRequest(req, res)
        .then(function(request) {
            results = request;
            return Promise.all([Item.getAll(req.player), Player.gameObject.get(req.body.req_id)])
        })
        .spread(function(playerItems, requestor) {
            console.log("O.O O.O O.O player's items:");
            console.log(playerItems);
            return Item.getAll(requestor)
        })
        .then(function(requestorItems) {
            console.log("O.O O.O O.O player's items:");
            console.log(requestorItems);
            return results;
        })
        .catch(function(err) {
            console.log(err)
            return res.send({error: err.message});
        });
};

var resourceKey = function(id) {
    return 'Tests:'+id.toString();
};

