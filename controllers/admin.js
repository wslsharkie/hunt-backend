// Load required packages
var Admin = require('../models/admin');

// Create endpoint /api/users for POST
exports.postAdmins = function(req, res) {
    var admin = new Admin({
        username: req.body.username,
        password: req.body.password
    });

    admin.save(function(err) {
        if (err)
            return res.send(err);

        console.log('New admin added!');
    });
};

// Create endpoint /api/users for GET
exports.getAdmins = function(req, res) {
    Admin.find(function(err, admins) {
        if (err)
            return res.send(err);

        res.json(admins);
    });
};