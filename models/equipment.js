// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var EquipmentSchema = new mongoose.Schema({
    ownerId:        { type: String, required: true },
    heroId:         { type: Number, required: true },
    equipment:      { type: Object, required: true },
    createdAt:      { type: Number, required: true }
});

EquipmentSchema.statics.gameObject = null;

EquipmentSchema.statics.resourceKey = function(objectId) {
    return 'equipment:'+objectId.toString();
};

// overrides the static resourceKey
EquipmentSchema.methods.resourceKey = function(instance) {
    return 'equipment:'+instance.ownerId;
};

EquipmentSchema.statics.fieldKey = function () {
    return 'heroId';
};

// Export the Mongoose model
var Equipment = mongoose.model('Equipment', EquipmentSchema);
Equipment.gameObject = new GameObject({model: Equipment});
module.exports = Equipment;
