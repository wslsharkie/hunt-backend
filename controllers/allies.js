"use strict";

var Allies = require('../models/allies');
var Player = require('../controllers/player');
var Game = require('../controllers/game');
var Squad = require('../controllers/squad');
var keyHash = require('key-hash');
var redis = require('../controllers/redis');
var Promise = require('bluebird');
var _ = require('lodash');

exports.TYPE = {};
exports.TYPE.REQUEST = 0;
exports.TYPE.INVITE = 1;
exports.TYPE.ALLY = 2;

exports.get = function (player) {
    return Allies.gameObject.get(player._id)
        .then(function (allies) {
            if(allies === null) {
                var instance = {
                    ownerId:    player._id.toString(),
                    hash:       keyHash(player._id.toString()),
                    allies:     [],
                    createdAt:  Date.now()
                };

                allies = new Allies(instance);

                return Allies.gameObject.set(allies)
                    .then(function (newAllies) {
                        return newAllies;
                    });
            }

            return allies;
        });
};

function createAllyObject(player, type) {
    return Squad.get(player.squadId)
        .then(function (squad) {
            var ally = {
                id: player._id,
                name: player.name,
                level: Player.getLevel(player),
                type: type,
                squadName: "",
                squadId: "",
                createdAt: Date.now()
            };

            if(squad) {
                ally.squadId = squad._id;
                ally.squadName = squad.name;
            }

            return ally;
        });
}

function isFriend(id, list) {
    var friends = _.filter(list, ['id', id]);
    if(friends.length > 0) {
        return true;
    }

    return false;
}

function addAlly(player, allies) {
    return createAllyObject(player, exports.TYPE.ALLY)
        .then(function (ally) {
            allies.allies.push(ally);
            return allies;
        });
}

function getResponseData(allies) {
    return {
        allies: allies.allies,
        invites: _.filter(allies.pending, ['type', exports.TYPE.INVITE]),
        requests: _.filter(allies.pending, ['type', exports.TYPE.REQUEST]),
        hash: allies.hash
    };
}

/*** ENDPOINTS ***/
exports.showAllies = function (req, res) {
    var player = req.player;

    return exports.get(player)
        .then(function (allies) {

            return res.send(getResponseData(allies));
        });
};

exports.sendInvite = function (req, res) {
    var player = req.player;

    if(req.params.player_id === undefined) {
        return res.send({ error: 'Missing param player_id!' });
    }

    var targetId = req.params.player_id;

    if(targetId === player._id.toString()) {
        return res.send({ error: 'Target id and player id are the same!' });
    }

    var playerAllies = null;
    var targetAllies = null;

    return Player.get(targetId)
        .then(function (target) {
            return Promise.all([
                target,
                Allies.gameObject.lock(player._id),
                Allies.gameObject.lock(target._id)
            ]);
        }).spread(function(target, lockedPlayerAllies, lockedTargetAllies) {
            playerAllies = lockedPlayerAllies;
            targetAllies = lockedTargetAllies;

            var playerId = player._id.toString();

            if(isFriend(playerId, targetAllies.allies)) {
                throw new Error('Already your friend!');
            }

            var playerPending = _.filter(playerAllies.pending, ['id', targetId]);
            var targetPending = _.filter(targetAllies.pending, ['id', playerId]);

            if(playerPending.length > 0 || targetPending.length > 0) {
                throw new Error('Already in pending!');
            }

            return Promise.all([
                createAllyObject(target, exports.TYPE.INVITE),
                createAllyObject(player, exports.TYPE.REQUEST),
                playerPending,
                targetPending
            ]);
        })
        .spread(function (invite, request, playerPending, targetPending) {
            if(playerPending.length === 0) {
                playerAllies.pending.push(invite);
            }

            if(targetPending.length === 0) {
                targetAllies.pending.push(request);
            }

            return Promise.all([
                Allies.gameObject.unlock(playerAllies),
                Allies.gameObject.unlock(targetAllies)
            ]);
        })
        .then(function () {
            return res.send(getResponseData(playerAllies));
        })
        .catch(function (err) {
            //console.log(err)
            Allies.gameObject.revert(playerAllies);
            Allies.gameObject.revert(targetAllies);
            return res.send({ error: 'Failed to send invite!' });
        });
};

exports.acceptInvite = function (req, res) {
    var player = req.player;

    if(req.params.player_id === undefined) {
        return res.send({ error: 'Missing param player_id!' });
    }

    var targetId = req.params.player_id;

    if(targetId === player._id.toString()) {
        return res.send({ error: 'Target id and player id are the same!' });
    }

    var playerAllies = null;
    var targetAllies = null;

    return Player.get(targetId)
        .then(function (target) {
            return Promise.all([
                target,
                Allies.gameObject.lock(player._id),
                Allies.gameObject.lock(target._id)
            ]);
        }).spread(function(target, playerAllies, targetAllies) {
            playerAllies = lockedPlayerAllies;
            targetAllies = lockedTargetAllies;

            var playerId = player._id.toString();

            if(isFriend(playerId, targetAllies.allies)) {
                throw new Error('Already your friend!');
            }

            var targetPending = _.filter(targetAllies.pending, ['id', playerId]);
            var playerPending = _.filter(playerAllies.pending, ['id', targetId]);

            if(targetPending.length === 0 && playerPending.length === 0) {
                throw new Error('Not in pending lists!');
            }

            return Promise.all([addAlly(target, playerAllies), addAlly(player, targetAllies)]);
        })
        .then(function () {
            targetAllies.pending = _.reject(targetAllies.pending, ['id', playerId]);
            playerAllies.pending = _.reject(playerAllies.pending, ['id', targetId]);

            return Promise.all([Allies.gameObject.unlock(playerAllies), Allies.gameObject.lock(targetAllies)]);
        })
        .then(function () {
            return res.send(getResponseData(playerAllies));
        })
        .catch(function (err) {
            Allies.gameObject.revert(playerAllies);
            Allies.gameObject.revert(targetAllies);
            return res.send({ error: 'Failed to accept invite!' });
        });
};

exports.removeAlly = function (req, res) {
    var player = req.player;

    if(req.params.player_id === undefined) {
        return res.send({ error: 'Missing param player_id!' });
    }

    var targetId = req.params.player_id;

    if(targetId === player._id.toString()) {
        return res.send({ error: 'Target id and player id are the same!' });
    }

    var playerAllies = null;
    var targetAllies = null;

    return Player.get(targetId)
        .then(function (target) {
            return Promise.all([
                target,
                Allies.gameObject.lock(player._id),
                Allies.gameObject.lock(target._id)
            ]);
        }).spread(function(target, playerAllies, targetAllies) {
            playerAllies = lockedPlayerAllies;
            targetAllies = lockedTargetAllies;

            var playerId = player._id.toString();

            if(isFriend(playerId, targetAllies.allies) === false) {
                throw new Error('Not your friend!');
            }

            playerAllies.allies = _.reject(playerAllies.allies, ['id', targetId]);
            targetAllies.allies = _.reject(targetAllies.allies, ['id', playerId]);

            return Promise.all([Allies.gameObject.unlock(playerAllies), Allies.gameObject.lock(targetAllies)]);
        })
        .then(function () {
            return res.send(getResponseData(playerAllies));
        })
        .catch(function (err) {
            Allies.gameObject.revert(playerAllies);
            Allies.gameObject.revert(targetAllies);
            return res.send({ error: 'Failed to remove friend!' });
        });
};

exports.removePending = function (req, res) {
    var player = req.player;

    if(req.params.player_id === undefined) {
        return res.send({ error: 'Missing param player_id!' });
    }

    var targetId = req.params.player_id;

    if(targetId === player._id.toString()) {
        return res.send({ error: 'Target id and player id are the same!' });
    }

    var playerAllies = null;
    var targetAllies = null;

    return Player.get(targetId)
        .then(function (target) {
            return Promise.all([
                target,
                Allies.gameObject.lock(player._id),
                Allies.gameObject.lock(target._id)
            ]);
        }).spread(function(target, playerAllies, targetAllies) {
            playerAllies = lockedPlayerAllies;
            targetAllies = lockedTargetAllies;

            var playerId = player._id.toString();

            var targetCount = targetAllies.pending.length;
            var playerCount = playerAllies.pending.length;

            playerAllies.pending = _.reject(playerAllies.pending, ['id', targetId]);
            targetAllies.pending = _.reject(targetAllies.pending, ['id', playerId]);

            if(playerCount === playerAllies.pending.length && targetCount === targetAllies.pending.length) {
                throw new Error('Invite or request not found!');
            }

            return Promise.all([Allies.gameObject.unlock(playerAllies), Allies.gameObject.lock(targetAllies)]);
        })
        .then(function () {
            return res.send(getResponseData(playerAllies));
        })
        .catch(function (err) {
            Allies.gameObject.revert(playerAllies);
            Allies.gameObject.revert(targetAllies);
            return res.send({ error: 'Failed to remove friend!' });
        });
};

exports.searchForAlly = function (req, res) {
    var player = req.player;

    if(req.params.hash === undefined) {
        return res.send({ error: 'Missing param player_id!' });
    }

    var hash = req.params.hash.toString();

    Allies.find({hash: hash}).lean().exec()
        .then(function(resultArr) {
            if(resultArr.length === 0) {
                throw new Error('Not found!');
            }

            var allies = resultArr[0];

            return Player.get(allies.ownerId);
        })
        .then(function (player) {
            return res.send({ ally: player });
        })
        .catch(function (err) {
            return res.send({ error: 'No player with hash'+hash+'!' });
        });
};

// testing
var Client = require('../controllers/client');
exports.test = function (req, res) {
    if(Game.DEBUG === false) {
        return res.send({error: "unauthorized"});
    }

    var player = req.player;
    var target;

    Client.create()
        .then(function (client) {
            return Player.newPlayer(client, "Allies Dummy");
        })
        .then(function (ally) {
            target = ally;
            return Promise.all([exports.get(player), exports.get(target)]);
        })
        .then(function () {
            return Promise.all([Allies.gameObject.lock(player._id), Allies.gameObject.lock(target._id)]);
        })
        .spread(function (playerAllies, targetAllies) {
            return Promise.all([addAlly(target, playerAllies), addAlly(player, targetAllies)]);
        })
        .spread(function (playerAllies, targetAllies) {
            return Promise.all([Allies.gameObject.unlock(playerAllies), Allies.gameObject.unlock(targetAllies)]);
        })
        .then(function () {
            return res.send({result: 'ok'});
        });
};
