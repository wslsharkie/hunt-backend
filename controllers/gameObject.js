"use strict";

// Redis and Mongodb handler
var fs = require('fs');
var jsonfile = require('jsonfile');
var Promise = require('bluebird');
var Game = require('../controllers/game');
var mongoose = require('mongoose');
var redis = require('../controllers/redis');
var _ = require('lodash');

function GameObject(params) {
    this.model = params.model;
}

GameObject.prototype.set = function(instance) {
    var key = this.resourceKey(instance);

    return instance.save()
        .then(function(object) {
            return Promise.all([object, redis.set(key, object)]);
        })
        .spread(function(object, set) {
            if(set) {
                return object;
            }
            throw new Error('Unable to set redis object after write to mongodb!');
        }).catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.get = function(id) {
    var gameObject = this;
    var model = this.model;
    var key = model.resourceKey(id);

    return redis.get(key)
        .then(function(object) {
            if(object) {
                return new model(object);
            }

            var query = gameObject.getQuery(id);

            return model.find(query).lean().exec()
                .then(function(resultArr) {
                    if(resultArr.length === 0) {
                        return null;
                    }

                    var object = resultArr[0];

                    return redis.set(key, object)
                        .then(function () {
                            return new model(object);
                        });
                });
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.lock = function(objectId) {
    var gameObject = this;
    var model = this.model;

    return redis.lock(model.resourceKey(objectId))
        .then(function(lock) {
            return Promise.all([lock, gameObject.get(objectId)]);
        })
        .spread(function(lock, data) {
            if(data === null) {
                throw new Error('No data to lock!');
            }

            var instance = new model(data);
            instance.lock = lock;
            return instance;
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.unlock = function(instance) {
    if(instance.lock === undefined || instance.lock === null) {
        throw new Error('Lock does not exist!');
    }

    return this.update(instance)
        .then(function() {
            return redis.unlock(instance.lock);
        })
        .then(function() {
            return delete instance.lock;
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.update = function(instance) {
    var gameObject = this;
    var key = this.resourceKey(instance);
    var model = this.model;
    instance = new model(instance);

    return redis.set(key, instance)
        .then(function (set) {
            if(set) {
                return gameObject.updateQueue(key);
            }
            throw new Error('Unable to set redis object on update!');
        });
};

/*
HMGET/HMSET Redis

notes:
Use hgetall if more than one hash map field needs to be retrieved.

Running 100 hmgets is slower than running hgetall.
Running 4 hmgets is about the same as running hgetall.
 */
GameObject.prototype.hmSet = function (id, object) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(id);

    return redis.hmset(key, object)
        .then(function() {
            // mark queue for cron job
            return gameObject.updateQueue(key);
        });
};

GameObject.prototype.hSet = function (id, field, object) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(id);

    var data = {};
    data[field] = object;
    return redis.hmset(key, data)
        .then(function() {
            // mark queue for cron job
            return gameObject.updateQueue(key);
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.hmGet = function(id, field) {
    var model = this.model;
    // always player id
    var key = model.resourceKey(id);
    var fieldKey = model.fieldKey();

    return redis.hmget(key, field)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }

            var query = {ownerId: id};
            query[fieldKey] = field;
            // must use find to query without _id!
            return model.find(query).lean().exec()
                .then(function(resultArr) {
                    if(resultArr.length === 0) {
                        return null;
                    }

                    var object = resultArr[0];
                    var data = {};
                    data[field] = object;

                    return redis.hmset(key, data)
                        .then(function () {
                            return object;
                        });
                });
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.hGetAll = function (id) {
    var model = this.model;
    // always player id
    var key = model.resourceKey(id);
    var fieldKey = model.fieldKey();

    return redis.hgetall(key)
        .then(function(objects) {
            if(objects) {
                //console.log('redis');
                return objects;
            }

            return model.find({ ownerId: id }).lean().exec()
                .then(function(objects) {
                    var hashMap = _.reduce(objects, function(result, object) {
                        var fieldKeyValue = object[fieldKey];
                        result[fieldKeyValue] = object;
                        return result;
                    }, {});

                    return Promise.all([hashMap, redis.hmset(key, hashMap)]);
                })
                .spread(function(objects, set) {
                    if (set) {
                        //console.log('mongodb');
                        return objects;
                    }
                    throw new Error('Unable to set redis objects after write to mongodb!');
                });
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.hmLock = function(objectId, field) {
    var gameObject = this;
    var model = this.model;

    return redis.lock(model.resourceKey(objectId))
        .then(function(lock) {
            return Promise.all([lock, gameObject.hmGet(objectId, field)]);
        })
        .spread(function(lock, data) {
            if(data === null) {
                throw new Error('No data to lock!');
            }

            var instance = new model(data);
            instance.lock = lock;
            return instance;
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.hmUnlock = function(instance) {
    if(instance.lock === undefined || instance.lock === null) {
        throw new Error('Lock does not exist!');
    }

    var model = this.model;
    var fieldKey = model.fieldKey();

    var data = {};
    var fieldKeyValue = instance[fieldKey];
    data[fieldKeyValue] = instance;
    return this.hmSet(instance.ownerId, data)
        .then(function() {
            return redis.unlock(instance.lock);
        })
        .then(function() {
            return delete instance.lock;
        })
        .catch(function(err) {
            throw new Error(err);
        });
};

GameObject.prototype.mGet = function(keys) {
    if(Array.isArray(keys) === false) {
        throw new Error('mGet keys is not an array!');
    }
    return redis.mget(keys);
};

GameObject.prototype.revert = function(instance) {
    if(_.isNil(instance) || _.isNil(instance.lock)) {
        //throw new Error('Lock does not exist!');
        return false;
    }

    return redis.unlock(instance.lock)
        .then(function() {
            return delete instance.lock;
        });
};

/* sorted set commands zadd, zscore, zrange, zrank

 */

GameObject.prototype.zadd = function(set_id, score, member_id) {
        var gameObject = this;
        var model = this.model;
        // always player id
        var key = model.resourceKey(set_id);

        return redis.zadd(key, Number(score), member_id.toString())
            .then(function() {
                // mark queue for cron job
                return gameObject.updateQueue(key);
            });
};

GameObject.prototype.zrevrange = function(set_id, start, end) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zrevrange(key, start, end)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zrevrangewithscores = function(set_id, start, end) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zrevrangewithscores(key, start, end)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zrange = function(set_id, start, end) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zrange(key, sgetart, end)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zcount = function(set_id, start, end) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zcount(key)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zcountbetween = function(set_id, start, end)
{
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zcountbetween(set_id, start, end)
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
}

GameObject.prototype.zincrby = function(set_id, points, member_id) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zincrby(key, Number(points), member_id.toString())
        .then(function() {
            // mark queue for cron job
            return gameObject.updateQueue(key);
        });
};

GameObject.prototype.zrank = function(set_id, member_id) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zrank(key, member_id.toString())
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zrevrank = function(set_id, member_id) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zrevrank(key, member_id.toString())
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};

GameObject.prototype.zscore = function(set_id, member_id) {
    var gameObject = this;
    var model = this.model;
    // always player id
    var key = model.resourceKey(set_id);

    return redis.zscore(key, member_id.toString())
        .then(function(object) {
            if(object) {
                //console.log('redis');
                return object;
            }
        })
        .catch(function(err) {
            //console.log(err)
            throw new Error(err);
        });
};








GameObject.prototype.updateQueue = function(key) {
    var queueKey = GameObject.queueKey(this.model);
    return redis.sadd(queueKey, key.toString());
};

GameObject.prototype.removeFromQueue = function (key) {
    var queueKey = GameObject.queueKey(this.model);
    return redis.srem(queueKey, key.toString());
};

GameObject.prototype.delete = function(field) {
    var key = this.model.resourceKey(field.value);
    redis.delete(key);
    this.removeFromQueue(key);

    var query = {};
    query[field.name] = field.value;
    return this.model.find(query).remove(query).lean().exec();
};

GameObject.prototype.validateProp = function(prop, value) {
    if(typeof prop !== 'string') {
        return false;
    }

    var spec = this.model.schema.paths;

    if(!spec.hasOwnProperty(prop)) {
        return false;
    }

    if (typeof value !== spec[prop].instance.toLowerCase()) {
        return false;
    }

    return true;
};

GameObject.prototype.resourceKey = function (instance) {
    if (typeof instance.resourceKey === 'function') {
        // check the mongoose instance
        return instance.resourceKey(instance);
    } else {
        // use the object id
        return this.model.resourceKey(instance._id);
    }
};

GameObject.prototype.getQuery = function (id) {
    if(typeof this.model.getQuery === 'function') {
        // check the model
        return this.model.getQuery(id);
    } else {
        // use the object id
        return { _id: mongoose.Types.ObjectId(id) };
    }
};

GameObject.queueKey = function(model) {
    return 'Queues-'+model.modelName;
};

module.exports = GameObject;
