"use strict";

var express = require('express');
var questRouter = express.Router();
var authController = require('../controllers/auth');
var questController = require('../controllers/quest');

questRouter.route('/:quest_id/?')
    .post(authController.isBearerAuthenticated, questController.doQuest);

module.exports = questRouter;
