// A list to track a players BattleList

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var BattleListSchema = new mongoose.Schema({
    ownerId:    { type: String, required: true, unique: true },
    list:       { type: Array },
    date:       { type: Date, default: Date.now }
});

BattleListSchema.pre('findOne', function(callback){
    var query = this;

    if(mongoose.Types.ObjectId.isValid(query._conditions._id)) {
        return callback();
    }

    throw new Error('_id query is not an object id!');
});

BattleListSchema.statics.gameObject = null;

BattleListSchema.statics.getQuery = function(id) {
    return {ownerId: id};
};

BattleListSchema.statics.resourceKey = function(objectId) {
    return 'BattleList:list:'+objectId.toString();
};

BattleListSchema.methods.resourceKey = function(instance) {
    return 'BattleList:list:'+instance.ownerId;
};

// Export the Mongoose model
var BattleList = mongoose.model('BattleList', BattleListSchema);
BattleList.gameObject = new GameObject({model: BattleList});
module.exports = BattleList;
