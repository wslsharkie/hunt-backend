"use strict";

var Player = require('../models/player');
var Hero = require('../controllers/hero');
var PlayerController = require('../controllers/player');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var _ = require('lodash');

// generate a list of players
// get random players of the same level range
// client will store this list in cache to randomize

exports.RANGE_BASE = 1.25;
exports.LIST_KEY = 'player:list:';
exports.LIST_MAX = 50;
exports.EXPIRE_TIME = 600; // seconds

function getLevelRange(player) {
    if(player.experience === 0) {
        return {min: 0, max: 1};
    }

    var value = Math.ceil(player.experience * Math.pow(exports.RANGE_BASE, 2) - player.experience);
    var rangeMin = Math.max(0, player.experience-value);
    var rangeMax = player.experience+value;
    return {min: rangeMin, max: rangeMax};
}

exports.resourceKey = function(player) {
    return exports.LIST_KEY+player._id;
};

exports.get = function (player) {
    return redis.get(exports.resourceKey(player))
        .then(function (list) {
            if(Game.DEBUG === false && list) {
                return list;
            }

            var range = getLevelRange(player);

            //console.log(range)

            var query = [
                // Grouping pipeline
                { "$match": {
                    "experience": {"$gte":range.min, "$lte": range.max},
                    "_id": {"$ne": mongoose.Types.ObjectId(player._id)}
                } },
                // specify fields
                { "$project" : {
                    "name" : 1 ,
                    "experience" : 1,
                    "coin" : 1,
                    "squad" : 1
                } },
                // random sample pipeline
                { "$sample": { "size" : exports.LIST_MAX } }
            ];

            return Player.aggregate(query).exec()
                .then(function (players) {
                    players = _.reduce(players, function (result, p) {
                        // remove player from list
                        if(p._id.toString() === player._id.toString()) {
                            return result;
                        }

                        p.level = PlayerController.getLevelForExperience(p.experience);
                        delete p.experience;
                        result.push(p);
                        return result;
                    }, []);

                    if(players.length > 0) {
                        return redis.set(exports.resourceKey(player), players, exports.EXPIRE_TIME)
                            .then(function () {
                                return players;
                            });
                    }

                    return players;
                });
        })
        .catch(function (err) {
            //console.log(err)
            throw new Error("Failed to get player list!");
        });
};

/*** ENDPOINTS ***/
exports.showList = function(req, res) {
    var player = req.player;

    exports.get(player)
        .then(function (list) {
            return res.send({list: list});
        })
};

// testing
var Client = require('../controllers/client');
exports.createDummyPlayers = function (req, res) {
    if(Game.DEBUG === false) {
        return res.send({error: "unauthorized"});
    }

    var playerPromises = [];

    for(var i=0; i<10; i++) {
        var p = Client.create()
            .then(function (client) {
                return PlayerController.newPlayer(client, "Battle Dummy");
            });

        playerPromises.push(p);
    }

    return Promise.all(playerPromises)
        .then(function (players) {
            var heroPromises = [];

            console.log(players)

            _.each(players, function (p) {
                heroPromises.push(Hero.addHeroes(p, Hero.starterHeroes));
            });

            return Promise.all(heroPromises);
        })
        .then(function () {
            return res.send(i + " dummy players created!");
        });
};
