var express = require('express');
var playerListRouter = express.Router();
var authController = require('../controllers/auth');
var playerListController = require('../controllers/playerList');

playerListRouter.route('/')
    .get(authController.isBearerAuthenticated, playerListController.showList);

playerListRouter.route('/test/generate')
    .post(authController.isBearerAuthenticated, playerListController.createDummyPlayers);

module.exports = playerListRouter;
