"use strict";

var cron = require('node-cron');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var Game = require('../controllers/game');
var _ = require('lodash');

var redis = require('../controllers/redis');
var GameObject = require('../controllers/gameObject');

var Player = require('../models/player');
var Squad = require('../models/squad');
var Hero = require('../models/hero');
//var Quest = require('../models/quest');
var Team = require('../models/team');
var BattleList = require('../models/battleList');
var Battle = require('../models/battle');
var Stage = require('../models/stage');
var Allies = require('../models/allies');
var Item = require('../models/item');
var Equipment = require('../models/equipment');
var Essence = require('../models/essence');
var SquadDonation = require('../models/squadDonation');
var Achievements = require('../models/achievements');
var ArenaRank = require('../models/arenaRank');

const models = [Player, Squad, BattleList, Battle, Team, Allies, ArenaRank];
const collections = [Stage, Hero, Item, Equipment, Achievements, Essence, SquadDonation];

/*

 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *

 */

// note: this cron may cause high memory usage. may need to find a better solution.

exports.MODELS_LIMIT = 100;
exports.COLLECTIONS_LIMIT = 15;

exports.init = function() {
    // models
    cron.schedule('* * * * *', function() {
        // console.log('running a task every minute');

        // single object models
        _.map(models, saveModel);

        // collections
        _.map(collections, saveCollection);

        // // lists
        // var lists = [];
        // _.map(lists, function (List) {
        //     List.generate();
        // });

    }); // end 1 min cron
};

// http://stackoverflow.com/questions/25285232/bulk-upsert-in-mongodb-using-mongoose

// Server already connected to mongoose!
// /**
//  * Atomic connect Promise - not sure if I need this, might be in mongoose already..
//  * @return {Promise}
//  */
// function connect(uri, options){
//     return new Promise(function(resolve, reject){
//         mongoose.connect(uri, options, function(err){
//             if (err) return reject(err);
//             resolve(mongoose.connection);
//         });
//     });
// }

/**
 * Bulk-upsert an array of records
 * @param  {Array}    records  List of records to update
 * @param  {Model}    Model    Mongoose Model to update
 * @param  {Object}   match    Database field to match
 * @return {Promise}  always resolves a BulkWriteResult
 */
function save(records, Model, match){
    match = match || 'id';
    return new Promise(function(resolve, reject){
        var bulk = Model.collection.initializeUnorderedBulkOp();

        _.map(records, function(record) {
            var query = {};
            query[match] = record[match];

            // cast redis _id string to ObjectId for query
            if(match === '_id') {
                query[match] = mongoose.Types.ObjectId(record[match]);
            }

            // THIS IS NOT NEEDED!
            // delete unique fields before updating
            // _.map(Model.schema.paths, function (path, pathKey) {
            //     if(path.options.hasOwnProperty('unique')) {
            //         delete record[pathKey];
            //     }
            // });

            //console.log(record)
            //console.log(Model.schema.paths)

            // never update _id
            delete record._id;

            bulk.find(query).upsert().updateOne( record );
        });

        bulk.execute(function(err, bulkres){
            if (err) return reject(err);
            resolve(bulkres);
        });
    });
}

function saveModel(Model) {
    var queueKey = GameObject.queueKey(Model);

    redis.smembers(queueKey)
        .then(function(queue) {
            if(!Array.isArray(queue)) { queue = []; }

            return queue;
        })
        .then(function(queue) {
            var objects = [];

            if(queue.length === 0) { return [queue, objects] }

            var slicedKeys = queue.slice(0, exports.MODELS_LIMIT);
            return Promise.all([queue, redis.mget(slicedKeys)]);
        })
        .spread(function (queue, objects) {
            if(queue.length === 0) return queue;

            //console.log(keys)
            //console.log(objects)

            // bulk save each collection
            return save(objects, Model, '_id')
                .then(function(bulkRes){
                    //console.log('Bulk complete.', bulkRes);
                    return queue;
                }, function(err){
                    console.log('Bulk Error:', err);
                    throw new Error('Failed to save model '+Model.modelName);
                });
        })
        .then(function (queue) {
            return removeQueueMembers(queueKey, queue);
        })
        .then(function (result) {
            if(Game.DEBUG && result.length > 0) {
                console.log('Sync objects '+Model.modelName+'!');
            }
        })
        .catch(function(err) {
            console.log('Failed to sync model '+Model.modelName+'!');
            console.log(err);
        });
}

function saveCollection(Model) {
    var queueKey = GameObject.queueKey(Model);

    redis.smembers(queueKey)
        .then(function(queue) {
            if(!Array.isArray(queue)) { queue = []; }

            return queue;
        })
        .then(function(queue) {
            var objects = [];

            if(queue.length === 0) { return [queue, objects] }

            var slicedKeys = queue.slice(0, exports.COLLECTIONS_LIMIT);

            objects = _.map(slicedKeys, function (key) {
                return redis.hgetall(key);
            });

            var collectionPromises = Promise.all(objects);
            return Promise.all([queue, collectionPromises]);
        })
        .spread(function (queue, objects) {
            // bulk save each collection
            var saves = _.map(objects, function (collection) {
                return save(collection, Model, '_id')
                    .then(function(bulkRes){
                        //console.log('Bulk complete.', bulkRes);
                        return true;
                    }, function(err){
                        console.log('Bulk Error:', err);
                        throw new Error('Failed to save collection!');
                    });
            });

            return Promise.all(saves)
                .then(function () {
                    return queue;
                });
        })
        .then(function (queue) {
            return removeQueueMembers(queueKey, queue);
        })
        .then(function (result) {
            if(Game.DEBUG && result.length > 0) {
                console.log('Sync collections '+Model.modelName+'!');
            }
        })
        .catch(function(err) {
            console.log('Failed to sync collection '+Model.modelName+'!');
            console.log(err);
        });
}

function removeQueueMembers(queueKey, queue) {
    if(queue.length > 0) {
        var removeMembersPromises = _.map(queue, function (key) {
            return redis.srem(queueKey, key);
        });

        return Promise.all(removeMembersPromises);
    }

    return [];
}