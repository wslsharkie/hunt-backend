"use strict";

var Token = require('../models/token');
var Player = require('../models/player');
var uid = require('uid-safe');
var redis = require('../controllers/redis');

const EXPIRATION_TIME = 7776000 * 1000; // about 3 months in milliseconds
const KEY_PREFIX = 'tokens-';

exports.resourceKey = function (value) {
    return KEY_PREFIX+value;
};

exports.create = function (client) {
    var tokenValue = uid.sync(18);
    var expirationDate = Date.now() + EXPIRATION_TIME;

    // Create a new access token
    return new Token({
        value: tokenValue,
        clientId: client._id.toString(),
        playerId: client.playerId,
        expiration: expirationDate
    });
};

exports.set = function (token) {
    return redis.set(exports.resourceKey(token.value), token);
};

exports.get = function (tokenValue) {
    return redis.get(exports.resourceKey(tokenValue));
};

