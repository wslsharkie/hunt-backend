"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');
var _ = require('lodash');

// Define our user schema
var TeamSchema = new mongoose.Schema({
    ownerId:        { type: String, required: true, unique: true },
    teams:          { type: Array, required: true },
    activeTeamId:   { type: Number, required: true, default: 0},
    createdAt:      { type: Number, required: true }
});

// Execute before each user.save() call
TeamSchema.pre('save', function(callback) {
    //var player = this;
    callback();
});

TeamSchema.statics.gameObject = null;

TeamSchema.statics.getQuery = function(id) {
    return {ownerId: id};
};

TeamSchema.statics.resourceKey = function(objectId) {
    return 'teams:'+objectId.toString();
};

// overrides the static resourceKey
TeamSchema.methods.resourceKey = function(instance) {
    return 'teams:'+instance.ownerId;
};

TeamSchema.methods.findHeroPosition = function (heroId) {
    var position = -1;

    _.each(this.heroes, function (member, key) {
        if(heroId.toString() === key.toString()) {
            position = key;
            return false;
        }
    });

    return position;
};

// Export the Mongoose model
var Team = mongoose.model('Team', TeamSchema);
Team.gameObject = new GameObject({model: Team});
module.exports = Team;
