"use strict";

var Item = require('../models/item');
var jsonfile = require('jsonfile');
var Game = require('../controllers/game');
var _ = require('lodash');

exports.items = {};

exports.COIN_ID = 3;

exports.createItem = function (playerId, itemId) {
    var config = exports.getItemConfig(itemId);

    if(config === null) {
        return null;
    }

    var instance = {
        ownerId:    playerId,
        itemId:     itemId,
        amount: 0,
        createdAt:  Date.now()
    };

    return new Item(instance);
};

exports.init = function() {
    var path = Game.paths.configPath+'/items/items.json';
    exports.items = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.items);
};

exports.getItemConfig = function(id) {
    id = id.toString();
    if(exports.items.hasOwnProperty(id)) {
        return exports.items[id];
    }

    return null;
};

exports.getItem = function (player, itemId) {
    return Item.gameObject.hmGet(player._id, itemId)
        .then(function (item) {
            return item;
        });
};

exports.getAll = function(player) {
    return Item.gameObject.hGetAll(player._id)
        .then(function (items) {
            return items;
            // return _.mapValues(items, function (itemInstance) {
            //     return exports.makeItemWithInstance(itemInstance);
            // });
        });
};

exports.getItems = function (player, itemIds) {
    if(itemIds.length > 0) {
        return Item.gameObject.hGetAll(player._id)
            .then(function (items) {
                return _.reduce(items, function (result, item) {
                    if(itemIds.includes(item.itemId)) {
                        result[item.itemId] = item;
                    }

                    return result;
                }, {});
            })
            .then(function(results) {
                _.each(itemIds, function(id) {
                    if (results.hasOwnProperty(id) === false)
                    {
                        results[id] = {itemId : id, amount : 0};
                    }
                })
                return results;
            });
    } else {
        return {};
    }
};

exports.makeItemWithInstance = function (instance) {
    var data = {};
    data.instance = instance;
    data.config = exports.getItemConfig(instance.itemId);
    return data;
};

exports.addItem = function (player, itemId, amount) {
    if(parseInt(amount) <= 0) {
        throw new Error("Must add item value greater than 0!");
    }

    return Item.gameObject.hmGet(player._id, itemId)
        .then(function (item) {
            if(item === null) {
                item = exports.createItem(player._id, itemId);
            }

            if(item === null) {
                throw new Error('Can not find item '+itemId+' config!');
            }

            item.amount += amount;
            return Item.gameObject.hSet(player._id, itemId, item);
        })
        .catch(function (err) {
            throw new Error(err.message);
        });
};

exports.subtractItem = function (player, itemId, amount) {
    if(parseInt(amount) <= 0) {
        throw new Error("Must subtract quantity greater than 1!");
    }

    return Item.gameObject.hmGet(player._id, itemId)
        .then(function (item) {
            if(item === null) {
                throw new Error("Sorry, cannot subtract for items you do not have.");
            }

            if(item === null) {
                throw new Error('Can not find item '+itemId+' config!');
            }

            if (amount > item.amount)
            {
                throw new Error("Sorry, you do not own enough of this item to subtract" + amount + " of them.");
            }

            item.amount -= amount;
            return Item.gameObject.hSet(player._id, itemId, item);
        })
        .catch(function (err) {
            throw new Error(err.message);
        });
};

exports.addItems = function (player, items) {
    if(items.length === 0) {
        return false;
    }

    return Item.gameObject.hGetAll(player._id)
        .then(function (playerItems) {
            _.each(items, function (item) {
                if(playerItems.hasOwnProperty(item.id) === false) {
                    var newItem = exports.createItem(player._id, item.id);
                    if(newItem === null) {
                        throw new Error('Can not find item '+item.id+' config!');
                    }

                    playerItems[item.id] = newItem;
                }

                playerItems[item.id].amount += item.amount;
            });

            return Item.gameObject.hmSet(player._id, playerItems);
        })
        .catch(function (err) {
            throw new Error(err.message);
        });
};

exports.subtractItems = function (player, items) {
    if(items.length === 0) {
        return false;
    }

    return Item.gameObject.hGetAll(player._id)
        .then(function (playerItems) {
            _.each(items, function (item) {
                if(playerItems.hasOwnProperty(item.id) === false) {
                    var newItem = exports.createItem(player._id, item.id);
                    if(newItem === null) {
                        throw new Error('Can not find item '+item.id+' config!');
                    }

                    playerItems[item.id] = newItem;
                }

                playerItems[item.id].amount -= item.amount;
            });

            return Item.gameObject.hmSet(player._id, playerItems);
        })
        .catch(function (err) {
            throw new Error(err.message);
        });
};

