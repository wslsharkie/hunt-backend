// Load required packages
var User = require('../models/user');
var Client = require('../models/client');
var keyHash = require('key-hash');
var uid = require('uid-safe');

const PASSWORD_MIN_LENGTH = 4;
const PASSWORD_MAX_LENGTH = 12;
const VALID_PASSWORD_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
const VALID_CODE_CHARS = "1234567890-";

function isPasswordValid(password) {
    if(password.length < PASSWORD_MIN_LENGTH || password.length > PASSWORD_MAX_LENGTH) {
        return false;
    }

    for(var i=0; i<password.length; i++) {
        var char = password.charAt(i);
        if(VALID_PASSWORD_CHARS.includes(char) === false) {
            return false;
        }
    }

    return true;
}

function isCodeValid(code) {
    for(var i=0; i<code.length; i++) {
        var char = code.charAt(i);
        if(VALID_CODE_CHARS.includes(char) === false) {
            return false;
        }
    }

    return true;
}

/** ENDPOINTS **/
exports.requestCode = function(req, res) {
    var bodyProps = ['client_id', 'client_secret', 'password'];

    for(var p in bodyProps) {
        var prop = bodyProps[p];
        if(!req.body.hasOwnProperty(prop)) {
            return res.send({error: "Missing data in post body!"});
        }
    }

    if(isPasswordValid(req.body.password) === false) {
        return res.send({error: "Invalid password!"});
    }

    var player = req.player;
    var password = req.body.password;
    var code = keyHash(player._id + '-' + password);
    var clientId = req.body.client_id;
    var clientSecret = req.body.client_secret;
    var userQuery = { clientId: clientId, clientSecret: clientSecret };

    // attempt to remove previously setup user first
    User.find(userQuery).remove(userQuery).exec()
        .then(function () {
            var user = new User({
                clientId:     req.body.client_id,
                clientSecret: req.body.client_secret,
                password:     password,
                code:         code
            });

            return user.save();
        })
        .then(function () {
            return res.send({code: code, password: password});
        })
        .catch(function (err) {
            return res.send({error: "Failed to get account binding code!"});
        });
};

exports.recoverAccount = function(req, res) {
    var bodyProps = ['code', 'password'];

    for(var p in bodyProps) {
        var prop = bodyProps[p];
        if(!req.body.hasOwnProperty(prop)) {
            return res.send({error: "Missing data in post body!"});
        }
    }

    if(isPasswordValid(req.body.password) === false) {
        return res.send({error: "Invalid password!"});
    }

    if(isCodeValid(req.body.code) === false) {
        return res.send({error: "Invalid code!"});
    }
    var password = req.body.password;
    var code = req.body.code;
    var playerId, client, user, newClient, secret;

    User.find({ code: code, password: password }).exec()
        .then(function (resultArr) {
            if(resultArr.length === 0) {
                throw new Error("Failed to find account!");
            }

            user = resultArr[0];
            return Client.findOne({ _id: user.clientId }).exec();
        })
        .then(function (currentClient) {
            //console.log(currentClient)
            if(!currentClient) {
                throw new Error("Failed to find client!");
            }

            client = currentClient;

            // validate secret password
            var isMatch = client.verifySecret(user.clientSecret);
            if(!isMatch) {
                throw new Error('Password invalid!');
            }

            playerId = client.playerId;

            // create new client for user to bind player id
            newClient = new Client();
            secret = uid.sync(18);
            newClient.secret = secret;
            newClient.playerId = playerId;

            return newClient.save();
        })
        .then(function () {
            var clientQuery = { _id: client._id };
            var userQuery = { _id: user._id };

            // delete client and user
            return Promise.all([
                Client.find(clientQuery).remove(clientQuery).exec(),
                User.find(userQuery).remove(userQuery).exec()
            ]);
        })
        .then(function () {
            return res.send({ _id: newClient._id, secret: secret });
        })
        .catch(function (err) {
            return res.send({error: err.message});
        });
};
