// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var AlliesSchema = new mongoose.Schema({
    ownerId:    { type: String, required: true, unique: true },
    hash:       { type: String, required: true, unique: true },
    allies:     { type: Array,  default: [] },
    pending:    { type: Array,  default: [] }, // pending invites/requests
    createdAt:  { type: Date, default: Date.now() }
});

AlliesSchema.pre('findOne', function(callback) {
    var query = this;

    if(mongoose.Types.ObjectId.isValid(query._conditions._id)) {
        return callback();
    }

    throw new Error('_id query is not an object id!');
});

AlliesSchema.statics.gameObject = null;

AlliesSchema.statics.getQuery = function(id) {
    return {ownerId: id};
};

AlliesSchema.statics.resourceKey = function(objectId) {
    return 'allies:'+objectId.toString();
};

AlliesSchema.methods.resourceKey = function(instance) {
    return 'allies:'+instance.ownerId;
};

// Export the Mongoose model
var Allies = mongoose.model('Allies', AlliesSchema);
Allies.gameObject = new GameObject({model: Allies});
module.exports = Allies;
