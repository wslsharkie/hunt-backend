var express = require('express');
var raidListRouter = express.Router();
var authController = require('../controllers/auth');
var raidListController = require('../controllers/raidList');

raidListRouter.route('/')
    .get(authController.isBearerAuthenticated, raidListController.showListOrGetRaidBattle);

raidListRouter.route('/test/generate')
    .post(authController.isBearerAuthenticated, raidListController.generateBattles);

module.exports = raidListRouter;
