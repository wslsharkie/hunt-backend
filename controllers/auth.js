// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var _ = require('lodash');

var Player = require('../controllers/player');
var Client = require('../models/client');
var ClientController = require('../controllers/client');
var PlayerController = require('../controllers/player');
var HeroController = require('../controllers/hero');
var Admin = require('../models/admin');
var Token = require('../controllers/token');

passport.use('client-basic', new BasicStrategy(
    {passReqToCallback: true},
    function(req, username, password, callback) {
        var playerName = "New Player";

        if(req.body.name !== undefined && req.body.name.length !== 0) {
            playerName = _.truncate(req.body.name, { 'length': 24 });
        }

        Client.findOne({ _id: username }).exec()
            .then(function(client){
                // validate secret password
                var isMatch = client.verifySecret(password);
                if(isMatch) return client;
                throw new Error('Password invalid!');
            })
            .then(function(client){
                // no pid so create new player
                if(client.playerId === "") {
                    PlayerController.newPlayer(client, playerName)
                        .then(function (player) {
                            return HeroController.addStarterHeroes(player);
                        })
                        .then(function() {
                            return callback(null, client);
                        })
                } else {
                    // client is authorized
                    return callback(null, client);
                }
            })
            .catch(function(err){
                return callback(null, false);
            });
    }
));

passport.use('new-client-basic', new BasicStrategy(
    function(username, password, callback) {
        var isMatch = false;

        if(ClientController.constants._GameClientName === username
        && ClientController.constants._GameClientPassword === password) {
            isMatch = true;
        }
        return callback(null, isMatch);
    }
));

passport.use('admin-basic', new BasicStrategy(
   function(username, password, callback) {
       Admin.findOne({ username: username }, function (err, admin) {
           if (err) { return callback(err); }

           // No user found with that username
           if (!admin) { return callback(null, false); }

           // Make sure the password is correct
           admin.verifyPassword(password, function(err, isMatch) {
               if (err) { return callback(err); }

               // Password did not match
               if (!isMatch) { return callback(null, false); }

               // Success
               return callback(null, admin);
           });
       });
   }
));

passport.use(new BearerStrategy(
  function(accessToken, callback) {
      Token.get(accessToken)
          .then(function(token){
              var tokenDate = new Date(token.expiration);

              if(token && Date.now() < tokenDate.getTime()) {
                  return Player.get(token.playerId);
              }

              return null;
          })
          .then(function (player) {
              if(player) {
                  return callback(null, player);
              }
              return callback(null, false);
          })
          .catch(function(err) {
              //console.log(err);
              return callback(null, false);
          });
  }
));

//exports.isAuthenticated = passport.authenticate(['basic', 'bearer'], { session : false });
exports.isAdminAuthenticated = passport.authenticate(['admin-basic'], { session : false });
exports.isNewClientAuthenticated = passport.authenticate('new-client-basic', { session : false });
exports.isClientAuthenticated = passport.authenticate('client-basic', { session : false });
exports.isBearerAuthenticated = passport.authenticate('bearer', { session: false, assignProperty: 'player' });
