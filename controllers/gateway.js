"use strict";

var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var Essence = require('../controllers/essence');
var Item = require('../controllers/item');
var Hero = require('../controllers/hero');
var jsonfile = require('jsonfile');
var _ = require('lodash');

// list of available gateways
exports.available_gateways = ["0", "1"];

exports.gateways = {};

exports.COST_TYPES = [{name: "gems"}, {name: "coins"}];

exports.init = function() {
    var path = Game.paths.configPath+'/gateways/gateways.json';
    exports.gateways = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.gateways);
    Game.deepFreeze(exports.available_gateways);
};

// generate a list of players
// get random players of the same level range
// client will store this list in cache to randomize

exports.summon = function (req, res) {
    if(req.params.buy_id === undefined) {
        return res.send({error: "Gateway request not recognized!"});
    }

    var player = req.player;
    var gateway_id = req.params.buy_id;
    var gateway_config, rewards;

    if(exports.available_gateways.hasOwnProperty(gateway_id) === false) {
        return res.send({error: "Sorry, gateway not available."});
    }

    if (exports.gateways.hasOwnProperty(gateway_id) === false) {
        return res.send({error: "Gateway " +gateway_id + " not recognized"});
    }

    gateway_config = exports.gateways[gateway_id];

    var costId = 0;
    var cost = gateway_config.cost;
    var costName = exports.COST_TYPES[costId].name;
    var playerCurrency = player.gems;

    if(req.body.hasOwnProperty("type") && req.body["type"] === "1") {
        costId = parseInt(req.body["type"]);
        cost = gateway_config.costInCoins;
        costName = exports.COST_TYPES[costId].name;
        playerCurrency = player.coins;
    }

    if(Game.DEBUG) {
        cost = 0;
    }

    if (playerCurrency < cost)
    {
        return res.send({error: "Sorry, you do not have enough "+costName
        +". " + costName + " "+cost+" needed!"});
    }

    return Player.gameObject.lock(player._id)
        .then(function(lockedPlayer) {
            player = lockedPlayer;

            if (playerCurrency < cost)
            {
                throw new Error("Sorry, you do not have enough "+costName
                    +". " + costName + " "+cost+" needed!");
            }

            switch(costId) {
                case 0:
                    return player.gems -= cost;
                    break;
                case 1:
                    return player.coins -= cost;
                    break;
                default:
                    throw new Error("Invalid cost type!");
                    break;
            }
        })
        .then(function () {
            return Hero.get(player);
        })
        .then(function (allHeroData) {
            rewards = exports.rollForHero(gateway_id, allHeroData);
            return exports.rewardForRoll(rewards, player);
        })
        .then(function () {
            return Player.gameObject.unlock(player)
        })
        .then(function () {
            return res.send({player: player, award: exports.showResults(rewards)});
        })
        .catch(function (err) {
            //console.log(err)
            Player.gameObject.revert(player);
            return res.send({error: "Failed to summon from Gateway!"});
        });
};

exports.rollForHero = function (gateway_id, allHeroData)
{
    var gateway_config = exports.gateways[gateway_id];

    var tierMaxRoll = gateway_config.table_roll_total;
    var rewardSelected = false;
    var tries = 0;

    var roll = _.random(0, (tierMaxRoll - 1));
    var tierChance = 0;
    var selectedTierInfo = false;
    var selectedHeroPool = false;
    var selectedHero = false;

    var maxedTierList = [];
    var maxedHeroList = [];

    while (tries < 100000 && rewardSelected === false) {
        selectedTierInfo = false;
        selectedHeroPool = {};
        selectedHero = false;
        tierChance = 0;

        roll = _.random(0, (gateway_config.table_roll_total - 1));

        //start with tier selection
        if (exports.isEmptyTierTable(gateway_config.table, maxedTierList))
        {
            break;
        }

        _.forEach(gateway_config.table, function (tier) {
            if (!maxedTierList.includes(tier.tier))
            {
                tierChance += tier.tier_roll_total;
                if (roll < tierChance) {
                    selectedTierInfo = tier;
                    return false;
                }
            }
        });

        //if tier is not a valid tier, re-loop tier selection
        if (maxedTierList.includes(selectedTierInfo.tier) || selectedTierInfo === false)
        {
            continue;
        }

        //with tier selected, start hero selection loop
        var emptyPool = true;
        while (selectedHero === false)
        {

            //if heroPool has ZERO valid hero options then break out of loop
            if(exports.isEmptyHeroPool(selectedTierInfo.pool, maxedHeroList))
            {
                maxedTierList.push(selectedTierInfo.tier);
                break;
            }

            selectedHero = _.sample(selectedTierInfo.pool);

            if (allHeroData.hasOwnProperty(selectedHero.hero_id)
                && Hero.getStars(allHeroData[selectedHero.hero_id]) >= Hero.max_stars
                && !maxedHeroList.includes(selectedHero.hero_id))
            {
                maxedHeroList.push(selectedHero.hero_id);
            }

            //if selectedHero is maxed then loop again and re-select
            if (maxedHeroList.includes(selectedHero.hero_id))
            {
                selectedHero = false;
                continue;
            }
        }

        //if after hero selection a hero is STILL NOT selected, then re-iterate tier selection loop
        if (selectedHero === false)
        {
            continue;
        } else {
            rewardSelected = true;
        }

        tries++;
    }

    if (rewardSelected === false)
    {
        return {"hero_id" : "recharge", "shards" : 1};
    } else {
        var shardsToReward = _.random(selectedHero.range.min, selectedHero.range.max);

        return {"hero_id" : selectedHero.hero_id, "shards" : shardsToReward};
    }
};

exports.isEmptyTierTable = function (tierTable, maxedTierList)
{
    var emptyTable = true;
    _.each(tierTable, function (tier)
    {
        if (!maxedTierList.includes(tier.tier))
        {
            emptyTable = false;
            return false;
        }
    });

    return emptyTable;
};

exports.isEmptyHeroPool = function (heroPool, maxedHeroList)
{
    var emptyPool = true;
    _.each(heroPool, function (hero) {
        if (!maxedHeroList.includes(hero.hero_id))
        {
            emptyPool = false;
            return false;
        }
    });

    return emptyPool;
};

/*
exports.getTierForRoll  = function (gateway_id) {
    var gateway_config = exports.gateways[gateway_id];
    var roll = _.random(0, gateway_config.table_roll_total) - 1;
    var count = 0;
    var tierConfig = 0;


    _.forEach(gateway_config.table, function (tier) {
        count += tier.tier_roll_total;
        if (roll < count)
        {
            tierConfig = tier;
            return false;
        }
    });

    if (tierConfig == 0)
    {
        throw new Error("Error, roll did not reward. Please contact Customer Service for help, we are very sorry about the delay");
    }

    return tierConfig;
};

exports.getHeroForRoll = function (tier_config) {
    var heroTable = tier_config.pool;
    var heroToReward = 0;
    var shardsToReward
    var chance = 0;
    var roll = _.random(0, tier_config.tier_roll_total) - 1;

    _.forEach(heroTable, function (heroOption) {
        chance += heroOption.chance;
        if (roll < count)
        {
            heroToReward = heroOption.hero_id;
            shardsToReward = _.random(heroOption.range.min, heroOption.range.max);
            return false;
        }
    })

    return {"hero_id" : heroToReward, "shards" : shardsToReward};

};
 */

/**
 * Should reward essence, or hero experience if essence is maxed
 * @param rewards - which is an array of objects
 * returns list of rewards
 */
exports.rewardForRoll = function(rewards, player) {

    if (rewards.hasOwnProperty("hero_id") === false || rewards.hasOwnProperty("shards") === false)
    {
        throw new Error("Error rewarding gateway");
    }

    if (rewards.hero_id === "recharge")
    {
        return Item.addItem(player, "1", 11);
    } else if (Hero.heroes.hasOwnProperty(rewards.hero_id))
    {
        return Essence.addEssence(player, rewards.hero_id, rewards.shards);
    }
}

/**
 * Packages return info for client
 * @param rewards - should be an object with { "hero_id" : x, "shards" : y}
 */
exports.showResults = function(rewards)
{
    var hero_id = rewards.hero_id;
    var resultsInfo  = {};

    if (hero_id === "recharge")
    {
        resultsInfo["desc"] = "Your heroes are already at their max shards count! You have received 1x Recharge instead. Thank you for your support!";
        resultsInfo["hero_id"] = hero_id;
        return resultsInfo;
    } else {
        var heroConfig = Hero.getHeroConfig(hero_id);

        //resultsInfo["name"] = heroConfig.name;
        resultsInfo["hero_id"] = hero_id;
        //resultsInfo["image"] = heroConfig.image;
        resultsInfo["shards"] = rewards.shards;
        return resultsInfo;
    }
}