"use strict";

const express = require('express');
const battleRouter = express.Router();
const authController = require('../controllers/auth');
const battleController = require('../controllers/battle');
const battleList = require('../controllers/battleList');
const rewardController = require('../controllers/reward');

battleRouter.route('/create/?')
    .post(authController.isBearerAuthenticated, battleController.createBattle);

battleRouter.route('/list/')
    .get(authController.isBearerAuthenticated, battleController.showBattleList);

battleRouter.route('/:object_id/?')
    .get(authController.isBearerAuthenticated, battleController.showBattle);

battleRouter.route('/:object_id/attack/?')
    .post(authController.isBearerAuthenticated, battleController.attack);

battleRouter.route('/:object_id/special/?')
    .post(authController.isBearerAuthenticated, battleController.special);

battleRouter.route('/:object_id/join/?')
    .post(authController.isBearerAuthenticated, battleController.joinBattle);

battleRouter.route('/:object_id/quit/?')
    .post(authController.isBearerAuthenticated, battleController.quitBattle);

battleRouter.route('/:object_id/collect/?')
    .post(authController.isBearerAuthenticated, battleController.collectBattle);

battleRouter.route('/recharge/')
    .post(authController.isBearerAuthenticated, battleController.payTeamRecharge);

battleRouter.route('/rewards/')
    .post(authController.isBearerAuthenticated, rewardController.showRewardsForBattle);

battleRouter.route('/list/delete')
    .post(authController.isBearerAuthenticated, battleList.destroy);

module.exports = battleRouter;
