// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var squadDonationSchema = new mongoose.Schema({
    requestorId:    { type: String, required: true },
    ownerId:        { type: String, required: true }, //ownerId = squadId
    itemId:         { type: String, required: true },
    donators:       { type: Object, required: false },
    donated:        { type: Number, required: true },
    createdAt:      { type: Number, required: true }
});

squadDonationSchema.statics.gameObject = null;

squadDonationSchema.statics.resourceKey = function(objectId) {
    return 'donations-'+objectId.toString();
};

squadDonationSchema.statics.fieldKey = function () {
    return 'requestorId';
};

// Export the Mongoose model
var squadDonation = mongoose.model('squadDonation', squadDonationSchema);
squadDonation.gameObject = new GameObject({model: squadDonation});
module.exports = squadDonation;
