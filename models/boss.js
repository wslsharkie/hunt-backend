// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var BossSchema = new mongoose.Schema({
    bossId:     { type: Number, required: true },
    ownerId:    { type: String, required: true },
    health:     { type: Number, required: true },
    attackers:  { type: Array,  required: false },
    createdAt:  { type: Number, required: true }
});

BossSchema.pre('findOne', function(callback){
    var query = this;

    if(mongoose.Types.ObjectId.isValid(query._conditions._id)) {
        return callback();
    }

    throw new Error('_id query is not an object id!');
});

BossSchema.statics.gameObject = null;

BossSchema.statics.resourceKey = function(objectId) {
    return 'bosses:'+objectId.toString();
};

// Export the Mongoose model
var Boss = mongoose.model('Boss', BossSchema);
Boss.gameObject = new GameObject({model: Boss});
module.exports = Boss;
