// Load required packages
var Hero = require('../controllers/hero');
var Minion = require('../controllers/minion');
var Quest = require('../controllers/quest');
var Stage = require('../controllers/stage');
var Item = require('../controllers/item');
var Ability = require('../controllers/ability');
var Equipment = require('../controllers/equipment');
var EventEmitter = require('events');
var Achievements = require('../controllers/achievements');
var Gateway = require('../controllers/gateway');
var SquadDonation = require('../controllers/squadDonation');
var _ = require('lodash');

exports.EventEmitter = null;
exports.EVENTS = { HOME : "home", QUEST_DONE : "quest_done"};

// const _GameClientName = "L3%s#V[z_";
// const _GameClientPassword = "87eYrHw%C+";

exports.DEBUG = true;

exports.paths = {
    configPath: __dirname+'/../configs'
};

exports.deepFreeze = function(obj) {
    if(obj === undefined || obj === null) {
        return obj;
    }

    // Retrieve the property names defined on obj
    var propNames = Object.getOwnPropertyNames(obj);

    // Freeze properties before freezing self
    propNames.forEach(function(name) {
        var prop = obj[name];

        // Freeze prop if it is an object
        if (typeof prop === 'object' && prop !== null) {
            exports.deepFreeze(prop);
        }
    });

    // Freeze self (no-op if already frozen)
    return Object.freeze(obj);
};

exports.init = function() {

    if (_.isNil(exports.EventEmitter)) {
        exports.EventEmitter = new EventEmitter();
    }

    Hero.init();
    Minion.init();
    Quest.init();
    Item.init();
    Stage.init();
    Ability.init();
    Equipment.init();
    Achievements.init();
    Gateway.init();
    SquadDonation.init();
};

exports.isoDateToTime = function (iso) {
    if(typeof iso === 'string') {
        return new Date(iso).getTime();
    }

    throw new Error('ISO date must be a string!');
};


/*
 {
 "message": "Client added!",
 "data": {
 "_id": "584b38c70eabfc1bd00b72d9",
 "secret": "zb_6aSB6PjQJjiB7KOtYwN8c"
 }
 }
 */
