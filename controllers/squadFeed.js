"use strict";

var Squad = require('../controllers/squad');
var SquadDonationController = require('../controllers/squadDonation');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var Promise = require('bluebird');
var _ = require('lodash');

const LIST_KEY = 'squads-feed-';
const MESSAGE_LENGTH = 160;
const FEED_LENGTH = 100;
const MESSAGE_TYPE = 5;
const DONATE_TYPE = 10;

exports.resourceKey = function (squadId) {
    return LIST_KEY+squadId;
};

exports.get = function(squadId) {
    return Squad.get(squadId)
        .then(function (squad) {
            if(!squad) {
                throw new Error("No squad found!");
            }

            return redis.lrange(exports.resourceKey(squadId), 0, FEED_LENGTH-1)
                .then(function (feed) {
                    if(!feed) {
                        feed = [];
                    }

                    return feed;
                });
        });
};

exports.getFeed = function (squadId) {
    return Promise.all([exports.get(squadId), SquadDonationController.get(squadId)])
        .spread(function(feedData, donationData) {
            return {feed: feedData, donations : donationData};
        })
}


/**** ENDPOINTS ***/
exports.post = function (req, res) {
    var player = req.player;

    if(req.body.squad_id === undefined) {
        return res.send({error: "Invalid squad id!"});
    }

    if(req.body.message === undefined) {
        return res.send({error: "Invalid message!"});
    }

    var squadId = req.body.squad_id.toString();
    var message = req.body.message.toString();

    if(message.length > MESSAGE_LENGTH) {
        return res.send({error: "Message is too long!"});
    }

    var messageObject = {
        type: MESSAGE_TYPE,
        pid: player._id,
        pname: player.name,
        msg: message,
        date: Date.now()
    };

    Squad.get(squadId)
        .then(function (squad) {
            if(!squad) {
                throw new Error("No squad found!");
            }

            redis.lpush(exports.resourceKey(squadId), messageObject)
                .then(function (feedLength) {
                    if(feedLength > FEED_LENGTH) {
                        return redis.ltrim(0,FEED_LENGTH-1);
                    }
                    return true;
                })
                .then(function () {
                    return exports.getFeed(squadId)
                })
                .then(function (feed) {
                    return res.send({feed: feed});
                });
        })
        .catch(function (err) {
            return res.send({error: "Invalid squad!"});
        });
};

exports.showFeed = function (req, res) {
    if(req.params.squad_id === undefined) {
        return res.send({error: "Invalid squad id!"});
    }

    var squadId = req.params.squad_id.toString();

    exports.getFeed(squadId)
        .then(function (feed) {
            return res.send({feed: feed});
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: "Invalid squad!"});
        });
};
