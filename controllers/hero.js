"use strict";

// Load required packages
var Hero = require('../models/hero');
var Game = require('../controllers/game');
var Player = require('../controllers/player');
var PlayerModel = require('../models/player');
var Essence = require('../controllers/essence');
var Promise = require('bluebird')
var jsonfile = require('jsonfile');
var _ = require('lodash');

// list of game heroes
exports.heroes = {};

exports.starterHeroes = [1, 2];

exports.max_stars = 5;

exports.stars_essences = [15, 25, 30, 65, 85, 9999];

const CLIENT_DEFAULTS = [
    'heroId',
    'ownerId',
    'health',
    'essence',
    'experience',
    'stars',
    'attack',
    'defense',
    'level'
];

exports.init = function() {
    var path = Game.paths.configPath+'/heroes/heroes.json';
    exports.heroes = jsonfile.readFileSync(path);
    Game.deepFreeze(exports.heroes);
    Game.deepFreeze(exports.starterHeroes);
    Game.deepFreeze(exports.max_stars);
};

exports.createHero = function (playerId, heroId) {
    var instance = exports.baseHeroPacket(playerId, heroId);
    instance.essence = exports.getEssenceForStars(1) // 1 star at least by default
    return new Hero(instance);
};

exports.baseHeroPacket = function (playerId, heroId)
{    var config = exports.getHeroConfig(heroId);

    if(config === null) {
        return null;
    }

    return {
        heroId:     heroId,
        ownerId:    playerId,
        health:     config.health,
        experience: 0,
        essence:    0
    };
}

exports.addHero = function (player, heroId) {
    return Hero.gameObject.hmGet(player._id, heroId)
        .then(function (hero) {
            if(hero) {
                throw new Error('Player already owns his hero!');
            }

            hero = exports.createHero(player._id, heroId);

            if(hero === null) {
                throw new Error('Can not find hero '+heroId+' config!');
            }

            return Hero.gameObject.hSet(player._id, heroId, hero);
        });
};

exports.get = function (player) {
    return Hero.gameObject.hGetAll(player._id)
        .then(function (heroes) {
           return _.mapValues(heroes, exports.makeFullHero);
        });
};

exports.makeFullHero = function (baseHeroData) {
    var fullHero = {config: exports.heroes[baseHeroData.heroId], instance: baseHeroData};

    baseHeroData.stars = exports.getStars(fullHero);
    baseHeroData.attack = exports.getAttack(fullHero);
    baseHeroData.defense = exports.getDefense(fullHero);
    baseHeroData.health = exports.getHealthMax(fullHero);
    baseHeroData.level = exports.getLevel(fullHero);
    return baseHeroData;
};

exports.clientHeroes = function (player) {
    var essenceTable = {};
    return Promise.all([exports.get(player), Essence.get(player)])
        .spread(function (heroes, essenceData) {
            essenceTable = essenceData;
            return _.mapValues(heroes, function (hero) {
                var clientHero = {};
                CLIENT_DEFAULTS.forEach(function (prop) {
                    clientHero[prop] = hero[prop];
                });
                if (essenceTable.hasOwnProperty(hero.heroId) && essenceTable[hero.heroId].essence > 0)
                {
                    clientHero['essence'] += essenceTable[hero.heroId].essence;
                }
                clientHero['nextess'] = exports.getEssenceForStars(parseInt(hero.stars) + 1);
                clientHero['images'] = exports.getImages(hero.heroId);
                Object.assign(clientHero, exports.getLevelData(hero));
                return clientHero;
            })
        })
        .then(function (clientHeroes) {

            return _.mapValues(exports.heroes, function (value, key) {
                if (clientHeroes.hasOwnProperty(key))
                {
                    return clientHeroes[key];
                } else if (key === 'version') {
                    return {};
                } else
                {
                    var blankHero = exports.makeFullHero(exports.baseHeroPacket(player._id, key));
                    var clientHero = {};
                    CLIENT_DEFAULTS.forEach(function (prop) {
                        clientHero[prop] = blankHero[prop];
                    });
                    if (essenceTable.hasOwnProperty(blankHero.heroId) && essenceTable[blankHero.heroId].essence > 0)
                    {
                        clientHero['essence'] += essenceTable[blankHero.heroId].essence;
                    }
                    clientHero['nextess'] = exports.getEssenceForStars(1);
                    clientHero['ownerId'] = '';
                    clientHero['stars'] = 0;
                    clientHero['images'] = exports.getImages(blankHero.heroId);
                    Object.assign(clientHero, exports.getLevelData(blankHero));
                    return clientHero;
                }
            });
        }).then(function (results) {
            if (results.hasOwnProperty('version'))
            {
                delete results.version;
                return results;
            }
        })
};

exports.getHero = function (player, heroId) {
    return Hero.gameObject.hmGet(player._id, heroId)
        .then(function (hero) {
            if(hero === null) {
                throw new Error('Player does not own this hero!');
            }
            hero.stars = exports.getStars(hero);
            return hero;
        });
};

exports.hasHero = function(player, heroId) {
    return Hero.gameObject.hmGet(player._id, heroId)
        .then(function (hero) {
            return (hero !== null);
        });
};

exports.addHeroes = function(player, heroes) {
    return Hero.gameObject.hGetAll(player._id, 'heroId')
        .then(function (playerHeroes) {
            _.each(heroes, function (heroId) {
                if(playerHeroes.hasOwnProperty(heroId)) {
                    throw new Error('Can not add existing hero!');
                }

                var hero = exports.createHero(player._id, heroId);

                if(hero === null) {
                    throw new Error('Can not find hero '+heroId+' config!');
                }

                playerHeroes[heroId] = hero;
            });

            return Hero.gameObject.hmSet(player._id, playerHeroes)
        })
        .catch(function (err) {
            //console.log(err)
            throw new Error(err.message);
        });
};

exports.addExperienceToHeroes = function (player, heroes, experience) {
    return Hero.gameObject.hGetAll(player._id)
        .then(function (playerHeroes) {
            _.each(heroes, function (heroId) {
                if(playerHeroes.hasOwnProperty(heroId) === false) {
                    throw new Error('Can not add experience to hero '+heroId+'!');
                }

                // current xp
                // xp to next level
                // previous level xp
                playerHeroes[heroId].experience += experience;
            });

            return Hero.gameObject.hmSet(player._id, playerHeroes)
        });
};

exports.getHeroConfig = function(heroId) {
    if(exports.heroes.hasOwnProperty(heroId)) {
        return exports.heroes[heroId];
    }

    return null;
};

exports.getImages = function (heroId) {
    if(exports.heroes.hasOwnProperty(heroId)) {
        var images = {};
        var image = exports.getImage(heroId);
        var portrait = exports.getPortraitImage(heroId);
        images[image] = {image: image};
        images[portrait] = {image: portrait};
        return images;
    }

    return null;
};

exports.getImage = function(heroId) {
    if(exports.heroes.hasOwnProperty(heroId)) {
        return exports.heroes[heroId].image;
    }

    return null;
};

exports.getPortraitImage = function (heroId) {
    if(exports.heroes.hasOwnProperty(heroId)) {
        return exports.heroes[heroId].portraitImage;
    }

    return null;
};

exports.makeHeroWithInstance = function (instance) {
    var data = {};
    data.instance = instance;
    data.instance.stars = exports.getStars(instance);
    data.config = exports.getHeroConfig(instance.heroId);
    return data;
};

exports.getLevel = function (hero) {
    var experience = 0;

    if(hero.hasOwnProperty("instance")) {
        experience = hero.instance.experience;
    } else {
        experience = hero.experience;
    }

    return exports.getLevelForExperience(experience);
};

exports.getExperienceForLevel = function (level) {
    if(level === 1) return 0;
    return Math.ceil(6.25*Math.pow(level,2));
};

exports.getLevelForExperience = function (experience) {
    if(experience === 0) {
        return 1;
    }

    var level = Math.floor(Math.sqrt(experience/6.25));
    return Math.max(1, level);
};

exports.getAttack = function (hero) {
    var heroLevel = exports.getLevelForExperience(hero.instance.experience);
    return Math.ceil(hero.config.attackScaling * heroLevel) + hero.config.attack;
};

exports.getDefense = function (hero) {
    var heroLevel = exports.getLevelForExperience(hero.instance.experience);
    return Math.ceil(hero.config.defenseScaling * heroLevel) + hero.config.defense;
};

exports.getHealthMax = function (hero) {
    var heroLevel = exports.getLevelForExperience(hero.instance.experience);
    return Math.ceil(hero.config.healthScaling * heroLevel) + hero.config.health;
};

exports.getStarsForEssence = function (essence) {
    var count = 0;
    _.each(exports.stars_essences, function(amount) {
        if (essence >= amount)
        {
            count++;
            essence -= amount;
        }
    });
    return count;
    /** old formula
    var quotient = essence / Math.pow(10, 2.699);
    var stars = Math.floor(Math.pow(quotient, 1/exports.max_stars));
    return Math.max(1, stars);
     **/
};

exports.getEssenceForStars = function(stars) {
    stars = parseInt(stars);
    var essence = 0;
    var count = 1;
    _.each(exports.stars_essences, function (amount) {
        if (count <= stars)
        {
            essence = amount;
        }
        count++;
    })
    /** old formula
    var essence = Math.pow(stars, exports.max_stars) * Math.pow(10, 2.699);
    return Math.ceil(essence);
     **/
    return essence;
};

exports.getStars = function(hero) {
    if(hero.hasOwnProperty('instance')) {
        hero = hero.instance;
    }

    return exports.getStarsForEssence(hero.essence);
};

function getLevelPct(experience) {
    var currentLevel = exports.getLevelForExperience(experience);
    var totalXPforNextLevel = exports.getExperienceForLevel(currentLevel + 1);
    var totalXPforCurrentLevel = exports.getExperienceForLevel(currentLevel);

    var experienceAtCurrentLevel = experience - totalXPforCurrentLevel;
    var experienceToNextLevel = totalXPforNextLevel - totalXPforCurrentLevel;

    // console.log(experienceAtCurrentLevel)
    // console.log(experienceToNextLevel)
    // console.log(experienceAtCurrentLevel/experienceToNextLevel);
    // console.log('********');

    return Math.round((experienceAtCurrentLevel / experienceToNextLevel)*100);
}

exports.getLevelData = function (hero) {
    var currentLevel = exports.getLevel(hero);
    var totalXPforNextLevel = exports.getExperienceForLevel(currentLevel + 1);
    var totalXPforCurrentLevel = exports.getExperienceForLevel(currentLevel);

    return {
        level: currentLevel,
        experienceAtLevel: hero.experience - totalXPforCurrentLevel,
        experienceToNextLevel: totalXPforNextLevel - totalXPforCurrentLevel
    }
};

exports.getLevelPctData = function(player, heroes, xpAdded) {
    return Hero.gameObject.hGetAll(player._id)
        .then(function (playerHeroes) {
            var levelData = {};

            _.each(heroes, function (heroId) {
                if(playerHeroes.hasOwnProperty(heroId) === false) {
                    throw new Error('Can not add experience to hero '+heroId+'!');
                }

                var totalHeroExperience = playerHeroes[heroId].experience;

                levelData[heroId] = {
                    prevPct:      getLevelPct(totalHeroExperience - xpAdded),
                    currentPct:   getLevelPct(totalHeroExperience),
                    prevLevel:    exports.getLevelForExperience(totalHeroExperience - xpAdded),
                    currentLevel: exports.getLevelForExperience(totalHeroExperience)
                };
            });

            return levelData;
        });
};

/*** ENDPOINTS ***/
exports.showHeroes = function(req, res) {
    var player = req.player;

    exports.clientHeroes(player)
        .then(function (heroes) {
            return res.send({heroes: heroes});
        })
};

exports.addStarterHeroes = function(player) {
    return exports.addHeroes(player, exports.starterHeroes);
};

exports.addStarterHeroesForClient = function(req, res) {
    var player = req.player;

    return exports.addStarterHeroes(player)
        .then(function () {
            return res.send({message: 'Starter heroes added!'});
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: err.message});
        });
};

exports.showHero = function (req, res) {
    var player = req.player;
    var heroId = req.params.hero_id;

    return exports.getHero(player._id, heroId)
        .then(function (hero) {
            return res.send({hero: hero});
        })
        .catch(function (err) {
            //console.log(err)
            return res.send({error: 'Player does not own this hero!'});
        });
};

exports.resetHeroes = function(req, res) {
    var player = req.player;
    Hero.gameObject.delete({name: 'ownerId', value: player._id})
        .then(function () {
            return res.send({message: 'Heroes reset!'});
        });
};

exports.summonHero = function (req, res) {
    var player = req.player;

    if(req.params.hero_id === undefined) {
        res.send({error: "Missing param!"});
    }

    var heroId = req.params.hero_id;

    var config = exports.getHeroConfig(heroId);

    if(config === null) {
        throw new Error("Essence hero "+heroId+" does not exist!");
    }

    var essenceRequired = exports.getEssenceForStars(1);

    PlayerModel.gameObject.lock(player._id)
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return exports.hasHero(player, heroId);
        })
        .then(function (hasHero) {
            if(hasHero) {
                throw new Error("Player already owns hero "+heroId+"!");
            }

            return Essence.subEssence(player, heroId, essenceRequired);
        })
        .then(function () {
            return exports.addHero(player, heroId);
        })
        .then(function () {
            return exports.getHero(player, heroId);
        })
        .then(function (hero) {
            hero.essence += essenceRequired; // star level 1

            return Hero.gameObject.hSet(player._id, heroId, hero)
                .then(function () {
                    return hero;
                });
        })
        .then(function (hero) {
            return PlayerModel.gameObject.unlock(player)
                .then(function () {
                    return hero;
                })
        })
        .then(function (hero) {
            hero.stars = 1;
            return res.send({hero: hero});
        })
        .catch(function (err) {
            //console.log(err)
            PlayerModel.gameObject.revert(player);
            res.send({error: "Failed to summon hero!"});
        });
};

exports.incrementStar = function (req, res) {
    var player = req.player;

    if(req.params.hero_id === undefined) {
        res.send({error: "Missing param!"});
    }

    var heroId = req.params.hero_id;

    var config = exports.getHeroConfig(heroId);

    if(config === null) {
        throw new Error("Essence hero "+heroId+" does not exist!");
    }

    PlayerModel.gameObject.lock(player._id)
        .then(function (lockedPlayer) {
            player = lockedPlayer;
            return exports.getHero(player, heroId);
        })
        .then(function (hero) {
            var currentStars = exports.getStars(hero);
            var essenceRequired = exports.getEssenceForStars(parseInt(currentStars)+1);

            return Essence.subEssence(player, heroId, essenceRequired)
                .then(function () {
                    return Promise.all([hero, essenceRequired]);
                })
        })
        .spread(function (hero, essenceRequired) {
            hero.essence += essenceRequired; // star level up!

            return Hero.gameObject.hSet(player._id, heroId, hero)
                .then(function () {
                    return hero;
                });
        })
        .then(function (hero) {
            return PlayerModel.gameObject.unlock(player)
                .then(function () {
                    return hero;
                });
        })
        .then(function (hero) {
            hero.stars = exports.getStars(hero);
            return res.send({hero: hero});
        })
        .catch(function (err) {
            //console.log(err)
            PlayerModel.gameObject.revert(player);
            res.send({error: "Failed to summon hero!"});
        });
};
