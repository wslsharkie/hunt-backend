"use strict";

var express = require('express');
var testerRouter = express.Router();
var authController = require('../controllers/auth');
var testerController = require('../controllers/tester');

testerRouter.route('/test1/?')
    .post(authController.isBearerAuthenticated, testerController.test1);

testerRouter.route('/test2/?')
    .post(authController.isBearerAuthenticated, testerController.test2);

testerRouter.route('/test4/?')
    .post(authController.isBearerAuthenticated, testerController.test4);

testerRouter.route('/test5/?')
    .post(authController.isBearerAuthenticated, testerController.test5);

testerRouter.route('/test6/?')
    .post(authController.isBearerAuthenticated, testerController.test6);

testerRouter.route('/test7/?')
    .post(authController.isBearerAuthenticated, testerController.test7);

testerRouter.route('/test8/?')
    .post(authController.isBearerAuthenticated, testerController.test8);

testerRouter.route('/test9/?')
    .post(authController.isBearerAuthenticated, testerController.test9);

testerRouter.route('/test10/?')
    .post(authController.isBearerAuthenticated, testerController.test10);

testerRouter.route('/test11/?')
    .post(authController.isBearerAuthenticated, testerController.test11);

testerRouter.route('/test12/?')
    .post(authController.isBearerAuthenticated, testerController.test12);

testerRouter.route('/test13/?')
    .post(authController.isBearerAuthenticated, testerController.test13);

testerRouter.route('/test14/?')
    .post(authController.isBearerAuthenticated, testerController.test14);

module.exports = testerRouter;
