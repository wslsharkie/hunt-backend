var express = require('express');
var oauth2Router = express.Router();
var oauth2Controller = require('../controllers/oauth2');
var authController = require('../controllers/auth');
var playerController = require('../controllers/player');

// oauth2Router.route('/authorize/?')
//   .get(oauth2Controller.authorization)
//   .post(oauth2Controller.decision);

// oauth2Router.route('/token/?')
//     .post(authController.isClientAuthenticated, oauth2Controller.token);

module.exports = oauth2Router;
