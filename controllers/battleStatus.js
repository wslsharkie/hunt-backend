"use strict";

// Load required packages
var Player = require('../models/player');
var Game = require('../controllers/game');
var redis = require('../controllers/redis');
var Promise = require('bluebird');
var _ = require('lodash');

exports.statusKey = function (battleId, playerId) {
    // battleId + playerId
    return "status-"+battleId+"-"+playerId;
};

exports.get = function (battle, player) {
    return redis.get(exports.statusKey(battle._id, player._id))
        .then(function (status) {
            if(status === null) {
                status = {heroes: {}, minions: {}};

                _.each(battle.players[player._id].team, function (hero) {
                    status.heroes[hero.id] = {};
                });

                _.each(battle.minions, function (minion) {
                    status.minions[minion.id] = {};
                });

                return status;
            } else {
                return status;
            }
        });
};

exports.set = function (battle, player, value) {
    return redis.set(exports.statusKey(battle._id, player._id), value);
};
