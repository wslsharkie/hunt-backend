// Load required packages
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var session = require('express-session');
var passport = require('passport');
var bluebird = require('bluebird');
var settings = require('./settings');

console.log('Node Environment: '+process.env.NODE_ENV);

// Connect to MongoDB
mongoose.set('debug', settings.MONGO_DB_DEBUG);
mongoose.Promise = bluebird;
mongoose.connect(settings.MONGODB_CONNECT);

// redis
var redis = require('redis');
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

var redisController = require('./controllers/redis');
redisController.init(settings.REDIS_PORT, settings.REDIS_HOST);

// game
var game = require('./controllers/game');
game.init();

// cron
var cron = require('./cron/cron');
cron.init();

// Create our Express application
var app = express();

// Set view engine to ejs
app.set('view engine', 'ejs');

// Use the body-parser package in our application
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// Use express session support since OAuth2orize requires it
// app.use(session({
//   secret: 'Super Secret Session Key',
//   saveUninitialized: true,
//   resave: true
// }));

// Use the passport package in our application
app.use(passport.initialize());

// serving static files such as images and json files
app.use(express.static('public'));

require('./routes')(app);

// Start the server
app.listen(settings.APP_PORT);