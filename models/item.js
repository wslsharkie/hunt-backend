// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var ItemSchema = new mongoose.Schema({
    ownerId:    { type: String, required: true },
    itemId:     { type: Object, required: true },
    amount:     { type: Number, required: true, default: 0},
    createdAt:  { type: Number, required: true }
});

ItemSchema.statics.gameObject = null;

ItemSchema.statics.resourceKey = function(objectId) {
    return 'items:'+objectId.toString();
};

ItemSchema.statics.fieldKey = function () {
    return 'itemId';
};

// Export the Mongoose model
var Item = mongoose.model('Item', ItemSchema);
Item.gameObject = new GameObject({model: Item});
module.exports = Item;
