"use strict";

var express = require('express');
var achievementsRouter = express.Router();
var authController = require('../controllers/auth');
var achievementsController = require('../controllers/achievements');

achievementsRouter.route('/')
    .get(authController.isBearerAuthenticated, achievementsController.showAllAchievementInfo);

achievementsRouter.route('/collect')
    .post(authController.isBearerAuthenticated, achievementsController.collectAchievement);

module.exports = achievementsRouter;
