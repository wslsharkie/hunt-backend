"use strict";

var Team = require('../controllers/team');
var Game = require('../controllers/game');
var Player = require('../controllers/player');

exports.VERSION = {
    HEROES:         "1",
    MINIONS:        "1",
    ITEMS:          "2",
    EQUIPMENT:      "1",
    ACHIEVEMENTS:   "1",
    STAGES:         "1",
    ABILITIES:      "1",
    IMAGES:         "3",
    NEWPLAYER:      "1"
};

exports.getHome = function(req, res) {
    var player = req.player;

    // testing achievements!
    Game.EventEmitter.emit(Game.EVENTS.HOME, player._id);

    return Team.getActiveTeam(player)
        .then(function (team) {
            var configs = {
                heroes:         {json: "heroes.json",       v: exports.VERSION.HEROES},
                minions:        {json: "minions.json",      v: exports.VERSION.MINIONS},
                items:          {json: "items.json",        v: exports.VERSION.ITEMS},
                equipment:      {json: "equipment.json",    v: exports.VERSION.EQUIPMENT},
                achievements:   {json: "achievements.json", v: exports.VERSION.ACHIEVEMENTS},
                stages:         {json: "stages.json",       v: exports.VERSION.STAGES},
                abilities:      {json: "abilities.json",    v: exports.VERSION.ABILITIES},
                images:         {json: "images.json",       v: exports.VERSION.IMAGES},
                newplayer:      {json: "newplayer.json",    v: exports.VERSION.NEWPLAYER}
            };

            return res.send({
                player: Player.clientPlayer(player, ['name', 'squadId']),
                team: team,
                configs: configs
            });
        })
        .catch(function(err) {
            //console.log(err)
            return res.send({error: 'Failed to get home contents!'});
        });
};
