"use strict";

var Battle = require('../models/battle');
var BattleController = require('../controllers/battle');
var BattleList = require('../controllers/battleList');
var Hero = require('../controllers/hero');
var Team = require('../controllers/team');
var Game = require('../controllers/game');
var Player = require('../models/player');
var PlayerController = require('../controllers/player');
var redis = require('../controllers/redis');
var mongoose = require('mongoose');
var Promise = require('bluebird');
var _ = require('lodash');

// generate a list of public battles for player to join
exports.LIST_KEY = 'raid:battle:list:';
exports.LIST_MAX = 5;
exports.EXPIRE_TIME = 600; // seconds
exports.MIN_RANGE = 10;
exports.RANGE_BASE = 1.15;

exports.resourceKey = function(player) {
    return exports.LIST_KEY+player._id;
};

exports.get = function (player) {
    return redis.get(exports.resourceKey(player))
        .then(function (list) {
            if(Game.DEBUG === false && list) {
                return list;
            }

            return Team.getAverageTeamLevel(player)
                .then(function (teamLevel) {
                    var rangeValue = getRangeValue(teamLevel);
                    var levelMin = Math.max(0, teamLevel - rangeValue);
                    var levelMax = teamLevel+rangeValue;

                    var query = [
                        // Grouping pipeline
                        { "$match": {
                            "level": {"$gte":levelMin, "$lte": levelMax},
                            "ownerId": {"$ne": mongoose.Types.ObjectId(player._id)},
                            "public": {"$eq" : true},
                            "state": {"$eq" : BattleController.STATE.RUNNING}
                        } },
                        // specify fields
                        { "$project" : {
                            "battleId" : 1 ,
                            "ownerId" : 1,
                            "minions": 1,
                            "level" : 1
                        } },
                        // random sample pipeline
                        { "$sample": { "size" : exports.LIST_MAX } }
                    ];

                    return Battle.aggregate(query).exec()
                        .then(function (battles) {
                            var battleList = {};
                            var playerPromises = [];

                            _.each(battles, function (battle) {
                                var config = BattleController.getConfig(battle.battleId);

                                battle.name = config.name;
                                battle.health = getBattleHealth(battle.minions);
                                battle.healthMax = getBattleHealthMax(battle.minions);

                                delete battle.minions;
                                delete battle.battleId;

                                battleList[battle.ownerId] = battle;

                                playerPromises.push(PlayerController.get(battle.ownerId));
                            });

                            if(battles.length > 0) {
                                return Promise.all(playerPromises)
                                    .then(function (players) {
                                        _.each(players, function (p) {
                                            if(p) {
                                                battleList[p._id].playerName = p.name;
                                            }
                                        });

                                        return redis.set(exports.resourceKey(player), battleList, exports.EXPIRE_TIME);
                                    })
                                    .then(function () {
                                        return battleList;
                                    })
                            }

                            return battleList;
                        });
                });
        })
        .catch(function (err) {
            //console.log(err)
            throw new Error("Failed to get raids list!");
        });
};

function getBattleHealth(minions) {
    return _.reduce(minions, function (res, minion) {
        res += minion.health;
        return res;
    }, 0);
}

function getBattleHealthMax(minions) {
    return _.reduce(minions, function (res, minion) {
        res += minion.healthMax;
        return res;
    }, 0);
}

function getRangeValue(teamLevel) {
    if(teamLevel < 1) {
        return exports.MIN_RANGE;
    }

    return Math.floor(teamLevel * Math.pow(exports.RANGE_BASE, 2) - teamLevel) + exports.MIN_RANGE;
}

/*** ENDPOINTS ***/
exports.showListOrGetRaidBattle = function(req, res) {
    var player = req.player;

    BattleList.get(player._id)
        .then(function (battleList) {
            var raids = _.filter(battleList.list, {type: BattleController.TYPE.RAID});

            if(raids.length > 0) {
                var raid = raids[0];

                return BattleController.getBattle(raid.id)
                    .then(function (data) {
                        var battle = {};
                        battle.instance = data;
                        battle.config = BattleController.getConfig(data.battleId);
                        var team = BattleController.makeFullTeamWithConfig(
                            battle.instance.players[player._id].team,
                            Hero.heroes);
                        return res.send({player: player, battle: battle, team: team});
                    });
            }

            return exports.get(player)
                .then(function (list) {
                    var clientList = _.mapValues(list, function (raid) {
                        return {
                            "_id": raid._id,
                            "ownerId": raid.ownerId,
                            "title": "Hunter LV. "+raid.level+": "+raid.playerName,
                            "name": raid.name,
                            "desc": "Battle HP: "+raid.health +"/"+raid.healthMax
                        }
                    });

                    return res.send({list: clientList});
                })
        })
        .catch(function (err) {
            //console.log(err);
            return res.send({error: "Failed to retrieve raid list!"});
        });
};

// testing:
exports.generateBattles = function(req, res) {
    var player = req.player;

    if(Game.DEBUG === false) {
        return res.send({error: "unauthorized"});
    }

    var size = 10;
    var battleId = 2;
    var players;

    var query = [
        // Grouping pipeline
        { "$match": {
            "_id": {"$ne": mongoose.Types.ObjectId(player._id)}
        } },
        // // specify fields
        // { "$project" : {
        //     "name" : 1,
        //     "experience" : 1
        // } },
        // random sample pipeline
        { "$sample": { "size" : size } }
    ];

    return Player.aggregate(query).exec()
        .then(function (queryResults) {
            players = queryResults;

            //console.log(players)

            if(players.length <= 0) {
                throw new Error('No players to generate battles!');
            }

            // set teams
            var teamPromises = _.map(players, function (p) {
                return Team.setTeams(p, Team.getStarterTeams());
            });

            return Promise.all(teamPromises);
        })
        .then(function () {
            var battlePromises = _.map(players, function (p) {
                return BattleController.create(p, battleId);
            });

            return Promise.all(battlePromises);
        })
        .then(function (results) {
            return res.send({battles: results});
        })
        .catch(function (err) {
            return res.send({error: err.message});
        });
};
