// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var EssenceSchema = new mongoose.Schema({
    heroId:     { type: Number, required: true },
    ownerId:    { type: String, required: true },
    essence:    { type: Number, required: true, default: 0 }, // star
    date:       { type: Date, required: true, default: Date.now() }
});

EssenceSchema.statics.gameObject = null;

EssenceSchema.statics.resourceKey = function(objectId) {
    return 'essence-'+objectId.toString();
};

EssenceSchema.statics.fieldKey = function () {
    return 'heroId';
};

// Export the Mongoose model
var Essence = mongoose.model('Essence', EssenceSchema);
Essence.gameObject = new GameObject({model: Essence});
module.exports = Essence;
