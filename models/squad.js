"use strict";

// Load required packages
var mongoose = require('mongoose');
var GameObject = require('../controllers/gameObject');

// Define our user schema
var SquadSchema = new mongoose.Schema({
    leaderId:   { type: String, required: true, unique: true },
    name:       { type: String, required: true, unique: true },
    members:    { type: Object, required: true },
    blurb:      { type: String, required: false},
    wins:       { type: Number, default: 0},
    losses:     { type: Number, default: 0},
    bossBattle: { type: Object, required: false},
    public:     { type: Boolean, default: true },
    size:       { type: Number, default: 1 },
    date:       { type: Date, default: Date.now() }
});

// Execute before each user.save() call
SquadSchema.pre('save', function(callback) {
    //var player = this;
    callback();
});

SquadSchema.statics.gameObject = null;

SquadSchema.statics.resourceKey = function(objectId) {
    return 'squads-'+objectId.toString();
};

// Export the Mongoose model
var Squad = mongoose.model('Squad', SquadSchema);
Squad.gameObject = new GameObject({model: Squad});
module.exports = Squad;
