// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/**
 Todo: Account Binding
 http://grand-fate.tumblr.com/post/125427320164/binding-accounts-restoring-data

 Bind account
  - user provides a valid password
  - server provides a code
  - bind code/password combination to user model player id

 Change device/Recover account
  - user provides code/password combination
  - find player id using combination
  - delete clients with player id
  - create new client
  - bind player id to new client
  - save new client id/secret to client

  Ideally the server for storing this data will be separated from main game servers
 */

// Define our user schema
var UserSchema = new mongoose.Schema({
    clientId:     { type: String, required: true, unique: true },
    clientSecret: { type: String, required: true, unique: true },
    code:         { type: String, required: true, unique: true },
    password:     { type: String, required: true }
});

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);