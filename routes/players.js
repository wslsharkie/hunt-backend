var express = require('express');
var playerRouter = express.Router();
var authController = require('../controllers/auth');
var playerController = require('../controllers/player');

playerRouter.route('/profile')
    .post(authController.isBearerAuthenticated, playerController.showProfile);

module.exports = playerRouter;
