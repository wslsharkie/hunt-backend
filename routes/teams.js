"use strict";

var express = require('express');
var teamRouter = express.Router();
var authController = require('../controllers/auth');
var teamController = require('../controllers/team');

teamRouter.route('/')
    .get(authController.isBearerAuthenticated, teamController.showTeamsAndHeroes);

teamRouter.route('/set')
    .post(authController.isBearerAuthenticated, teamController.postTeams);

teamRouter.route('/reset')
    .post(authController.isBearerAuthenticated, teamController.resetTeams);

module.exports = teamRouter;
